/**
 * Created by christophe on 19/01/06.
 */

// Import serviceAskForHelp model
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');
ChoixDeliverAndBillHelpForAService = require('./../Models/choixDeliverAndBillHelpForAServiceModel');
Image_service = require('./../Models/image_serviceModel');
Preferred_date_and_time_service = require('./../Models/preferred_date_and_time_serviceModel');
User = require('./../Models/userModel');
WalletUser = require('./../Models/walletUserModel');
Argent_en_attente_pour_finition_service = require('./../Models/argent_en_attente_pour_finition_serviceModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
CategoryService = require('./../Models/categoryServiceModel');
SousCategory = require('./../Models/souscategoryModel');
AddressUser = require('./../Models/addressUserModel');
Bid = require('./../Models/bidModel');
Device = require('./../Models/deviceModel');
Methodpayment = require('./../Models/methodPaymentModel');
Settings = require('./../Models/settingModel');
HelpNotes = require('./../Models/HelpNotesModel');
ScheduleDates = require('./../Models/ScheduleDatesModel');


// Handle view service ask for help info    permet de GET tous les services du User
exports.view = function (req, res) {

    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien


    //on vérifie si le user existe
    User.findOne({_id: req.params.user_id}, function(err, user){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (user == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });

    //Image_service.get(function(err, image){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services User details loading..',
    //        data: image
    //    });
    //});

    //Image_service.find({serviceAskForHelp_id: '5c34ba16443ef20ee8c6b659'},function(err, image){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services User details loading..',
    //        data: image
    //    });
    //});

    ServiceAskForHelp.find({user_id: req.params.user_id}, function(err, service){
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        //Pour chaque service_id je vais prendre ses image_service et ses preferred_date_and_time_service
        var result = new Array();
        var services = service;
        var taille_tableau_services = services.length;

        //for(var i=0;i<service.length;i++){
        //    var service_id = service[i]._id;
        //
        //    Image_service.find({serviceAskForHelp_id:service_id}, function(err, image){
        //        Preferred_date_and_time_service.find({serviceAskForHelp_id:service_id}, function(err, preferred_date_and_time_service){
        //            ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id:service_id}, function(err, choixdeliverandbillhelpforaservice){
        //                //res.status(200).json({
        //                //    status: 'success',
        //                //    message: 'Services User details loading..',
        //                //    data: image/*[service[0], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice]*/
        //                //});
        //
        //                result[i] = [service[i], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice];
        //
        //            });
        //
        //            //result[i] = [service[i], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice];
        //
        //
        //
        //        });
        //    });
        //
        //
        //}
        res.status(200).json({
            status: 'success',
            message: 'Services User details loading..',
            data: service
        });

    }).sort(mysort);

};


// Handle view service ask for help info    permet de GET les infos principales d'un service du User
exports.view_service = function (req, res) {

    //on vérifie si le service existe
    ServiceAskForHelp.findById({_id: req.params.serviceaskforhelp_id}, function(err, service){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Services User details loading..',
            data: service
        });
    });
};



// Handle create service of user actions    permet d'enregistrer un service d'un user
exports.new = function (req, res) {
    var serviceAskForHelp = new ServiceAskForHelp();
    serviceAskForHelp.user_id = req.body.user_id;
    serviceAskForHelp.categoryService_id = req.body.categoryService_id;
    serviceAskForHelp.sousCategory_id = req.body.sousCategory_id;
    serviceAskForHelp.description = req.body.description;
    serviceAskForHelp.min_price = req.body.min_price;
    serviceAskForHelp.max_price = req.body.max_price;
    serviceAskForHelp.status = 'in_progress';
    serviceAskForHelp.nombre_bids = 0;
    serviceAskForHelp.Avg_bids = 0;
    serviceAskForHelp.update_date = new Date(Date.now()).toISOString();

    serviceAskForHelp.user_id_celui_choisit = "";
    serviceAskForHelp.username_celui_choisit = "";
    serviceAskForHelp.firstname_celui_choisit = "";
    serviceAskForHelp.location_name_celui_choisit = "";
    serviceAskForHelp.number_helps_asked_celui_choisit = "";
    serviceAskForHelp.number_helps_offer_celui_choisit = "";
    serviceAskForHelp.nbre_etoiles_celui_choisit = 0;
    serviceAskForHelp.picture_celui_choisit = "";
    serviceAskForHelp.price_bid_celui_choisit = "";
    serviceAskForHelp.date_livraison_celui_choisit = "";
    serviceAskForHelp.time_livraison_celui_choisit = "";
    serviceAskForHelp.description_proposition_celui_choisit = "";
    serviceAskForHelp.etoiles_donnees_par_offer_service = 0;
    serviceAskForHelp.commentaire_offer_service = "";



    var device_id = "5c6a917f9522db5b1e2642e5";         //représente le device_id de la monnaie enregistrée en BD
    serviceAskForHelp.device_id = device_id;

    var nom_service = "";
    var device_name = ";"

    //je recherche le device_name
    Device.findById(device_id, function(err, data){
        device_name = data.name;
        serviceAskForHelp.device_name = device_name;
    });


    var transactions = new Transactions();
    var transactionWalletUser = new TransactionWalletUser();
    var argent_en_attente_pour_finition_service = new Argent_en_attente_pour_finition_service();

    var location_name = null;

    //je me rassure si le user existe vraiment
    User.findOne({_id:req.body.user_id}, function (err,user){
        /*if (err)
         res.status(500).json({
         status: 'error',
         message: 'Internal Problem Server'
         });*/

        if(user == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
        else{
            serviceAskForHelp.nbre_etoiles = user.nbre_etoiles;
            location_name = user.location_name;

            //je me rassure si categoryService_id existe
            CategoryService.findById(req.body.categoryService_id,function( err, categoryservice){
                if (categoryservice == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'Category Service not exist'
                    });
                }
                else{

                    //je me rassure si sousCategory_id existe
                    SousCategory.findById(req.body.sousCategory_id,function( err, souscategory){
                        if (souscategory == null){
                            res.status(401).json({
                                status: 'error',
                                message: 'SousCategory Service not exist'
                            });
                        }
                        else{
                            //je prends en paramètre le nom de la souscategory comme nom_service
                            serviceAskForHelp.nom_service = souscategory.souscategoryName + ' '+ Math.random().toString(36).substr(2, 2) + '-'+ Math.floor(Math.random() * 100000);
                            nom_service = souscategory.souscategoryName + ' '+ Math.random().toString(36).substr(2, 2) + '-'+ Math.floor(Math.random() * 100000);


                            //je vais faire le contrôle sur min_price et max_price par rapport au solde du wallet du user
                            //si c'est bon alors j'enregistre le service puis je mets service_id dans une variable
                            WalletUser.findOne({user_id:req.body.user_id, device_id:device_id}, function(err, walletuser){
                                if (err)
                                    res.status(500).json({
                                        status: 'error',
                                        message: 'Internal Problem Server'
                                    });

                                if(walletuser == null){
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'Insufficient Balance. Recharge your account and try again.'
                                    });
                                }
                                else if(walletuser.amount < req.body.min_price || walletuser.amount < req.body.max_price){
                                    //alors solde wallet insuffisant
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'Insufficient Balance. Recharge your account and try again.'
                                    });
                                }
                                else{
                                    //je dois récupérer le city_deliver_help, preferred_date_service et preferred_time_service
                                    //commençons par city_deliver_help
                                    var tableau_deliverandbillhelpforaservice = req.body.choixdeliverandbillhelpforaservice;
                                    var city_deliver_help = null;
                                    for(var i=0;i<tableau_deliverandbillhelpforaservice.length;i++){
                                        if(tableau_deliverandbillhelpforaservice[i].deliver_help_to_this_address == 1){
                                            //pour dire que c'est cette adresse qui a été sélectionnée
                                            city_deliver_help = tableau_deliverandbillhelpforaservice[i].city;
                                        }
                                    }

                                    if(city_deliver_help == null){
                                        city_deliver_help = location_name;
                                    }

                                    //j'initialise city_deliver_help dans l'objet serviceAskForHelp
                                    serviceAskForHelp.city_deliver_help = city_deliver_help;

                                    //continuons avec preferred_date_service et preferred_time_service
                                    var tableau_preferred_date_and_time_service = req.body.preferred_date_and_time_service;

                                    //j'initialise preferred_date_service et preferred_time_service dans l'objet serviceAskForHelp
                                    serviceAskForHelp.preferred_date_service = tableau_preferred_date_and_time_service[0].date_service;
                                    serviceAskForHelp.preferred_time_service = tableau_preferred_date_and_time_service[0].time_service;


                                    //Enregistrement partiel du service pour avoir le service_id
                                    serviceAskForHelp.save(function(err, serviceaskforhelp){
                                        if (err)
                                            res.status(500).json({
                                                status: 'error',
                                                message: 'Internal Problem Server'
                                            });

                                        var service_id = serviceAskForHelp._id;

                                        //j'incrémente son number_helps_asked dans userModel puis je mets ça à jour dans bidModel
                                        User.findById(req.body.user_id, function(err, data_user){
                                            User.findByIdAndUpdate(req.body.user_id,
                                                {
                                                    number_helps_asked: data_user.number_helps_asked + 1,
                                                    update_date: new Date(Date.now()).toISOString()
                                                },
                                                function (err, resultat_new){
                                                    //puis je mets ça à jour dans tous les bid dont il a participé
                                                    //commençons par celui qui bid
                                                    Bid.findAndUpdate({user_id_celui_qui_bid: req.body.user_id},
                                                        {
                                                            number_helps_asked_celui_qui_bid: resultat_new.number_helps_asked
                                                        },
                                                        function(err, result){});

                                                    //continuons avec celui qui offre service
                                                    Bid.findAndUpdate({user_id_celui_qui_offre_service: req.body.user_id},
                                                        {
                                                            number_helps_asked_celui_qui_offre_service: resultat_new.number_helps_asked
                                                        },
                                                        function(err, result){});
                                                });
                                        });

                                        var choix = new Array();
                                        var image = new Array();
                                        var preferred_date_and_time = new Array();

                                        //capture des infos des choix de Deliver et Bill address choisi par le user pour un service
                                        //Je prends le tableau d'objets ici
                                        var tableau_deliverandbillhelpforaservice = req.body.choixdeliverandbillhelpforaservice;

                                        var choixdeliverandbillhelpforaservicearray = new Array();



                                        //je stocke les objets du tableau dans un tableau
                                        for(var i=0;i<tableau_deliverandbillhelpforaservice.length;i++){

                                            choixdeliverandbillhelpforaservicearray[i] = new ChoixDeliverAndBillHelpForAService();

                                            //recherche de AddressUser
                                            //AddressUser.findById(tableau_deliverandbillhelpforaservice[0].addressUser_id, function(err, addressUser){



                                            //if (addressUser != null){

                                            choixdeliverandbillhelpforaservicearray[i].address_title = tableau_deliverandbillhelpforaservice[i].address_title;
                                            choixdeliverandbillhelpforaservicearray[i].address = tableau_deliverandbillhelpforaservice[i].address;
                                            choixdeliverandbillhelpforaservicearray[i].zip_code_postal = tableau_deliverandbillhelpforaservice[i].zip_code_postal;
                                            choixdeliverandbillhelpforaservicearray[i].telephone = tableau_deliverandbillhelpforaservice[i].telephone;
                                            choixdeliverandbillhelpforaservicearray[i].city = tableau_deliverandbillhelpforaservice[i].city;
                                            choixdeliverandbillhelpforaservicearray[i].email = tableau_deliverandbillhelpforaservice[i].email;

                                            choixdeliverandbillhelpforaservicearray[i].user_id = req.body.user_id;
                                            choixdeliverandbillhelpforaservicearray[i].serviceAskForHelp_id = service_id;
                                            choixdeliverandbillhelpforaservicearray[i].addressUser_id = tableau_deliverandbillhelpforaservice[i].addressUser_id;
                                            choixdeliverandbillhelpforaservicearray[i].deliver_help_to_this_address = tableau_deliverandbillhelpforaservice[i].deliver_help_to_this_address;
                                            choixdeliverandbillhelpforaservicearray[i].bill_to_this_address = tableau_deliverandbillhelpforaservice[i].bill_to_this_address;
                                            choixdeliverandbillhelpforaservicearray[i].status_service = 'in_progress';
                                            choixdeliverandbillhelpforaservicearray[i].update_date = new Date(Date.now()).toISOString();
                                            // }
                                            //});


                                        }

                                        //j'enregistre chaque objet du tableau
                                        for(var i=0;i<tableau_deliverandbillhelpforaservice.length;i++){
                                            choixdeliverandbillhelpforaservicearray[i].save(function(err, choix){
                                                choix = choixdeliverandbillhelpforaservicearray;
                                            });
                                        }

                                        //capture des infos de image_service
                                        //Je prends le tableau d'objets ici
                                        var tableau_image_service = req.body.image_service;

                                        var image_servicearray = new Array();

                                        //je stocke les objets du tableau dans un tableau
                                        for(var i=0;i<tableau_image_service.length;i++){
                                            image_servicearray[i] = new Image_service();

                                            image_servicearray[i].user_id = req.body.user_id;
                                            image_servicearray[i].serviceAskForHelp_id = service_id;
                                            image_servicearray[i].picture = tableau_image_service[i].picture;
                                            image_servicearray[i].update_date = new Date(Date.now()).toISOString();
                                        }

                                        //j'enregistre chaque objet du tableau
                                        for(var i=0;i<tableau_image_service.length;i++){
                                            image_servicearray[i].save(function(err,image_servicearraynew){
                                                image = image_servicearray;
                                            });
                                        }

                                        //capture des infos de preferres_date_and_time_service
                                        //Je prends le tableau d'objets ici
                                        var tableau_preferred_date_and_time_service = req.body.preferred_date_and_time_service;

                                        var preferred_date_and_time_servicearray = new Array();

                                        //je stocke les objets du tableau dans un tableau
                                        for(var i=0;i<tableau_preferred_date_and_time_service.length;i++){
                                            preferred_date_and_time_servicearray[i] = new Preferred_date_and_time_service();

                                            preferred_date_and_time_servicearray[i].user_id = req.body.user_id;
                                            preferred_date_and_time_servicearray[i].serviceAskForHelp_id = service_id;
                                            preferred_date_and_time_servicearray[i].time = tableau_preferred_date_and_time_service[i].time_service;
                                            preferred_date_and_time_servicearray[i].date = tableau_preferred_date_and_time_service[i].date_service;
                                            preferred_date_and_time_servicearray[i].update_date = new Date(Date.now()).toISOString();
                                        }

                                        //j'enregistre chaque objet du tableau
                                        for(var i=0;i<tableau_preferred_date_and_time_service.length;i++){
                                            preferred_date_and_time_servicearray[i].save(function(err,preferred_date_and_time_servicearraynew){
                                                preferred_date_and_time =  preferred_date_and_time_servicearray;
                                            });
                                        }



                                        //soustraction de l'argent (max_price) dans le wallet du user et le stocke dans Argent_en_attente_pour_finition_service
                                        WalletUser.findOne({user_id:req.body.user_id, device_id:device_id}, function(err, wallet){

                                            //if(wallet.amount < req.body.max_price){
                                            //    res.status(401).json({
                                            //        status: 'error',
                                            //        message: 'Insufficient Balance. Recharge your account and try again.'
                                            //    });
                                            //}

                                            /*WalletUser.findByIdAndUpdate(req.body.user_id,
                                             {
                                             amount: wallet.amount - req.body.max_price,
                                             update_date: new Date(Date.now()).toISOString()
                                             },
                                             function (err, data) {});*/

                                            var myquery = { user_id: req.body.user_id, device_id: device_id };
                                            var newvalues = {$set: {amount: wallet.amount - req.body.max_price} };

                                            WalletUser.updateMany(myquery, newvalues, function(err, res) {
                                                //if (err){}
                                            });

                                            //var reference_transaction = 'Cash-deposit-' + /*Math.random().toString(36).substr(2, 2) + '-'+*/ Math.floor(Math.random() * 1000000000000000);
                                            var reference_transaction = nom_service;

                                            //je prends l'ID de methodPayment = Wallet
                                            var methodpayment_id = "";
                                            Methodpayment.findOne({name: "Wallet"}, function(err, method){
                                                methodpayment_id = method._id;
                                            });

                                            //Enregistrement transaction Wallet User et transactions
                                            //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                            transactions.type_transaction = 'wallet';
                                            transactions.amount = req.body.max_price;
                                            transactions.sens = 'sortant';
                                            transactions.user_id = req.body.user_id;
                                            transactions.reference_transaction = reference_transaction;
                                            transactions.device_id = device_id;
                                            transactions.device_name = device_name;
                                            transactions.methodpayment_id = methodpayment_id;
                                            transactions.methodpayment_name = "Wallet";
                                            transactions.save();

                                            transactionWalletUser.user_id = req.body.user_id;
                                            transactionWalletUser.amount = req.body.max_price;
                                            transactionWalletUser.sens = 'sortant';
                                            transactionWalletUser.reference_transaction = reference_transaction;
                                            transactionWalletUser.device_id = device_id;
                                            transactionWalletUser.device_name = device_name;
                                            transactionWalletUser.methodpayment_id = methodpayment_id;
                                            transactionWalletUser.methodpayment_name = "Wallet";
                                            transactionWalletUser.save();


                                            //Enregistrement dans Argent_en_attente_pour_finition_service
                                            argent_en_attente_pour_finition_service.user_id = req.body.user_id;
                                            argent_en_attente_pour_finition_service.serviceAskForHelp_id = service_id;
                                            argent_en_attente_pour_finition_service.amount = req.body.max_price;
                                            argent_en_attente_pour_finition_service.update_date = new Date(Date.now()).toISOString();
                                            argent_en_attente_pour_finition_service.save();

                                        });



                                        //je récupère les images, preferred_date_time et choix du user
                                        //Image_service.find({serviceAskForHelp_id:service_id}, function(err, image_servicearraynew){
                                        //
                                        //    Preferred_date_and_time_service.find({serviceAskForHelp_id:service_id}, function(err, preferred_date_and_timearraynew){
                                        //
                                        //        ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id:service_id}, function(err, choixarraynew){
                                        //
                                        //            var result = [serviceaskforhelp/*serviceAskForHelp*/, image_servicearraynew, preferred_date_and_timearraynew, choixarraynew];
                                        //
                                        //            res.status(200).json({
                                        //                status: 'success',
                                        //                message: 'New Service User created!',
                                        //                data: result
                                        //            });
                                        //
                                        //        });
                                        //    });
                                        //
                                        //});


                                        var result = [serviceaskforhelp/*serviceAskForHelp*/, image, preferred_date_and_time, choix];

                                        res.status(200).json({
                                            status: 'success',
                                            message: 'New Service User created!',
                                            data: result
                                        });


                                    });

                                }

                                //var solde = walletuser.amount;
                                //if (solde < req.body.min_price || solde < req.body.max_price){
                                //    //alors solde wallet insuffisant
                                //    res.status(401).json({
                                //        status: 'error',
                                //        message: 'Insufficient Balance. Recharge your account and try again.'
                                //    });
                                //}
                            });




                        }
                    });


                }
            });




        }
    });









    //AddressUser.findById('5c34dd660acd171297d08310'/*tableau_deliverandbillhelpforaservice[0].addressUser_id*/, function(err, good){
    //    if (err){
    //        res.status(500).json({
    //            status: 'success',
    //            message: 'Internal Problem Server',
    //            data: err
    //        });
    //    }
    //
    //
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Internal Problem Server',
    //        data: good
    //    });
    //
    //});



};



// Handle update Service info  permet d'update un service
exports.update = function (req, res) {
    var user_id = req.body.user_id;
    var categoryService_id = req.body.categoryService_id;
    var sousCategory_id = req.body.sousCategory_id;
    var description = req.body.description;
    var min_price = req.body.min_price;
    var max_price = req.body.max_price;
    var status = 'in_progress';
    var update_date = new Date(Date.now()).toISOString();
    var serviceAskForHelp_id = req.params.serviceaskforhelp_id;

    var wallet = new WalletUser();
    var transactions = new Transactions();
    var transactionWalletUser = new TransactionWalletUser();

    var city_deliver_help = null;
    var preferred_date_service = null;
    var preferred_time_service = null;

    var device_id = "5c6a917f9522db5b1e2642e5";     //ObjectId de la device ZAR
    var device_name = "";
    var reference_transaction = "";

    var methodpaymend_id = "";
    //je cherche le nom de methodPayment
    Methodpayment.findOne({name: "Wallet"}, function(err, methodpayment){
        methodpaymend_id = methodpayment._id;
    });

    //je prends le nom de device_id
    Device.findById(device_id, function(err, data){
        device_name = data.name;
    });

    //je récupère city_deliver_help actualisé
    var tableau_deliverandbillhelpforaservice = req.body.choixdeliverandbillhelpforaservice;
    for(var j=0;j</*choixdeliverandbillhelpforaservice*/tableau_deliverandbillhelpforaservice.length;j++){
        if(tableau_deliverandbillhelpforaservice[j].deliver_help_to_this_address == 1){
            //pour dire que c'est cette adresse qui a été choisie
            city_deliver_help = tableau_deliverandbillhelpforaservice[j].city;
        }
    }

    //je récupère preferred_date_service et preferred_date_time
    var tableau_preferred_date_and_time_service = req.body.preferred_date_and_time_service;
    preferred_date_service = tableau_preferred_date_and_time_service[0].date_service;
    preferred_time_service = tableau_preferred_date_and_time_service[0].time_service;


    //je contrôle si le serviceAskForHelp existe
    ServiceAskForHelp.findById(serviceAskForHelp_id, function (err, serviceAskForHelp){
        /*if (err) {
         res.status(500).json({
         status: 'error',
         message: 'Internal Problem Server'
         });
         }*/

        if (serviceAskForHelp == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            //je prends la reference_transaction qui représente le nom du service
            reference_transaction = serviceAskForHelp.nom_service;

            //je me rassure si le user existe vraiment
            User.findOne({_id:user_id}, function (err,user){
                /*if (err)
                 res.status(500).json({
                 status: 'error',
                 message: 'Internal Problem Server'
                 });*/

                if(user == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'User not exist'
                    });
                }
                else{
                    //je me rassure si categoryService_id existe
                    CategoryService.findById(req.body.categoryService_id,function( err, categoryservice){
                        if (categoryservice == null){
                            res.status(401).json({
                                status: 'error',
                                message: 'Category Service not exist'
                            });
                        }
                        else{
                            //je me rassure si sousCategory_id existe
                            SousCategory.findById(req.body.sousCategory_id,function( err, souscategory){
                                if (souscategory == null){
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'SousCategory Service not exist'
                                    });
                                }
                                else{
                                    //je vais faire le contrôle sur min_price et max_price par rapport au solde du wallet du user
                                    //si c'est bon alors je fais l'update
                                    WalletUser.findOne({user_id:user_id, device_id:device_id}, function(err, walletuser){
                                        /*if (err)
                                         res.status(500).json({
                                         status: 'error',
                                         message: 'Internal Problem Server'
                                         });
                                         */

                                        if(walletuser == null){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'User not exist'
                                            });
                                        }
                                        else{
                                            var solde = walletuser.amount;
                                            if (solde < min_price || solde < max_price){
                                                //alors solde wallet insuffisant
                                                res.status(401).json({
                                                    status: 'error',
                                                    message: 'Insufficient Balance. Recharge your account and try again.'
                                                });
                                            }
                                            else{

                                                //j'update les infos de ServiceAskForHelp
                                                ServiceAskForHelp.findByIdAndUpdate(serviceAskForHelp_id,
                                                    {
                                                        categoryService_id: categoryService_id,
                                                        sousCategory_id: sousCategory_id,
                                                        description: description,
                                                        min_price: min_price,
                                                        max_price: max_price,
                                                        city_deliver_help: city_deliver_help,
                                                        preferred_date_service: preferred_date_service,
                                                        preferred_time_service: preferred_time_service,
                                                        update_date: new Date(Date.now()).toISOString()
                                                    },
                                                    function (err, serviceAskForHelp){
                                                        //j'update aussi ça dans bidModel pour tous les users ayant biddé pour le service
                                                        var myquery = { serviceAskForHelpId: serviceAskForHelp_id };
                                                        var newvalues = {$set: {
                                                            categoryService_id: categoryService_id,
                                                            sousCategory_id: sousCategory_id,
                                                            description: description,
                                                            min_price: min_price,
                                                            max_price: max_price,
                                                            city_deliver_help: city_deliver_help,
                                                            preferred_date_service: preferred_date_service,
                                                            preferred_time_service: preferred_time_service
                                                        }
                                                        };

                                                        Bid.updateMany(myquery, newvalues, function(err, res) {
                                                            //if (err){}
                                                        });


                                                        //puis j'update les infos de ChoixDeliverAndBillHelpForAService avec serviceAskForHelp_id
                                                        //je récupère les choixdeliverandbillhelpforaservice_id pour pouvoir faire une fois les update
                                                        ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id:serviceAskForHelp_id},function (err, choixdeliverandbillhelpforaservice){
                                                            if (choixdeliverandbillhelpforaservice.length != 0){
                                                                //alors je fais les update
                                                                //capture des infos des choix de Deliver et Bill address choisi par le user pour un service
                                                                //Je prends le tableau d'objets ici
                                                                var tableau_deliverandbillhelpforaservice = req.body.choixdeliverandbillhelpforaservice;
                                                                //var choixdeliverandbillhelpforaservicearray = new Array();

                                                                for(var j=0;j</*choixdeliverandbillhelpforaservice*/tableau_deliverandbillhelpforaservice.length;j++){

                                                                    ChoixDeliverAndBillHelpForAService.findByIdAndUpdate(choixdeliverandbillhelpforaservice[j]._id,
                                                                        {
                                                                            addressUser_id: tableau_deliverandbillhelpforaservice[j].addressUser_id,
                                                                            address_title: tableau_deliverandbillhelpforaservice[j].address_title,
                                                                            address: tableau_deliverandbillhelpforaservice[j].address,
                                                                            zip_code_postal: tableau_deliverandbillhelpforaservice[j].zip_code_postal,
                                                                            telephone: tableau_deliverandbillhelpforaservice[j].telephone,
                                                                            city: tableau_deliverandbillhelpforaservice[j].city,
                                                                            email: tableau_deliverandbillhelpforaservice[j].email,
                                                                            deliver_help_to_this_address: tableau_deliverandbillhelpforaservice[j].deliver_help_to_this_address,
                                                                            bill_to_this_address: tableau_deliverandbillhelpforaservice[j].bill_to_this_address,
                                                                            update_date: new Date(Date.now()).toISOString()
                                                                        },
                                                                        function(err, resultat_new){});
                                                                }
                                                            }
                                                        });


                                                        //puis j'update les infos de Image_service avec serviceAskForHelp_id
                                                        //je récupère les image_service_id pour pouvoir faire une fois les update
                                                        Image_service.find({serviceAskForHelp_id:serviceAskForHelp_id},function(err, image_service){
                                                            if (image_service.length != 0){
                                                                //alors je fais les update
                                                                //capture des infos de image_service
                                                                //Je prends le tableau d'objets ici
                                                                var tableau_image_service = req.body.image_service;

                                                                //var image_servicearray = new Array();

                                                                for (var j=0;j<tableau_image_service.length;j++){

                                                                    Image_service.findByIdAndUpdate(tableau_image_service[j]._id,
                                                                        {
                                                                            picture: tableau_image_service[j].picture,
                                                                            update_date: new Date(Date.now()).toISOString()
                                                                        },
                                                                        function (err, resultat_new){});
                                                                }
                                                            }
                                                        });


                                                        //puis j'update les infos de Preferred_date_and_time_service avec serviceAskForHelp_id
                                                        //je récupère les preferred_date_and_time_service_id pour pouvoir faire une fois les update
                                                        Preferred_date_and_time_service.find({serviceAskForHelp_id:serviceAskForHelp_id},function(err, preferred_date_and_time_service){
                                                            if (preferred_date_and_time_service.length != 0){
                                                                //alors je fais les update
                                                                //capture des infos de preferres_date_and_time_service
                                                                //Je prends le tableau d'objets ici
                                                                var tableau_preferred_date_and_time_service = req.body.preferred_date_and_time_service;

                                                                //var preferred_date_and_time_servicearray = new Array();

                                                                for (var j=0;j<tableau_preferred_date_and_time_service.length;j++){

                                                                    Preferred_date_and_time_service.findByIdAndUpdate(tableau_preferred_date_and_time_service[j]._id,
                                                                        {
                                                                            time: tableau_preferred_date_and_time_service[j].time_service,
                                                                            date: tableau_preferred_date_and_time_service[j].date_service,
                                                                            update_date: new Date(Date.now()).toISOString()
                                                                        },
                                                                        function(err, resultat_new){});
                                                                }
                                                            }
                                                        });
                                                    });


                                                //je dois mettre à jour argent en attente pour finition service pour le service à modifier
                                                //je prends d'abord l'ancien amount dans argent en attente pour finition service
                                                Argent_en_attente_pour_finition_service.findOne({serviceAskForHelp_id: serviceAskForHelp_id}, function (err, argent_en_attente){
                                                    /*if (err) {
                                                     res.status(500).json({
                                                     status: 'error',
                                                     message: 'Internal Problem Server'
                                                     });
                                                     }*/

                                                    if (argent_en_attente != null){
                                                        //alors je crédite le wallet du user avec le montant présent dans Argent en attente pour finition service
                                                        //puis je save les transactions (sens = entrant)
                                                        WalletUser.findOne({user_id:user_id, device_id:device_id}, function (err, data){

                                                            if(data != null) { //alors il y a une occurence d'enregistrement et on doit tout simplement faire un update

                                                                var myquery = { user_id: user_id };

                                                                WalletUser.findByIdAndUpdate(data._id,
                                                                    {
                                                                        amount: data.amount + argent_en_attente.amount,
                                                                        update_date: new Date(Date.now()).toISOString()
                                                                    },
                                                                    function (err, data_wallet) {
                                                                        /*if (err)
                                                                         res.status(500).json({
                                                                         status: 'error',
                                                                         message: 'Internal Problem Server'
                                                                         });*/



                                                                        //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                                                        transactions.type_transaction = 'wallet';
                                                                        transactions.amount = argent_en_attente.amount;
                                                                        transactions.sens = 'entrant';
                                                                        transactions.user_id = user_id;
                                                                        transactions.reference_transaction = reference_transaction;
                                                                        transactions.device_id = device_id;
                                                                        transactions.device_name = device_name;
                                                                        transactions.methodpayment_id = methodpaymend_id;
                                                                        transactions.methodpayment_name = "Wallet";
                                                                        transactions.save();

                                                                        transactionWalletUser.user_id = user_id;
                                                                        transactionWalletUser.amount = argent_en_attente.amount;
                                                                        transactionWalletUser.sens = 'entrant';
                                                                        transactionWalletUser.reference_transaction = reference_transaction;
                                                                        transactionWalletUser.device_id = device_id;
                                                                        transactionWalletUser.device_name = device_name;
                                                                        transactionWalletUser.methodpayment_id = methodpaymend_id;
                                                                        transactionWalletUser.methodpayment_name = "Wallet";
                                                                        transactionWalletUser.save();
                                                                    });

                                                            }
                                                            else{
                                                                wallet.user_id = user_id;
                                                                wallet.amount = argent_en_attente.amount;
                                                                wallet.update_date = new Date(Date.now()).toISOString();

                                                                // save wallet user and check for errors
                                                                wallet.save(function (err) {
                                                                    /*if (err) {
                                                                     res.status(500).json({
                                                                     status: 'error',
                                                                     message: 'Internal Problem Server'
                                                                     });
                                                                     }*/

                                                                    //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                                                    transactions.type_transaction = 'wallet';
                                                                    transactions.amount = argent_en_attente.amount;
                                                                    transactions.sens = 'entrant';
                                                                    transactions.user_id = user_id;
                                                                    transactions.reference_transaction = reference_transaction;
                                                                    transactions.device_id = device_id;
                                                                    transactions.device_name = device_name;
                                                                    transactions.methodpayment_id = methodpaymend_id;
                                                                    transactions.methodpayment_name = "Wallet";
                                                                    transactions.save();

                                                                    transactionWalletUser.user_id = user_id;
                                                                    transactionWalletUser.amount = argent_en_attente.amount;
                                                                    transactionWalletUser.sens = 'entrant';
                                                                    transactionWalletUser.reference_transaction = reference_transaction;
                                                                    transactionWalletUser.device_id = device_id;
                                                                    transactionWalletUser.device_name = device_name;
                                                                    transactionWalletUser.methodpayment_id = methodpaymend_id;
                                                                    transactionWalletUser.methodpayment_name = "Wallet";
                                                                    transactionWalletUser.save();

                                                                });
                                                            }

                                                        });

                                                        //puis j'update argent en attente pour finition service avec le nouveau montant max_price
                                                        Argent_en_attente_pour_finition_service.findByIdAndUpdate(argent_en_attente._id,
                                                            {
                                                                amount: max_price,
                                                                update_date: new Date(Date.now()).toISOString()
                                                            },
                                                            function (err, data) {});


                                                        //puis je débite à nouveau le wallet du user avec le nouveau montant max_price
                                                        //puis je save les transactions (sens = sortant)
                                                        WalletUser.findOne({user_id:user_id, device_id:device_id}, function (err, wallet_new){
                                                            if (wallet_new != null){
                                                                WalletUser.findByIdAndUpdate(wallet_new._id,
                                                                    {
                                                                        amount: wallet_new.amount - max_price,
                                                                        update_date: new Date(Date.now()).toISOString()
                                                                    },
                                                                    function (err, resultat){
                                                                        var transactions = new Transactions();
                                                                        var transactionWalletUser = new TransactionWalletUser();

                                                                        //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                                                        transactions.type_transaction = 'wallet';
                                                                        transactions.amount = max_price;
                                                                        transactions.sens = 'sortant';
                                                                        transactions.user_id = user_id;
                                                                        transactions.reference_transaction = reference_transaction;
                                                                        transactions.device_id = device_id;
                                                                        transactions.device_name = device_name;
                                                                        transactions.methodpayment_id = methodpaymend_id;
                                                                        transactions.methodpayment_name = "Wallet";
                                                                        transactions.save();

                                                                        transactionWalletUser.user_id = user_id;
                                                                        transactionWalletUser.amount = max_price;
                                                                        transactionWalletUser.sens = 'sortant';
                                                                        transactionWalletUser.reference_transaction = reference_transaction;
                                                                        transactionWalletUser.device_id = device_id;
                                                                        transactionWalletUser.device_name = device_name;
                                                                        transactionWalletUser.methodpayment_id = methodpaymend_id;
                                                                        transactionWalletUser.methodpayment_name = "Wallet";
                                                                        transactionWalletUser.save();
                                                                    });
                                                            }
                                                        });
                                                    }
                                                });


                                                //Maintenant je prends toutes les nouvelles données update et je get tout ça
                                                ServiceAskForHelp.find({_id:serviceAskForHelp_id}, function( err, serviceAskForHelp){
                                                    if (err) {
                                                        res.status(500).json({
                                                            status: 'error',
                                                            message: 'Internal Problem Server'
                                                        });
                                                    }

                                                    ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id:serviceAskForHelp_id}, function(err, choixdeliverandbillhelpforaservice_new){

                                                        Preferred_date_and_time_service.find({serviceAskForHelp_id:serviceAskForHelp_id}, function( err, preferred_date_and_time_service_new){

                                                            Image_service.find({serviceAskForHelp_id:serviceAskForHelp_id}, function( err, image_service_new){

                                                                //var result = [serviceAskForHelp, image_service_new, preferred_date_and_time_service_new, choixdeliverandbillhelpforaservice_new];

                                                                res.status(200).json({
                                                                    status: 'success',
                                                                    message: 'Service User Info updated'
                                                                    //data: result
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};



// Handle delete service ask for help of user  permet de supprimer un service
exports.delete = function (req, res) {
    var methodpaymend_id = "";
    //je cherche le nom de methodPayment
    Methodpayment.findOne({name: "Wallet"}, function(err, methodpayment){
        methodpaymend_id = methodpayment._id;
    });


    //je vérifie si status = selected pour voir si un user n'a pas encore choisi ce service
    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function(err, data){
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (data == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            if (data.status == 'bidder_selected'){
                res.status(401).json({
                    status: 'error',
                    message: 'This Service is already selected by a user'
                });
            }
            else{
                //je remets l'argent en attente pour finition service dans le portemonnaie du user puis je supprime maintenant le service
                Argent_en_attente_pour_finition_service.findOne({serviceAskForHelp_id:req.params.serviceAskForHelp_id}, function (err, data_new){
                    if (data_new != null){
                        var reference_transaction = data_new.nom_service;
                        var device_id = data_new.device_id;
                        var device_name = data_new.device_name;

                        WalletUser.findOne({user_id:data_new.user_id, device_id:device_id}, function (err, wallet){
                            if(wallet != null){
                                WalletUser.findByIdAndUpdate(data_new.user_id,
                                    {
                                        amount: wallet.amount + data_new.amount,
                                        update_date: new Date(Date.now()).toISOString()
                                    },
                                    function (err, data_wallet) {
                                        //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                        transactions.type_transaction = 'wallet';
                                        transactions.amount = data_new.amount;
                                        transactions.sens = 'entrant';
                                        transactions.user_id = data_new.user_id;
                                        transactions.reference_transaction = reference_transaction;
                                        transactions.device_id = device_id;
                                        transactions.device_name = device_name;
                                        transactions.methodpayment_id = methodpaymend_id;
                                        transactions.methodpayment_name = "Wallet";
                                        transactions.save();

                                        transactionWalletUser.user_id = data_new.user_id;
                                        transactionWalletUser.amount = data_new.amount;
                                        transactionWalletUser.sens = 'entrant';
                                        transactionWalletUser.reference_transaction = reference_transaction;
                                        transactionWalletUser.device_id = device_id;
                                        transactionWalletUser.device_name = device_name;
                                        transactionWalletUser.methodpayment_id = methodpaymend_id;
                                        transactionWalletUser.methodpayment_name = "Wallet";
                                        transactionWalletUser.save();
                                    });
                            }
                            else{
                                var wallet = new WalletUser();
                                wallet.user_id = data_new.user_id;
                                wallet.amount = data_new.amount;
                                wallet.update_date = new Date(Date.now()).toISOString();

                                var transactions = new Transactions();
                                var transactionWalletUser = new TransactionWalletUser();


                                // save wallet user and check for errors
                                wallet.save(function (err) {
                                    //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                    transactions.type_transaction = 'wallet';
                                    transactions.amount = data_new.amount;
                                    transactions.sens = 'entrant';
                                    transactions.user_id = data_new.user_id;
                                    transactions.reference_transaction = reference_transaction;
                                    transactions.device_id = device_id;
                                    transactions.device_name = device_name;
                                    transactions.methodpayment_id = methodpaymend_id;
                                    transactions.methodpayment_name = "Wallet";
                                    transactions.save();

                                    transactionWalletUser.user_id = data_new.user_id;
                                    transactionWalletUser.amount = data_new.amount;
                                    transactionWalletUser.sens = 'entrant';
                                    transactionWalletUser.reference_transaction = reference_transaction;
                                    transactionWalletUser.device_id = device_id;
                                    transactionWalletUser.device_name = device_name;
                                    transactionWalletUser.methodpayment_id = methodpaymend_id;
                                    transactionWalletUser.methodpayment_name = "Wallet";
                                    transactionWalletUser.save();
                                });
                            }
                        });
                    }
                });

                //suppression du service
                ServiceAskForHelp.remove({_id: req.params.serviceAskForHelp_id}, function (err, service) {
                    if (err)
                        res.status(401).json({
                            status: 'error',
                            message: 'Service not exist'
                        });

                    res.status(200).json({
                        status: "success",
                        message: 'Service deleted'
                    });
                });


            }
        }


    });
};


//permet de changer le status d'un service à bidder_selected
exports.change_status_service = function (req, res) {
    
    ServiceAskForHelp.findByIdAndUpdate(req.params.serviceaskforhelp_id,
                {
                    status: 'bidder_selected'
                    //update_date: new Date(Date.now()).toISOString()
                }, function(err, data){
                    
                    var myquery = { serviceAskForHelpId: req.params.serviceaskforhelp_id };
                    var newvalues = {$set: {
                        status_service: 'bidder_selected'
                    }
                    };

                    Bid.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });


                    //je récupère les nouvelles infos pour renvoyer
                    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function(err, data_new){
                        res.status(200).json({
                            status: 'success',
                            message: 'Service User Info updated',
                            data: data_new
                        });
                    });
                });
    
    
};


// Handle update Service info  permet d'enregistrer celui qui a été accepté pour faire le service du user
exports.accept_bidder = function (req, res) {
    //je contrôle si le serviceAskForHelp existe
    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function (err, serviceAskForHelp) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (serviceAskForHelp == null) {
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            //je récupère les infos de celui qui a été accepté pour le service
            var user_id_celui_choisit = req.body.user_id_celui_choisit;
            var username_celui_choisit = req.body.username_celui_choisit;
            var firstname_celui_choisit = req.body.firstname_celui_choisit;
            var location_name_celui_choisit = req.body.location_name_celui_choisit;
            var number_helps_asked_celui_choisit = req.body.number_helps_asked_celui_choisit;
            var number_helps_offer_celui_choisit = req.body.number_helps_offer_celui_choisit;
            var nbre_etoiles_celui_choisit = req.body.nbre_etoiles_celui_choisit;
            var picture_celui_choisit = req.body.picture_celui_choisit;
            var price_bid_celui_choisit = req.body.price_bid_celui_choisit;
            var date_livraison_celui_choisit = req.body.date_livraison_celui_choisit;
            var time_livraison_celui_choisit = req.body.time_livraison_celui_choisit;
            var description_proposition_celui_choisit = req.body.description_proposition_celui_choisit;
            var etoiles_donnees_par_offer_service = req.body.etoiles_donnees_par_offer_service;
            var commentaire_offer_service = req.body.commentaire_offer_service;
            var status = 'bidder_selected';


            //j'update maintenant les infos de serviceAskForHelp
            //j'update les infos de ServiceAskForHelp
            ServiceAskForHelp.findByIdAndUpdate(req.params.serviceaskforhelp_id,
                {
                    user_id_celui_choisit: user_id_celui_choisit,
                    username_celui_choisit: username_celui_choisit,
                    firstname_celui_choisit: firstname_celui_choisit,
                    location_name_celui_choisit: location_name_celui_choisit,
                    number_helps_asked_celui_choisit: number_helps_asked_celui_choisit,
                    number_helps_offer_celui_choisit: number_helps_offer_celui_choisit,
                    nbre_etoiles_celui_choisit: nbre_etoiles_celui_choisit,
                    picture_celui_choisit: picture_celui_choisit,
                    price_bid_celui_choisit: price_bid_celui_choisit,
                    date_livraison_celui_choisit: date_livraison_celui_choisit,
                    time_livraison_celui_choisit: time_livraison_celui_choisit,
                    description_proposition_celui_choisit: description_proposition_celui_choisit,
                    etoiles_donnees_par_offer_service: etoiles_donnees_par_offer_service,
                    commentaire_offer_service: commentaire_offer_service,
                    status: status,
                    update_date: new Date(Date.now()).toISOString()
                }, function(err, data){
                    
                    //j'update aussi status_service dans bidModel
                    var myquery = { serviceAskForHelpId: req.params.serviceaskforhelp_id };
                    var newvalues = {$set: {
                        status_service: 'bidder_selected'
                    }
                    };

                    Bid.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });

                    //je récupère les nouvelles infos pour renvoyer
                    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function(err, data_new){
                        res.status(200).json({
                            status: 'success',
                            message: 'Service User Info updated',
                            data: data_new
                        });
                    });
                });
        }

    });


};




// Handle update Service info       permet de confirmer que le service a été fait avec succès puis enregistre le nombre d'étoiles
//et le commentaire fait par l'offer de service
exports.confirm_completed_service = function (req, res) {
    var etoiles_donnees_par_offer_service = parseInt(req.body.etoiles_donnees_par_offer_service);
    var commentaire_offer_service = req.body.commentaire_offer_service;


    //je contrôle si le serviceAskForHelp existe
    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function (err, serviceAskForHelp) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (serviceAskForHelp == null) {
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            //j'update status = completed pour le service donné

            var status = "completed";


            //j'update maintenant les infos de serviceAskForHelp
            //j'update les infos de ServiceAskForHelp en enregistrant les étoiles et les commentaires de l'offer de service
            ServiceAskForHelp.findByIdAndUpdate(req.params.serviceaskforhelp_id,
                {
                    status: status,
                    etoiles_donnees_par_offer_service: etoiles_donnees_par_offer_service,
                    commentaire_offer_service: commentaire_offer_service,
                    update_date: new Date(Date.now()).toISOString()
                }, function(err, data){

                    //je mets aussi à jour le status_service = completed dans bidModel et etoiles_donnees_par_offer_service et commentaire_offer_service
                    //Bid.findAndUpdate({serviceAskForHelpId: req.params.serviceaskforhelp_id},
                    //    {
                    //        status_service: status
                    //    },
                    //    function(err, data2){});

                    var myquery = { serviceAskForHelpId: req.params.serviceaskforhelp_id };
                    var newvalues = {$set: {
                        status_service: status,
                        etoiles_donnees_par_offer_service: etoiles_donnees_par_offer_service,
                        commentaire_offer_service: commentaire_offer_service
                        //update_date: new Date(Date.now()).toISOString()
                    }
                    };

                    Bid.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });

                    //je mets à jour le nbre_etoiles de celui choisit dans son profil (userModel, serviceAskForHelpModel, bidModel
                    //je dois recenser les services status = completed fait par le user choisit afin de faire la somme des étoiles données
                    //par l'offer de service et faire la moyenne qui va représenter son nombre d'étoiles dans son profil

                    //recensement des services status = completed fait par le user choisit
                    ServiceAskForHelp.find({user_id_celui_choisit: data.user_id_celui_choisit, status: status}, function(err, new_data){
                        var nbre_services = new_data.length;
                        var somme_etoiles = 0;
                        var moyenne_etoiles = 0;

                        for(var i=0;i<nbre_services;i++){
                            somme_etoiles = somme_etoiles + new_data[i].etoiles_donnees_par_offer_service;
                        }

                        moyenne_etoiles = somme_etoiles/nbre_services;

                        //je mets cette moyenne à jour dans userModel, serviceAskForHelpModel et bidModel
                        //Dans userModel
                        User.findByIdAndUpdate(serviceAskForHelp.user_id_celui_choisit,
                            {
                                nbre_etoiles: moyenne_etoiles,
                                update_date: new Date(Date.now()).toISOString()
                            },
                            function(err, user){});


                        //Dans serviceAskForHelp
                        var myquery = { user_id_celui_choisit: serviceAskForHelp.user_id_celui_choisit };
                        var newvalues = {$set: {
                            nbre_etoiles_celui_choisit: moyenne_etoiles,
                            update_date: new Date(Date.now()).toISOString()
                        }
                        };

                        ServiceAskForHelp.updateMany(myquery, newvalues, function(err, res) {
                            //if (err){}
                        });

                        //Dans bidModel
                        //1- service où il a biddé
                        var myquery = { user_id_celui_qui_bid: serviceAskForHelp.user_id_celui_choisit };
                        var newvalues = {$set: {
                            nbre_etoiles_celui_qui_bid: moyenne_etoiles,
                            update_date: new Date(Date.now()).toISOString()
                        }
                        };

                        Bid.updateMany(myquery, newvalues, function(err, res) {
                            //if (err){}
                        });

                        //2- service où il a créé lui même
                        var myquery = { user_id_celui_qui_offre_service: serviceAskForHelp.user_id_celui_choisit };
                        var newvalues = {$set: {
                            nbre_etoiles_celui_qui_offre_service: moyenne_etoiles,
                            update_date: new Date(Date.now()).toISOString()
                        }
                        };

                        Bid.updateMany(myquery, newvalues, function(err, res) {
                            //if (err){}
                        });


                        //3- mise à jour du status_service à completed dans HelpNotesModel
                        var myquery = { serviceAskForHelpId: req.params.serviceaskforhelp_id };
                        var newvalues = {$set: {
                            status_service: status,
                            update_date: new Date(Date.now()).toISOString()
                        }
                        };

                        HelpNotes.updateMany(myquery, newvalues, function(err, res) {
                            //if (err){}
                        });


                        //4- mise à jour du status_service à completed dans scheduleDatesModel
                        var myquery = { serviceAskForHelpId: req.params.serviceaskforhelp_id };
                        var newvalues = {$set: {
                            status_service: status,
                            update_date: new Date(Date.now()).toISOString()
                        }
                        };

                        ScheduleDates.updateMany(myquery, newvalues, function(err, res) {
                            //if (err){}
                        });


                        //je récupère les nouvelles infos pour renvoyer
                        ServiceAskForHelp.findOne({_id: req.params.serviceaskforhelp_id}, function(err, service){
                            res.status(200).json({
                                status: 'success',
                                message: 'Service User Info updated',
                                data: service
                            });
                        });


                    });


                });
        }

    });
};


// Handle update Service info  permet de confirmer que le service a été fait avec succès
exports.payHelper = function (req, res){
    var pourboire = parseInt(req.body.tips);      //représente la valeur de l'argent en plus que l'offreur de service veut donner
    var service_price = parseInt(req.body.service_price);
    var serviceaskforhelp_id = req.body.serviceaskforhelp_id;

    var device_id = /*"5c61afb55f1e831f77d42609"*/"5c6a917f9522db5b1e2642e5";     //ObjectId de la device ZAR

    var device_name = "";
    var reference_transaction = "";

    var setting_id = "5bdecc273e5adb7c02163c04";

    var methodpaymend_id = "";
    //je cherche le nom de methodPayment
    Methodpayment.findOne({name: "Wallet"}, function(err, methodpayment){
        if(methodpayment == null){
            res.status(401).json({
                status: 'error',
                message: 'MethodPayment not exist'
                //data: serviceAskForHelp
            });
        }
        else{
            methodpaymend_id = methodpayment._id;
        }

    });

    //je prends le nom de device_id
    Device.findById(device_id, function(err, data){
        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'Device not exist'
                //data: serviceAskForHelp
            });
        }
        else{
            device_name = data.name;
        }
    });


    //ServiceAskForHelp.findOne({_id:serviceaskforhelp_id}, function(err, serviceAskForHelp){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Service retrieved',
    //        data: serviceAskForHelp
    //    });
    //});



    //je contrôle si le serviceAskForHelp existe
    ServiceAskForHelp.findById(serviceaskforhelp_id, function (err, serviceAskForHelp) {
        //if (err) {
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });
        //}

        if (serviceAskForHelp == null) {
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            var reference_transaction = serviceAskForHelp.nom_service + Math.random().toString(36).substr(2, 2);

            //je me rassure que l'argent en plus renseigné est bien présent dans le wallet de l'offreur de service
            var user_id = serviceAskForHelp.user_id;
            WalletUser.findOne({user_id: user_id, device_id:device_id}, function(err, wallet){
                if (wallet == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'Insufficient Balance. Recharge your account and try again to pay tips.'
                    });
                }
                else{
                    var amount = wallet.amount;
                    //je vérifie si la valeur du pourboir (tips) est suffisante dans le wallet
                    if(amount - pourboire < 0){
                        //alors solde wallet insuffisant
                        res.status(401).json({
                            status: 'error',
                            message: 'Insufficient Balance. Recharge your account and try again to pay tips.'
                        });
                    }
                    else{
                        var total = service_price + pourboire;

                        //argent_restant_du_service représente l'argent à rembourser à l'offreur de service
                        var argent_restant_du_service = serviceAskForHelp.max_price - service_price;


                        //je prends le pourcentage de Help123 à couper sur le service
                        Settings.findById(setting_id,function(err, settings){
                            var percentageOneService;
                            if(settings == null){
                                percentageOneService = 0;
                            }
                            else{
                                percentageOneService = settings.percentageOneService;
                            }

                            var profit_help123 = (total * percentageOneService) / 100;
                            var somme_a_crediter = total - profit_help123;

                            //res.status(200).json({
                            //    status: 'success',
                            //    message: 'Pay helper successful',
                            //    amount: amount,
                            //    total: total,
                            //    percentage: percentageOneService,
                            //    profit: profit_help123,
                            //    somme: somme_a_crediter
                            //});

                            //je débite l'argent du pourboir dans le wallet de l'offreur de service
                            WalletUser.findByIdAndUpdate(wallet._id,
                                {
                                    amount: wallet.amount - pourboire,
                                    update_date: new Date(Date.now()).toISOString()
                                },
                                function(err, wallet_new){});

                            var transactions = new Transactions();
                            var transactionWalletUser = new TransactionWalletUser();

                            //je save les transactions dans transactionWalletUserModel.js et transactionsModel.js avec type = wallet et sens = sortant
                            transactions.type_transaction = 'wallet';
                            transactions.amount = pourboire;
                            transactions.sens = 'sortant';
                            transactions.user_id = wallet.user_id;
                            transactions.reference_transaction = reference_transaction;
                            transactions.device_id = device_id;
                            transactions.device_name = device_name;
                            transactions.methodpayment_id = methodpaymend_id;
                            transactions.methodpayment_name = "Wallet";
                            transactions.save();

                            transactionWalletUser.user_id = wallet.user_id;
                            transactionWalletUser.amount = pourboire;
                            transactionWalletUser.sens = 'sortant';
                            transactionWalletUser.reference_transaction = reference_transaction;
                            transactionWalletUser.device_id = device_id;
                            transactionWalletUser.device_name = device_name;
                            transactionWalletUser.methodpayment_id = methodpaymend_id;
                            transactionWalletUser.methodpayment_name = "Wallet";
                            transactionWalletUser.save();


                            var transactions = new Transactions();
                            var transactionWalletUser = new TransactionWalletUser();

                            //je crédite le profit_help123 dans le compte de help123
                            //ceci représente les transactions type = profit dans transactionsModel.js
                            transactions.type_transaction = 'profit';
                            transactions.amount = profit_help123;
                            transactions.sens = 'entrant';
                            transactions.user_id = wallet.user_id;
                            transactions.reference_transaction = reference_transaction;
                            transactions.device_id = device_id;
                            transactions.device_name = device_name;
                            transactions.methodpayment_id = methodpaymend_id;
                            transactions.methodpayment_name = "Wallet";
                            transactions.save();


                            //je rembourse le reste d'argent de l'offreur de service dans son wallet ayant fait le reliquat sur le montant max_service et la proposition du prix du bidder
                            if (argent_restant_du_service > 0){
                                if(argent_restant_du_service != 0){
                                    WalletUser.findByIdAndUpdate(wallet._id,
                                        {
                                            amount: wallet.amount + argent_restant_du_service,
                                            update_date: new Date(Date.now()).toISOString()
                                        },
                                        function(err, wallet_new){});

                                    var transactions = new Transactions();
                                    var transactionWalletUser = new TransactionWalletUser();

                                    var reference_transaction_new = serviceAskForHelp.nom_service + Math.random().toString(36).substr(2, 2);

                                    //je save les transactions dans transactionWalletUserModel.js et transactionsModel.js avec type = wallet et sens = sortant
                                    transactions.type_transaction = 'wallet';
                                    transactions.amount = argent_restant_du_service;
                                    transactions.sens = 'entrant';
                                    transactions.user_id = wallet.user_id;
                                    transactions.reference_transaction = reference_transaction_new;
                                    transactions.device_id = device_id;
                                    transactions.device_name = device_name;
                                    transactions.methodpayment_id = methodpaymend_id;
                                    transactions.methodpayment_name = "Wallet";
                                    transactions.save();

                                    transactionWalletUser.user_id = wallet.user_id;
                                    transactionWalletUser.amount = argent_restant_du_service;
                                    transactionWalletUser.sens = 'entrant';
                                    transactionWalletUser.reference_transaction = reference_transaction_new;
                                    transactionWalletUser.device_id = device_id;
                                    transactionWalletUser.device_name = device_name;
                                    transactionWalletUser.methodpayment_id = methodpaymend_id;
                                    transactionWalletUser.methodpayment_name = "Wallet";
                                    transactionWalletUser.save();
                                }
                            }


                            var user_id_celui_choisit = serviceAskForHelp.user_id_celui_choisit;



                            //je crédite le wallet du user qui doit recevoir l'argent du service qu'il a rendu
                            //pour cela je prends d'abord son ancien solde
                            WalletUser.findOne({user_id:user_id_celui_choisit}, function(err, wallet_celui_choisit){

                                //res.status(200).json({
                                //    status: 'success',
                                //    message: 'Pay helper successful',
                                //    user_id: user_id_celui_choisit,
                                //    data: wallet_celui_choisit
                                //});


                                if(wallet_celui_choisit == null){
                                    //alors je crédite le compte du user
                                    var wallet = new WalletUser();
                                    wallet.user_id = serviceAskForHelp.user_id_celui_choisit;
                                    wallet.amount = somme_a_crediter;
                                    wallet.device_id = device_id;
                                    wallet.name_device = device_name;
                                    wallet.update_date = new Date(Date.now()).toISOString();
                                    wallet.save();

                                    //puis je save les transactions du user dans transactionsWalletUserModel.js et transactionsModel.js
                                    var transactions = new Transactions();
                                    var transactionWalletUser = new TransactionWalletUser();

                                    var reference_transaction = serviceAskForHelp.nom_service + Math.random().toString(36).substr(2, 2);

                                    transactions.type_transaction = 'wallet';
                                    transactions.amount = somme_a_crediter;
                                    transactions.sens = 'entrant';
                                    transactions.user_id = serviceAskForHelp.user_id_celui_choisit;
                                    transactions.reference_transaction = reference_transaction;
                                    transactions.device_id = device_id;
                                    transactions.device_name = device_name;
                                    transactions.methodpayment_id = methodpaymend_id;
                                    transactions.methodpayment_name = "Wallet";
                                    transactions.save();

                                    transactionWalletUser.user_id = serviceAskForHelp.user_id_celui_choisit;
                                    transactionWalletUser.amount = somme_a_crediter;
                                    transactionWalletUser.sens = 'entrant';
                                    transactionWalletUser.reference_transaction = reference_transaction;
                                    transactionWalletUser.device_id = device_id;
                                    transactionWalletUser.device_name = device_name;
                                    transactionWalletUser.methodpayment_id = methodpaymend_id;
                                    transactionWalletUser.methodpayment_name = "Wallet";
                                    transactionWalletUser.save();

                                    //je change le statut du service à completed
                                    ServiceAskForHelp.findByIdAndUpdate(serviceAskForHelp._id,
                                        {
                                            status: 'completed',
                                            update_date: new Date(Date.now()).toISOString()
                                        },
                                        function(err, service_new){});


                                    res.status(200).json({
                                        status: 'success',
                                        message: 'Pay helper successful',
                                        data: somme_a_crediter
                                    });

                                }
                                else{
                                    WalletUser.findByIdAndUpdate(wallet_celui_choisit._id/*serviceAskForHelp.user_id_celui_choisit*/,
                                        {
                                            amount: wallet_celui_choisit.amount + somme_a_crediter,
                                            update_date: new Date(Date.now()).toISOString()
                                        },
                                        function(err, wallet_new){});

                                    //puis je save les transactions du user dans transactionsWalletUserModel.js et transactionsModel.js
                                    var transactions = new Transactions();
                                    var transactionWalletUser = new TransactionWalletUser();

                                    var reference_transaction = serviceAskForHelp.nom_service + Math.random().toString(36).substr(2, 2);

                                    transactions.type_transaction = 'wallet';
                                    transactions.amount = somme_a_crediter;
                                    transactions.sens = 'entrant';
                                    transactions.user_id = serviceAskForHelp.user_id_celui_choisit;
                                    transactions.reference_transaction = reference_transaction;
                                    transactions.device_id = device_id;
                                    transactions.device_name = device_name;
                                    transactions.methodpayment_id = methodpaymend_id;
                                    transactions.methodpayment_name = "Wallet";
                                    transactions.save();

                                    transactionWalletUser.user_id = serviceAskForHelp.user_id_celui_choisit;
                                    transactionWalletUser.amount = somme_a_crediter;
                                    transactionWalletUser.sens = 'entrant';
                                    transactionWalletUser.reference_transaction = reference_transaction;
                                    transactionWalletUser.device_id = device_id;
                                    transactionWalletUser.device_name = device_name;
                                    transactionWalletUser.methodpayment_id = methodpaymend_id;
                                    transactionWalletUser.methodpayment_name = "Wallet";
                                    transactionWalletUser.save();

                                    //je change le statut du service à completed
                                    ServiceAskForHelp.findByIdAndUpdate(serviceAskForHelp._id,
                                        {
                                            status: 'completed',
                                            update_date: new Date(Date.now()).toISOString()
                                        },
                                        function(err, service_new){});


                                    res.status(200).json({
                                        status: 'success',
                                        message: 'Pay helper successful',
                                        data: somme_a_crediter
                                    });
                                }
                            });
                        });
                    }
                }
            });
        }
    });
};



// permet de GET tous les services du User qui sont en status = in_progress (My Open Helps)
exports.my_open_helps = function (req, res) {

    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien


    //on vérifie si le user existe
    User.findOne({_id: req.params.user_id}, function(err, user){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (user == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });

    //Image_service.get(function(err, image){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services User details loading..',
    //        data: image
    //    });
    //});

    //Image_service.find({serviceAskForHelp_id: '5c34ba16443ef20ee8c6b659'},function(err, image){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services User details loading..',
    //        data: image
    //    });
    //});

    ServiceAskForHelp.find({user_id: req.params.user_id, status: 'in_progress'}, function(err, service){
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        //Pour chaque service_id je vais prendre ses image_service et ses preferred_date_and_time_service
        var result = new Array();
        var services = service;
        var taille_tableau_services = services.length;

        //for(var i=0;i<service.length;i++){
        //    var service_id = service[i]._id;
        //
        //    Image_service.find({serviceAskForHelp_id:service_id}, function(err, image){
        //        Preferred_date_and_time_service.find({serviceAskForHelp_id:service_id}, function(err, preferred_date_and_time_service){
        //            ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id:service_id}, function(err, choixdeliverandbillhelpforaservice){
        //                //res.status(200).json({
        //                //    status: 'success',
        //                //    message: 'Services User details loading..',
        //                //    data: image/*[service[0], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice]*/
        //                //});
        //
        //                result[i] = [service[i], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice];
        //
        //            });
        //
        //            //result[i] = [service[i], image, preferred_date_and_time_service, choixdeliverandbillhelpforaservice];
        //
        //
        //
        //        });
        //    });
        //
        //
        //}
        res.status(200).json({
            status: 'success',
            message: 'My Open Helps loading..',
            data: service
        });

    }).sort(mysort);

};



//permet de get tous les services différents de celui qui est connecté (user) avec pour statut 'in_progress'
exports.recent_helps_requests = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });

    //on renvoit tous les services en cours qui ne sont pas créés par le user connecté
    ServiceAskForHelp.find({user_id:{'$ne':req.params.user_id},status:'in_progress'},function(err, service){

        res.status(200).json({
            status: 'success',
            message: 'My Recents Helps loading..',
            data: service
        });


    }).sort(mysort);

    //var Doc = new Array();
    //var result = [];
    ////v//ar resultat =
    //var i =0;
    //
    //ServiceAskForHelp.find({user_id:{'$eq':req.params.user_id},status:'in_progress'},function(err, Doc){
    //    Doc.forEach(function(myDoc){
    //        //je recherche pour le user correspondant
    //        User.find({_id: myDoc.user_id}, function(err, user){
    //            //result[i] = new User();
    //            result.push(user);
    //            //i = i+1;
    //
    //            //res.status(401).json({
    //            //    status: 'success',
    //            //    message: 'Services details loading..',
    //            //    data: user/*myDoc.user_id*/
    //            //});
    //
    //            var resultat = [myDoc, result];
    //
    //            res.status(401).json({
    //                status: 'success',
    //                message: 'Services details loading..',
    //                data: resultat/*resultat[1][0][0]*//*myDoc.user_id*/
    //            });
    //
    //        });
    //
    //        //res.status(401).json({
    //        //    status: 'success',
    //        //    message: 'Services details loading..',
    //        //    data: result/*myDoc.user_id*/
    //        //});
    //
    //    });
    //
    //
    //});




    //var result = ServiceAskForHelp.aggregate([
    //    {
    //        $lookup:
    //        {
    //            from: "User",
    //            localField: "user_id",
    //            foreignField: "_id",
    //            as: "userarray"
    //        }
    //    }
    //], function(err, resultat){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services details loading..',
    //        data: resultat
    //    });
    //
    //    //console.log(err, resultat)
    //
    //});


};


//permet de get tous les services d'un user sans distinction de statut
exports.other_recent_helps_requests = function(req, res) {
	var mysort = { create_date: -1 };

	ServiceAskForHelp.find({user_id:req.params.user_id}, function(err, service){
		res.status(200).json({
			status: 'success',
			message: 'My recents helps loading ...',
			data: service
		});
	}).sort(mysort);

};

//permet de get tous les services avec pour status = in_progress (ceci c'est pour les users non connectés)
exports.recent_helps_requests_for_non_connected_users = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien


    //on renvoit tous les services en cours
    ServiceAskForHelp.find({status:'in_progress'},function(err, service){

        res.status(200).json({
            status: 'success',
            message: 'The Recents Helps loading..',
            nombre_service: service.length,
            data: service
        });


    }).sort(mysort);

    //var Doc = new Array();
    //var result = [];
    ////v//ar resultat =
    //var i =0;
    //
    //ServiceAskForHelp.find({user_id:{'$eq':req.params.user_id},status:'in_progress'},function(err, Doc){
    //    Doc.forEach(function(myDoc){
    //        //je recherche pour le user correspondant
    //        User.find({_id: myDoc.user_id}, function(err, user){
    //            //result[i] = new User();
    //            result.push(user);
    //            //i = i+1;
    //
    //            //res.status(401).json({
    //            //    status: 'success',
    //            //    message: 'Services details loading..',
    //            //    data: user/*myDoc.user_id*/
    //            //});
    //
    //            var resultat = [myDoc, result];
    //
    //            res.status(401).json({
    //                status: 'success',
    //                message: 'Services details loading..',
    //                data: resultat/*resultat[1][0][0]*//*myDoc.user_id*/
    //            });
    //
    //        });
    //
    //        //res.status(401).json({
    //        //    status: 'success',
    //        //    message: 'Services details loading..',
    //        //    data: result/*myDoc.user_id*/
    //        //});
    //
    //    });
    //
    //
    //});




    //var result = ServiceAskForHelp.aggregate([
    //    {
    //        $lookup:
    //        {
    //            from: "User",
    //            localField: "user_id",
    //            foreignField: "_id",
    //            as: "userarray"
    //        }
    //    }
    //], function(err, resultat){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services details loading..',
    //        data: resultat
    //    });
    //
    //    //console.log(err, resultat)
    //
    //});


};



//permet de get le nbre de My Open Helps, le nbre de My Bidding Helps, le nbre de My Completed Helps et le nbre de My Closed Helps d'un user
exports.nbre_helps_dashboard = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });

    //pour avoir le nbre_my_open_helps
    ServiceAskForHelp.find({user_id: req.params.user_id, status: 'in_progress'}, function(err, service){
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        //Pour chaque service_id je vais prendre ses image_service et ses preferred_date_and_time_service
        var result = new Array();
        var services = service;
        var nbre_my_open_helps = services.length;

        //pour avoir nbre_my_bidding_helps
        Bid.find({user_id_celui_qui_bid: req.params.user_id/*, status_service: 'in_progress'*/}, function(err, bid){
            var nbre_my_bidding_helps = bid.length;

            //pour avoir nbre_my_completed_helps
            ServiceAskForHelp.find({user_id: req.params.user_id, status: 'completed'}, function(err, completed_help){
                var nbre_my_completed_helps = completed_help.length;

                //pour avoir nbre_my_closed_helps
                ServiceAskForHelp.find({user_id_celui_choisit: req.params.user_id, status: 'completed'}, function(err, closed_help){
                    var nbre_my_closed_helps = closed_help.length;


                    res.status(200).json({
                        status: 'success',
                        message: 'Dashboard loading..',
                        nbre_my_open_helps: nbre_my_open_helps,
                        nbre_my_bidding_helps: nbre_my_bidding_helps,
                        nbre_my_completed_helps: nbre_my_completed_helps,
                        nbre_my_closed_helps: nbre_my_closed_helps
                    });


                });


            });


        });




    }).sort(mysort);





};


//permet de renvoyer les services biddés par un user (My Bidding Helps)
exports.my_bidding_help = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });


    //je recense tous les bids où le user a biddé
    Bid.find({user_id_celui_qui_bid: req.params.user_id/*, status_service: 'in_progress'*/}, function(err, bid_service){
        var result = new Array();
        var code;
        //var service_to_save = new ServiceAskForHelpSave();
        //
        //for (var i = 0; i < bid_service.length; i++){
        //    //pour chaque valeur de bid retrouvé je dois stocké les infos du service dans le tableau
        //    ServiceAskForHelp.find({_id: bid_service[i].serviceAskForHelpId, status: 'in_progress'}, function(err, service){
        //        code = Math.random().toString(36).substr(2, 2);
        //
        //        //je vais save le service avec le code
        //        service_to_save.code =
        //
        //
        //        result[i] = service;
        //        //result[i].nom_service = service.nom_service;
        //
        //
        //
        //    });
        //}

        res.status(200).json({
            status: 'success',
            message: 'My Bidding Helps loading ...',
            data: bid_service
        });

    }).sort(mysort);

};





