/**
 * Created by christophe on 18/09/24.
 */

// Import contact model
Contact = require('./../Models/contactModel');


// Handle index actions     permet de get tous les contacts
exports.index = function (req, res) {
    Contact.get(function (err, contacts) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Contacts retrieved successfully",
            data: contacts          //contacts avec s ici représente la base de données contacts
        });
    });
};



// Handle create contact actions    permet de save(post) un contact
exports.new = function (req, res) {
    var contact = new Contact();
    contact.name = req.body.name ? req.body.name : contact.name;
    contact.gender = req.body.gender;
    contact.email = req.body.email;
    contact.phone = req.body.phone;
// save the contact and check for errors
    contact.save(function (err) {
         if (err)
             res.json(err);
        res.json({
            message: 'New contact created!',
            data: contact
        });
    });
};



// Handle view contact info   permet de GET un contact à partir de son Id
exports.view = function (req, res) {
    Contact.findById(req.params.contact_id, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            message: 'Contact details loading..',
            data: contact
        });
    });
};



// Handle update contact info  permet d'update un contact
exports.update = function (req, res) {
    Contact.findById(req.params.contact_id, function (err, contact) {
        if (err)
            res.send(err);
        contact.name = req.body.name ? req.body.name : contact.name;
        contact.gender = req.body.gender;
        contact.email = req.body.email;
        contact.phone = req.body.phone;
// save the contact and check for errors
        contact.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'Contact Info updated',
                data: contact
            });
        });
    });
};



// Handle delete contact  permet de supprimer un contact
exports.delete = function (req, res) {
    Contact.remove({
        _id: req.params.contact_id
    }, function (err, contact) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'Contact deleted'
        });
    });
};


// Handle update user info  permet d'update un user
exports.updateNew = function (req, res) {
//exports.updateProfile = async (req, res) => {

    Contact.findByIdAndUpdate(req.params.contact_id,
        {
            name: req.body.name,
            email: req.body.email,
            gender: req.body.gender,
            phone: req.body.phone
        },
        function (err, user) {
            if (err)
                res.json(err);
            res.json({
                message: 'User Info updated',
                data: user
            });
        });
}