/**
 * Created by christophe on 18/10/22.
 */


// Import passwordreset model
Passwordreset = require('./../Models/passwordresetsModel');
User = require('./../Models/userModel');
var randomInt = require('random-int');      //pour faire des random sur les entiers
var nodemailer = require('nodemailer');     //pour l'envoi des mails
//const bcrypt = require('bcrypt');    //pour crypter les passwords
//const saltRounds = 10;      //constante pour la taille du password crypté

var crypto = require('crypto');


// Handle index actions     permet de get tous les passwordresets
exports.index = function (req, res) {
    Passwordreset.get(function (err, passwordresets) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server',
            });
        }
        res.status(200).json({
            status: "success",
            message: "Passwordresets retrieved successfully",
            data: passwordresets
        });
    });
};



exports.eeee = function(email, nombre, code){
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            host: "smtp.gmail.com",
            auth: {
              user: "moo.sdkgames@gmail.com", // generated ethereal user
              pass: "ajokfabdzfheudpq" // generated ethereal password
            }
          });

        var mailOptions = {
          from: "moo.sdkgames@gmail.com",//'help123backend',
          to: email,
          subject: 'Account activation',
          text: 'Use this code to reset your password : ',
          html: 'Use this code to reset your password : ' + code
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            res.status(401).json({
                status: 'error',
                message: 'Device not exist'
            });
          } else {
            console.log('Email sent: ' + info.response);
            res.json({
                status: DOMAIN,
                message: 'Users received successfully',
                data: "ok"  
            });
          }
          transporter.close();
        });
}


// Handle create passwordresets actions    permet d'envoyer le code au user
exports.sendCodeUser = function (req, res) {
    var passwordreset = new Passwordreset();

    var email = req.body.email;

    //je me rassure si le user existe vraiment
    let userNew = User.findOne({email : email.toLowerCase()}, function(err, data){
        if(err)
            res.status(401).json({
                status: 'error',
                message: 'Email not exist'
            });
        //res.json({data: data._id})
        if (data == null){          //pour dire si le user n'existe pas
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            })
        }

        User.findByIdAndUpdate(data._id, {
                confirmed: true,
                suspended: false
            },
             function (err, user1) {
                    if (err)
                       res.status(401).json({
                           status: 'error',
                            message: 'There was an error'
                       });
                                               
            });

        //dans ce cas le user existe et on doit save son code
        //res.json({data: data});
        //alors on génère le code
        //var code = randomInt(4);
        var code = Math.floor(Math.random() * 10000);
        var statusvalidity = 'valid';
        var confirmation_code = 0;   //pour que le user n'a pas encore confirmé le code

        passwordreset.user_id = data._id;/*req.body.user_id;*/
        passwordreset.code = code;
        passwordreset.confirmation_code = confirmation_code;
        passwordreset.statusvalidity = statusvalidity;
        passwordreset.update_date = new Date(Date.now()).toISOString();

        /****** Envoyer le code par mail *************/
       

       process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            host: "smtp.gmail.com",
            auth: {
              user: "moo.sdkgames@gmail.com", // generated ethereal user
              pass: "ajokfabdzfheudpq" // generated ethereal password
            }
          });

        var mailOptions = {
          from: "moo.sdkgames@gmail.com",//'help123backend',
          to: email.toLowerCase(),
          subject: 'Account activation',
          text: 'Use this code to reset your password : ',
          html: 'Use this code to reset your password : ' + code
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            res.status(401).json({
                status: 'error',
                message: 'Device not exist'
            });
          } else {
            console.log('Email sent: ' + info.response);
            res.json({
                status: DOMAIN,
                message: 'Users received successfully',
                data: "ok"  
            });
          }
          transporter.close();
        });

        

        // var transporter = nodemailer.createTransport({
        //   service: 'gmail',
        //   auth: {
        //     user: 'help123backend@gmail.com',
        //     pass: '123help@2018'
        //   }
        // });

        // var mailOptions = {
        //   from: 'help123backend@gmail.com',
        //   to: email.toLowerCase(),
        //   subject: 'Code Password Reset',
        //   text: 'Use this code to reset your password : ' + code
        // };

        // transporter.sendMail(mailOptions, function(error, info){
        //   if (error) {
        //     console.log(error);
        //   } else {
        //     console.log('Email sent: ' + info.response);
        //   }
        // });


        /********** fin envoi mail  **************************/

        // save the passwordreset and check for errors
        passwordreset.save(function (err) {
            if (err)
                res.status(500).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });
            res.status(200).json({
                status: 'success',
                message: 'New passwordreset created!',
                data: passwordreset
            });
        });

    });

};



// Handle view passwordreset info   permet de GET un passwordreset à partir de son Id
exports.view = function (req, res) {
    Passwordreset.findById(req.params.passwordresets_id, function (err, passwordreset) {
        if (err){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId passwordreset not exist'
            });
        }

        if (passwordreset == null){
           res.status(401).json({
                status: 'error',
                message: 'ObjectId passwordreset not exist'
            }); 
        }
        res.status(200).json({
            status: 'success',
            message: 'Passwordreset details loading..',
            data: passwordreset
        });
    });
};



// permet d'update confirmation_code et statusvalidity lorsque le user a renseigné le bon code
exports.updateNew = function (req, res) { 

    //res.send(req.params.code);
    Passwordreset.findOne({$and : [{code : req.body.code}, {confirmation_code : 0}]}, function(err, passwordreset){
        if(err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        //res.json({data: data._id})
        if (passwordreset != null){          //pour dire si que cet adresse email est déjà utilisée
            Passwordreset.findByIdAndUpdate(passwordreset._id, {
                confirmation_code: 1,   //pour dire que le code a été confirmé
                statusvalidity: 'invalid',  //pour rendre le code invalide
                update_date: new Date(Date.now()).toISOString()
            },
             function (err, user1) {
                    if (err)
                       res.status(401).json({
                           status: 'error',
                            message: 'There was an error'
                       });
                        else{
                            res.status(200).json({
                                status: 'success',
                                message: 'Update successful',
                                data: user1
                            });
                        }                               
            });

        }
        else{
            res.status(401).json({
               status: 'error',
                message: 'Code not exist'
           });
        }
    });

    // let num = Passwordreset.findOneAndUpdate({code : req.body.code},
    //     {
    //     confirmation_code: 1,   //pour dire que le code a été confirmé
    //     statusvalidity: 'invalid',  //pour rendre le code invalide
    //     update_date: new Date(Date.now()).toISOString()
    //     },
    //     function(err, data){
    //     if(err)
    //         res.status(401).json({
    //             status: 'error',
    //             message: 'Code not exist'
    //         });

    //     if(data != null){
    //         //je capture en BD les nouvelles données enregistrées
    //         Passwordreset.findOne({code: req.body.code}, function(err, passwordreset){
    //             res.status(200).json({
    //                 status: 'success',
    //                 message: 'Update successful',
    //                 data: passwordreset
    //             });
    //         });
    //     }
    //     else{
    //         res.status(401).json({
    //             status: 'error',
    //             message: 'Code not exist'
    //         });
    //     }

        //let ObjectId_pass = data[0]._id;


        //res.send(ObjectId_pass);
        //Passwordreset.findByIdAndUpdate(ObjectId_pass,
        //    {
        //        confirmation_code: 1,   //pour dire que le code a été confirmé
        //        statusvalidity: 'invalid'  //pour rendre le code invalide
        //    },
        //    function (err, passwordreset) {
        //        if (err)
        //            res.json(err);
        //        res.json({
        //            message: 'Passwordreset Info updated',
        //            data: passwordreset
        //        });
        //    });
    // });


}



// Handle delete passwordreset  permet de supprimer un passwordreset
exports.delete = function (req, res) {
    Passwordreset.remove({
        _id: req.params.passwordreset_id
    }, function (err, passwordreset) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'ObjectId passwordreset not exist'
            });
        res.status(200).json({
            status: "success",
            message: 'Passwordreset deleted'
        });
    });
};



//permet de save le nouveau password du user
exports.newPassword = function (req, res) {
    var key = 'salt_from_the_user_document';
    var plaintext = req.body.new_password;      //nouveau password à crypter
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var decipher = crypto.createDecipher('aes-256-cbc', key);

    cipher.update(plaintext, 'utf8', 'base64');
    var encryptedPassword = cipher.final('base64')

    decipher.update(encryptedPassword, 'base64', 'utf8');
    var decryptedPassword = decipher.final('utf8');
    var ladate = new Date();
    ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();

    User.findByIdAndUpdate(req.params.user_id,
        {
            password: encryptedPassword, /*req.body.password*/ /*bcrypt.hashSync(req.body.new_password, saltRounds),*/
            pwdFailed: false,
            number_connection_failed: 0,
            date_connection_failed: ladate,
            update_date: new Date(Date.now()).toISOString()
        },
        function (err, user) {
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });

            if(user != null){
                //je capture en BD les nouvelles données enregistrées
                User.findOne({email: user.email}, function(err, data){
                    res.status(200).json({
                        status: 'success',
                        message: 'Password updated',
                        data: data
                    });
                });
            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            }
        });
};

