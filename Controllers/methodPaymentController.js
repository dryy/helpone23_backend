/**
 * Created by christophe on 19/02/15.
 */

// Import method payment model
MethodPayment = require('./../Models/methodPaymentModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');


// Handle index actions     permet de get toutes les méthodes de paiement
exports.index = function (req, res) {
    MethodPayment.get(function (err, methodpayments) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Devices retrieved successfully",
            data: methodpayments
        });
    });
};



// Handle create methodPayment actions    permet de save(post) une méthode de paiement
exports.new = function (req, res) {
    var methodpayment = new MethodPayment();
    methodpayment.name = req.body.name;
    methodpayment.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le name de la méthode de paiement là n'existe pas encore
    MethodPayment.findOne({name:req.body.name}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null) { //dans ce cas name methode paiement existe déjà
            res.status(401).json({
                status: 'error',
                message: 'This payment method name already exist'
            });
        }

        // save the methodpayment name and check for errors
        methodpayment.save(function (err) {
            if (err) {
                res.status(500).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });
            }


            res.status(200).json({
                status: 'success',
                message: 'New payment method created!',
                data: methodpayment
            });
        });
    });
};



// Handle view methodpayment info   permet de GET une méthode de paiement à partir de son Id
exports.view = function (req, res) {
    MethodPayment.findById(req.params.methodpayment_id, function (err, methodpayment) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'MethodPayment ObjectId not exist'
            });

        if (methodpayment == null){
            res.status(401).json({
                status: 'error',
                message: 'MethodPayment ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'MethodPayment details loading..',
            data: methodpayment
        });
    });
};



// Handle update method payment info  permet d'update une méthode de paiement
exports.update = function (req, res) {

    //je me rassure si ce nom de méthode de paiement n'existe pas dans le document methodPaymentModel
    MethodPayment.findOne({name: req.body.name}, function(err, data1){

        if (data1 != null){

            if(data1.name == req.body.name){
                //on ne fait rien et on retourne juste les données
                //je capture en BD les nouvelles données enregistrées
                MethodPayment.findOne({_id: req.params.methodpayment_id}, function(err, data_new){
                    res.status(200).json({
                        status: 'success',
                        message: 'MethodPayment Info updated',
                        data: data_new
                    });
                });
            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'MethodPayment name already exist'
                });
            }
        }
        else{

            MethodPayment.findByIdAndUpdate(req.params.methodpayment_id,
                {
                    name: req.body.name,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, methodpayment) {
                    if (err)
                        res.status(401).json({
                            status: 'error',
                            message: 'MethodPayment ObjectId not exist'
                        });


                    //je dois me rassurer que nom method payment n'existe pas déjà
                    if(methodpayment != null){
                        if(methodpayment.name == req.body.name){
                            //on ne fait rien et on retourne juste les données
                            //je capture en BD les nouvelles données enregistrées
                            MethodPayment.findOne({_id: req.params.methodpayment_id}, function(err, data_new){
                                res.status(200).json({
                                    status: 'success',
                                    message: 'MethodPayment Info updated',
                                    data: data_new
                                });
                            });
                        }
                        else{

                            //je me rassure si ce nom de méthode de paiement n'existe pas dans le document methodPaymentModel
                            MethodPayment.findOne({name: req.body.name}, function(err, data1){

                                /*if (data1 != null){
                                 res.status(401).json({
                                 status: 'error',
                                 message: 'Language name already exist'
                                 });
                                 }*/
                                //else{

                                //ici je update le nom de la méthode de paiement dans MethodPaymentModel, dans transactionsModel et dans tansactionWalletUserModel
                                var myquery = { _id: req.params.methodpayment_id };
                                var newvalues = {$set: {name: req.body.name} };

                                MethodPayment.updateMany(myquery, newvalues, function(err, res) {
                                    //if (err){}
                                });

                                //j'update aussi dans transactionsModel
                                var myquery3 = {methodpayment_id: req.params.methodpayment_id};
                                var newvalues3 = {$set: {methodpayment_name: req.body.name}};

                                Transactions.updateMany(myquery3, newvalues3, function(err, res){});

                                //j'update aussi dans transactionWalletUserModel
                                var myquery4 = {methodpayment_id: req.params.methodpayment_id};
                                var newvalues4 = {$set: {methodpayment_name: req.body.name}};

                                TransactionWalletUser.updateMany(myquery4, newvalues4, function(err, res){});


                                //je capture en BD les nouvelles données enregistrées
                                MethodPayment.findOne({_id: req.params.methodpayment_id}, function(err, data){
                                    res.status(200).json({
                                        status: 'success',
                                        message: 'MethodPayment Info updated',
                                        data: data
                                    });
                                });
                                //}

                            });
                        }
                    }
                });

        }

    });

};



// Handle delete methodpayment   permet de supprimer une méthode de paiement
exports.delete = function (req, res) {
    //je dois me rassurer que cette méthode de paiement n'est pas encore utilisée dans les transactions portemonnaie et transactions du user
    Transactions.findOne({methodpayment_id: methodpayment._id}, function(err, methodpayments){
        if (err){
            res.status(401).json({
                status: 'error',
                message: 'MethodPayment is already used in User transactions. Impossible to delete !!!'
            });
        }

        if (methodpayments != null){ //ça veut dire que la méthode de paiement a déjà été utilisée dans les transactions portemonnaie et transactions du user
            res.status(401).json({
                status: 'error',
                message: 'MethodPayment is already used for an user. Impossible to delete !!!'
            });
        }
        else{
            //res.status(200).json({
            //    status: 'success',
            //    message: 'MethodPayment deleted'
            //});

                MethodPayment.remove({
                    _id: req.params.methodpayment_id
                }, function (err, methodpayment) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if(methodpayment == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId MethodPayment not exist'
                        });
                    }
                    else{
                            res.status(200).json({
                                status: 'success',
                                message: 'MethodPayment deleted'
                            });
                    }
                });
        }
    });
};


