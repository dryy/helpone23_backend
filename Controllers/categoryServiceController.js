/**
 * Created by christophe on 18/09/26.
 */

// Import categoryservice model
categoryService = require('./../Models/categoryServiceModel');
service = require('./../Models/serviceModel');
Souscategory = require('./../Models/souscategoryModel');

// Handle index actions     permet de get toutes les catégories de services
exports.index = function (req, res) {
    categoryService.get(function (err, categoryservices) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Categories Services retrieved successfully",
            data: categoryservices
        });
    });
};



// Handle create categories services actions    permet de save(post) une catégorie de service
exports.new = function (req, res) {
    var categoryservice = new categoryService();
    categoryservice.categoryName = req.body.categoryName;
    categoryservice.logo = req.body.logo;
    categoryservice.description = req.body.description;
    categoryservice.update_date = new Date(Date.now()).toISOString();


    //on se rassure si categoryName n'existe pas encore dans categoryService
    categoryService.findOne({categoryName:req.body.categoryName}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null) { //dans ce cas categoryName n'existe pas encore pour la categoryService
            res.status(401).json({
                status: 'error',
                message: 'categoryName already exist in this categoryService'
            });
        }
        else{
            // save the categoryName and check for errors
            categoryservice.save(function (err) {
                if (err)
                    res.status(500).json({
                        status: 'error',
                        message: 'Internal Problem Server'
                    });
                res.status(200).json({
                    status: 'success',
                    message: 'New categoryName created!',
                    data: categoryservice
                });
            });
        }
    });
};



// Handle view categoryService info   permet de GET une catégorie de service à partir de son Id
exports.view = function (req, res) {
    categoryService.findById(req.params.category_id, function (err, categoryservice) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(categoryservice == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });
        }
        else{
            res.json({
                status: 'success',
                message: 'CategoryService details loading..',
                data: categoryservice
            });
        }
    });
};



// Handle update categoryservice info   permet d'update une catégorie de service
exports.update = function (req, res) {
    //on se rassure si categoryName n'existe pas encore dans la categoryService
    categoryService.findOne({categoryName:req.body.categoryName}, function(err, data){
        if(err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(data == null){   //dans ce cas le categoryName n'existe pas encore dans la categoryService
            //on fait alors un update du categoryName
            categoryService.findByIdAndUpdate(req.params.category_id,
                {
                    categoryName: req.body.categoryName,
                    logo: req.body.logo,
                    description: req.body.description,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, categoryservice) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if (categoryservice == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId categoryService not exist'
                        });
                    }

                    //je change tous les noms de categoryName dans Services ayant pour Id ce nom de catégorie
                    var myquery = { categoryServiceId: req.params.category_id };
                    var newvalues = {$set: {categoryName: req.body.categoryName} };

                    service.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });

                    //je change aussi les nouveaux paramètres de Category dans SouscategoryModel
                    var myquery = { categoryServiceId: req.params.category_id};
                    var newvalues = {$set: {
                                                categoryName: req.body.categoryName,
                                                description_category: req.body.description,
                                                logo_category: req.body.logo
                    }};

                    Souscategory.updateMany(myquery, newvalues, function(err, res){

                    });

                    //je capture en BD ls nouvelles données enregistrées
                    categoryService.findOne({_id: req.params.category_id}, function(err, categoryservice){
                        res.status(200).json({
                            status: 'success',
                            message: 'Category Service Info updated',
                            data: categoryservice
                        });
                    });

                    /*res.status(200).json({
                     status: 'success',
                     message: 'Category Service updated',
                     data: categoryservice
                     });*/
                });
        }
        else if(data._id == req.params.category_id){ //alors on fait l'update c'est le même
            //on fait alors un update du categoryName
            categoryService.findByIdAndUpdate(req.params.category_id,
                {
                    categoryName: req.body.categoryName,
                    logo: req.body.logo,
                    description: req.body.description,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, categoryservice) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if (categoryservice == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId categoryService not exist'
                        });
                    }

                    //je change tous les noms de categoryName dans Services ayant pour Id ce nom de catégorie
                    var myquery = { categoryServiceId: req.params.category_id };
                    var newvalues = {$set: {categoryName: req.body.categoryName} };

                    service.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });

                    //je change aussi les nouveaux paramètres de Category dans SouscategoryModel
                    var myquery = { categoryServiceId: req.params.category_id};
                    var newvalues = {$set: {
                        categoryName: req.body.categoryName,
                        description_category: req.body.description,
                        logo_category: req.body.logo
                    }};

                    Souscategory.updateMany(myquery, newvalues, function(err, res){

                    });

                    //je capture en BD ls nouvelles données enregistrées
                    categoryService.findOne({_id: req.params.category_id}, function(err, categoryservice){
                        res.status(200).json({
                            status: 'success',
                            message: 'Category Service Info updated',
                            data: categoryservice
                        });
                    });

                    /*res.status(200).json({
                     status: 'success',
                     message: 'Category Service updated',
                     data: categoryservice
                     });*/
                });
        }
        else{
            res.status(401).json({
                status: 'error',
                message: 'categoryName already exist in this categoryService'
            });
        }
    });
};



// Handle delete categoryservice  permet de supprimer une catégorie de service
exports.delete = function (req, res) {
    categoryService.remove({
        _id: req.params.category_id
    }, function (err, categoryservice) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(categoryservice == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryService not exist'
            });
        }
        else{

            //je dois me rassurer que cette catégorie n'est pas encore utilisée dans un service
            //service.findOne({categoryServiceId: req.params.category_id}, function(err, service_data){
            //        if (err){
            //           res.status(401).json({
            //                status: 'error',
            //                message: 'Category Service is already used for a service. Impossible to delete !!!'
            //            });
            //        }
            //
            //        if (service_data != null){ //ça veut dire que la catégorie a déjà été utilisée pour un service
            //            res.status(401).json({
            //                status: 'error',
            //                message: 'Category Service is already used for a service. Impossible to delete !!!'
            //            });
            //        }
            //        else{
            //                res.status(200).json({
            //                status: "success",
            //                message: 'Category Service deleted'
            //                 });
            //        }
            //});

            //je dois me rassurer que cette catégorie n'est pas encore utilisée pour une sous catégorie
            Souscategory.findOne({categoryServiceId: req.params.category_id}, function(err, souscategory_data){
                if (err){
                    res.status(401).json({
                        status: 'error',
                        message: 'Category Service is already used for a sousCategory. Impossible to delete !!!'
                    });
                }

                if (souscategory_data != null){ //ça veut dire que la catégorie a déjà été utilisée pour un service
                    res.status(401).json({
                        status: 'error',
                        message: 'Category Service is already used for a sousCategory. Impossible to delete !!!'
                    });
                }
                else{
                    res.status(200).json({
                        status: "success",
                        message: 'Category Service deleted'
                    });
                }
            });


        }
    });
};

