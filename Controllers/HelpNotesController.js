/**
 * Created by christophe on 19/03/01.
 */

// Import HelpNotes model
Device = require('./../Models/deviceModel');
WalletUser = require('./../Models/walletUserModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
HelpNotes = require('./../Models/HelpNotesModel');
AnsHelp = require('./../Models/AnsHelpModel');
User = require('./../Models/userModel');
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');

var nodemailer = require('nodemailer');     //pour l'envoi des mails
var validator = require('validator');



exports.enregistrer = function(req, res){
    var helpnotes = new HelpNotes();
    var receiver = "";
    var input = validator.isEmail(req.body.receiver);

    if(input == true){  //alors dans ce cas c'est l'email qui est saisi
        User.findOne({email : req.body.receiver.toLowerCase()}, function(err, data){
            if(err)
                res.status(401).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });

            if(data != null){
                receiver = data._id;
            }
            else{
                res.status(200).json({
                    status: 'error',
                    message: 'Your message coulden\'t be sent, Receiver Not exists',
                    data: 'any'
                });
            }    

            var helpnotes = new HelpNotes();
            helpnotes.user_id_celui_qui_ecrit = req.body.senderId;
            helpnotes.user_id_celui_qui_recoit = data._id;
            helpnotes.username_celui_qui_ecrit = req.body.username;
            helpnotes.username_celui_qui_recoit = data.username;
            helpnotes.subjest_note_help = req.body.subject;
            helpnotes.note_help = req.body.message;
            helpnotes.update_date = new Date(Date.now()).toISOString();

            helpnotes.save(function(err, helpnotes){
                if(err){
                    res.status(401).json({
                        status: 'error',
                        message: 'The message couldn\'t be sent'
                    });
                }
                res.status(200).json({
                    status: 'success',
                    message: 'Your message has been succesfully sent!',
                    data: helpnotes
                });

            });
        });
    }
    else{
        User.findOne({surname : req.body.receiver}, function(err, data){
            if(err)
                res.status(401).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });

            if(data != null){
                receiver = data._id;
            }
            else{
                res.status(200).json({
                    status: 'error',
                    message: 'Your message coulden\'t be sent, Receiver Not exists',
                    data: 'any'
                });
            }    

            var helpnotes = new HelpNotes();
            helpnotes.user_id_celui_qui_ecrit = req.body.senderId;
            helpnotes.user_id_celui_qui_recoit = data._id;
            helpnotes.username_celui_qui_ecrit = req.body.username;
            helpnotes.username_celui_qui_recoit = data.surname;
            helpnotes.subjest_note_help = req.body.subject;
            helpnotes.note_help = req.body.message;
            helpnotes.update_date = new Date(Date.now()).toISOString();

            helpnotes.save(function(err, helpnotes){
                if(err){
                    res.status(401).json({
                        status: 'error',
                        message: 'The message couldn\'t be sent'
                    });
                }
                res.status(200).json({
                    status: 'success',
                    message: 'Your message has been succesfully sent!',
                    data: helpnotes
                });

            });
        });
    }
};


//permet de GET un HelpNote à partir de l'Id de l'utilisateur qui a envoyé le message
exports.getNotes = function(req, res){
    HelpNotes.find({user_id_celui_qui_ecrit: req.body.userId}, function (err, helpnote) {
        if (helpnote == null){
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'HelpNote details loading..',
            data: helpnote
        });
    });
};


exports.delete = function (req, res) {
    HelpNotes.deleteOne({
        _id: req.params.helpnote_id
    }, function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Note not exist'
            });
        res.status(200).json({
            status: "success",
            message: 'Note deleted successfully'
        });
    });
};

exports.getReceivedNotes = function(req, res){
    HelpNotes.find({user_id_celui_qui_recoit: req.body.userId}, function (err, helpnote) {
        if (helpnote == null){
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'HelpNote details loading..',
            data: helpnote
        });
    });
};

// Handle create helpnotes actions    permet de save(post) une note help par rapport à un service (interface Bid Awarded  - Add Notes.png)
exports.new = function (req, res) {
    var helpnotes = new HelpNotes();
    helpnotes.serviceAskForHelpId = req.body.serviceAskForHelpId;
    helpnotes.user_id_celui_qui_ecrit = req.body.user_id;
    //helpnotes.username_celui_qui_ecrit = req.body.username_celui_qui_ecrit;
    //helpnotes.firstname_celui_qui_ecrit = req.body.firstname_celui_qui_ecrit;
    helpnotes.note_help = req.body.note_help;
    helpnotes.status_help_note = req.body.status_help_note;             //status_help_note = selected, awarded, completed 
    helpnotes.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le service existe
    ServiceAskForHelp.findById(req.body.serviceAskForHelpId, function(err, data){

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'Service Ask For help not exist'
            });
        }
        else{
            var status_service = data.status;

            helpnotes.status_service = status_service;
            var le_demandeur = data.user_id;
            helpnotes.user_service_id = le_demandeur;

            //je récupère le user
            User.findById(req.body.user_id, function(err, user){
                if(user == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'User not exist'
                    });
                }
                else
                {
                    var username = user.username;
                    var firstname = user.firstname;

                    helpnotes.username_celui_qui_ecrit = username;
                    helpnotes.firstname_celui_qui_ecrit = firstname;
                    
                    //Je dois notifier par mail le user qui a été choisi pour faire le travail
                    //je récupère l'email du user concerné
                    //je me rassure si le user_id renseigné est pour celui de l'employeur ou pour le travailleur
                    if(data.user_id == req.body.user_id){
                        //alors c'est l'employeur qui écrit et je dois plutôt notifier le travailleur
                        User.findById(data.user_id_celui_choisit, function(err, new_user){
                            if(new_user != null){
                                var email_travailleur = new_user.email;
                                
                                /****** Envoyer le code par mail *************/

                                var transporter = nodemailer.createTransport({
                                  service: 'gmail',
                                  auth: {
                                    user: 'help123backend@gmail.com',
                                    pass: '123help@2018'
                                  }
                                });
                        
                                var mailOptions = {
                                  from: 'help123backend@gmail.com',
                                  to: email_travailleur,
                                  subject: 'Help Note Notification',
                                  text: 'Check your account to see the new help note from your employer. Service name : ' + data.nom_service + ' Help Note : ' + req.body.note_help
                                };
                        
                                transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                    console.log(error);
                                  } else {
                                    console.log('Email sent: ' + info.response);
                                  }
                                });
                        
                        
                                /********** fin envoi mail  **************************/
                                
                            }
                        });
                        
                    }
                    
                    
                    if(data.user_id_celui_choisit == req.body.user_id){
                        //alors c'est le travailleur qui écrit et je dois plutôt notifier l'employeur
                        User.findById(data.user_id, function(err, new_user){
                            if(new_user != null){
                                var email_employeur = new_user.email;
                                
                                /****** Envoyer le code par mail *************/

                                var transporter = nodemailer.createTransport({
                                  service: 'gmail',
                                  auth: {
                                    user: 'help123backend@gmail.com',
                                    pass: '123help@2018'
                                  }
                                });
                        
                                var mailOptions = {
                                  from: 'help123backend@gmail.com',
                                  to: email_employeur,
                                  subject: 'Help Note Notification',
                                  text: 'Check your account to see the new help note from your worker. Service name : ' + data.nom_service + ' Help Note : ' + req.body.note_help
                                };
                        
                                transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                    console.log(error);
                                  } else {
                                    console.log('Email sent: ' + info.response);
                                  }
                                });
                        
                        
                                /********** fin envoi mail  **************************/
                                
                            }
                        });
                        
                    }

                    helpnotes.save(function(err, helpnotes){
                        res.status(200).json({
                            status: 'success',
                            message: 'New HelpNote created!',
                            data: helpnotes
                        });

                    });
                }
            });
        }
    });
};



exports.newAnswer = function(req, res){
    var anshelp = new AnsHelp();
    anshelp.note_help_id = req.body.note_help_id;
    anshelp.ans_help = req.body.ans_help;
    anshelp.user_service_id = '';
    anshelp.user_id_celui_qui_recoit = req.body.user_id_celui_qui_recoit;
    anshelp.create_date = new Date(Date.now()).toISOString();
    anshelp.save(function(err, anshelp){
        res.status(200).json({
            status: 'success',
            message: 'New AnswerHelp successfully created!',
            data: anshelp
        });

    });
};

// Handle view HelpNote info   permet de GET un HelpNote à partir de son Id
exports.view = function (req, res) {
    HelpNotes.findById(req.params.helpnote_id, function (err, helpnote) {
        /*if (err)
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
            */

        if (helpnote == null){
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'HelpNote details loading..',
            data: helpnote
        });
    });
};


// Handle view All HelpNotes services info   permet de GET tous les HelpNotes d'un Service Ask For Help
exports.view_helpnotes = function (req, res) {
    HelpNotes.find({serviceAskForHelpId:req.params.serviceAskForHelpId}, function (err, helpnote) {
        /*if (err)
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
            */

        if (helpnote == null){
            res.status(401).json({
                status: 'error',
                message: 'HelpNote ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'HelpNotes details loading..',
            data: helpnote
        });
    });
};

// Handle view All HelpNotes services info   permet de GET tous les answers d'un Service HelpNotes
exports.view_anshelp = function(req, res){
    AnsHelp.find({note_help_id:req.params.helpNotesId}, function (err, anshelp){
        if (anshelp == null){
            res.status(401).json({
                status: 'error',
                message: 'Answers ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Answers details loading..',
            data: anshelp
        });
    });
};


//permet d'update le status d'un HelpNote
exports.update_helpnotes = function (req, res) {
  var status_help_note = req.body.status_help_note;
  
  HelpNotes.findById(req.params.help_notes_id, function(err, data){
      if (data == null){
          res.status(401).json({
            status: 'success',
            message: 'HelpNotes ObjectId not exist'
        });
      }
      else{
          
            HelpNotes.findByIdAndUpdate(req.params.help_notes_id,
            {
                status_help_note: status_help_note,
                update_date: new Date(Date.now()).toISOString()
            },
            function(err, data){
                HelpNotes.findById(req.params.help_notes_id, function(err, helpnote){
                    res.status(200).json({
                    status: 'success',
                    message: 'HelpNotes details updated..',
                    data: helpnote
                });
                });
            }
          );
  
      }
  });
  
  
                                                                        
};




