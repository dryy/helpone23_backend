/**
 * Created by Landry on 20/06/07.
 */

// Import LiveChat model
Device = require('./../Models/deviceModel');
WalletUser = require('./../Models/walletUserModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
LiveChat = require('./../Models/LiveChatModel');

var nodemailer = require('nodemailer');     //pour l'envoi des mails
var validator = require('validator');



// exports.delete = function (req, res) {
//     HelpNotes.deleteOne({
//         _id: req.params.helpnote_id
//     }, function (err, user) {
//         if (err)
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Note not exist'
//             });
//         res.status(200).json({
//             status: "success",
//             message: 'Note deleted successfully'
//         });
//     });
// };


exports.save = function(req, res){
    
    var livechat = new LiveChat();
    livechat.user_name = req.body.name;
    livechat.user_email = req.body.email;
    livechat.chat_content = req.body.message;
    livechat.create_date = new Date(Date.now()).toISOString();
    livechat.update_date = new Date(Date.now()).toISOString();
    livechat.save(function(err, livechat){
        if(err){
            res.status(401).json({
                status: 'error',
                message: 'The message couldn\'t be sent'
            });
        }
        res.status(200).json({
            status: 'success',
            message: 'Thank you, Your message was successfully sent! We will get in touch with you ASAP',
            data: livechat
        });

    });
};

// Handle view LiveChat permet de GET tous les chats
exports.getChats = function (req, res) {
    LiveChat.get(function (err, chats) {
        if (err) {
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.json({
            status: 'success',
            message: 'Chats retrieved successfully',
            data: chats          //users avec s ici représente la base de données users
        });
    });
};


// // Handle view All HelpNotes services info   permet de GET tous les HelpNotes d'un Service Ask For Help
// exports.view_helpnotes = function (req, res) {
//     HelpNotes.find({serviceAskForHelpId:req.params.serviceAskForHelpId}, function (err, helpnote) {
//         /*if (err)
//             res.status(401).json({
//                 status: 'error',
//                 message: 'HelpNote ObjectId not exist'
//             });
//             */

//         if (helpnote == null){
//             res.status(401).json({
//                 status: 'error',
//                 message: 'HelpNote ObjectId not exist'
//             });
//         }

//         res.status(200).json({
//             status: 'success',
//             message: 'HelpNotes details loading..',
//             data: helpnote
//         });
//     });
// };

// // Handle view All HelpNotes services info   permet de GET tous les answers d'un Service HelpNotes
// exports.view_anshelp = function(req, res){
//     AnsHelp.find({note_help_id:req.params.helpNotesId}, function (err, anshelp){
//         if (anshelp == null){
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Answers ObjectId not exist'
//             });
//         }

//         res.status(200).json({
//             status: 'success',
//             message: 'Answers details loading..',
//             data: anshelp
//         });
//     });
// };



