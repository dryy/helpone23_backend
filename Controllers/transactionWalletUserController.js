/**
 * Created by christophe on 19/01/05.
 */

// Import transactionWalletUser model
TransactionWalletUser = require('./../Models/transactionWalletUserModel');


// Handle view transaction wallet user info   permet de GET les transactions du portefeuille du user
exports.view = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    TransactionWalletUser.find({user_id: req.params.user_id}, function(err, transactionwallet){
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        res.status(200).json({
            status: 'success',
            message: 'Transactions Wallet User details loading..',
            data: transactionwallet
        });
    }).sort(mysort);

};


