/**
 * Created by christophe on 18/09/26.
 */

// Import setting model
Setting = require('./../Models/settingModel');


// Handle index actions     permet de get tous les settings
exports.index = function (req, res) {
    Setting.get(function (err, settings) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Settings retrieved successfully",
            data: settings
        });
    });
};



// Handle create setting actions    permet de save(post) un setting
exports.new = function (req, res) {
    var setting = new Setting();
    setting.percentageOneService = req.body.percentageOneService ? req.body.percentageOneService : setting.percentageOneService;
    setting.timeToSendReport_value = req.body.value_time;
    setting.timeToSendReport_type = req.body.type_time;
    setting.amount_registration_fees = req.body.amount_registration_fees; 
    setting.costMinService_value = req.body.value_cost;
    setting.costMinService_device = req.body.device_cost;
    setting.percentageAfterHourWork = req.body.percentageAfterHourWork;
    setting.costOneAdvertising_value = req.body.value_advert;
    setting.costOneAdvertising_device = req.body.device_advert;
    setting.update_date = new Date(Date.now()).toISOString();

// save the setting and check for errors
    setting.save(function (err) {
         if (err)
             res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
             });
        res.status(200).json({
            status: 'success',
            message: 'New setting created!',
            data: setting
        });
    });
};



// Handle view setting info   permet de GET un setting à partir de son Id
exports.view = function (req, res) {
    Setting.findById(req.params.setting_id, function (err, setting) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Parameter ObjectId not exist'
            });
        res.status(200).json({
            status: 'success',
            message: 'Setting details loading..',
            data: setting
        });
    });
};



// Handle update setting info  permet d'update un setting
exports.update = function (req, res) {
    Setting.findByIdAndUpdate(req.params.setting_id,
        {
            percentageOneService: req.body.percentageOneService,
            timeToSendReport_value: req.body.value_time,
            timeToSendReport_type : req.body.type_time,
            amount_registration_fees: req.body.amount_registration_fees,
            costMinService_value: req.body.value_cost,
            costMinService_device: req.body.device_cost,
            percentageAfterHourWork: req.body.percentageAfterHourWork,
            costOneAdvertising_value: req.body.value_advert,
            costOneAdvertising_device: req.body.device_advert,
            update_date: new Date(Date.now()).toISOString()
        },
        function (err, setting) {
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'Parameter ObjectId not exist'
                });

            //je capture en BD les nouvelles données enregistrées
            Setting.findOne({_id: req.params.setting_id}, function(err, data){
                res.status(200).json({
                    status: 'success',
                    message: 'Parameter Info updated',
                    data: data
                });
            });    
        });
};



// Handle delete setting  permet de supprimer un setting
exports.delete = function (req, res) {
    Setting.remove({
        _id: req.params.setting_id
    }, function (err, setting) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Setting ObjectId not exist'
            });
        res.status(200).json({
            status: "success",
            message: 'Setting deleted'
        });
    });
};

