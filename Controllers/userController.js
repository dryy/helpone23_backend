/**
 * Created by christophe on 18/09/26.
 */

// Import user model
User = require('./../Models/userModel');
Role = require('./../Models/roleModel');
ChoixSousCategory = require('./../Models/choixsouscategoryModel');
Languages = require('./../Models/languagesModel');
bid = require('./../Models/bidModel');
serviceAskForHelp = require('./../Models/serviceAskForHelpModel');
WalletUser = require('./../Models/walletUserModel');
HelpNotes = require('./../Models/HelpNotesModel');
ScheduleDates = require('./../Models/ScheduleDatesModel');


var jwt = require('jsonwebtoken');
//const bcrypt = require('bcrypt');
//const saltRounds = 10;

var crypto = require('crypto');

var validator = require('validator');

var nodemailer = require('nodemailer');
var counter =0;


// Handle index actions     permet de get tous les users
exports.index = function (req, res) {
    User.get(function (err, users) {
        if (err) {
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.json({
            status: 'success',
            message: 'Users retrieved successfully',
            data: users          //users avec s ici représente la base de données users
        });
    });
};


//permet de vérifier si l'email existe déjà et renvoit les suggestions de username
exports.checkEmail = function (req, res) {
var email = req.body.email;
    User.findOne({email : email.toLowerCase()}, function(err, data){
        if(err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        //res.json({data: data._id})
        else if (data != null){          //pour dire si que cet adresse email est déjà utilisée
            res.status(401).json({
                status: 'error',
                message: 'Email already exist'
            });
        }
        else {

            //je dois extraire le nom avant @ puis faire une proposition de 3 username
            var str = email.toLowerCase();;

            var chars = str.split('@');
            //console.log(chars[0]);    chars[0] contient la chaîne de caractère qui vient avant @

            var code1 = Math.floor(Math.random() * 10000);
            var code2 = Math.floor(Math.random() * 1000);
            var code3 = Math.floor(Math.random() * 100000);

            var username1 = chars[0] + code1;
            var username2 = chars[0] + code2;
            var username3 = chars[0] + code3;

            //je dois vérifier que tous ces username n'existent pas déjà
            //username1
            User.findOne({username : username1}, function(err, data1){
                if(data1 != null){   //username1 existe déjà
                    //je regénère un autre code1
                    code1 = Math.floor(Math.random() * 10000);
                    username1 = chars[0] + code1;
                }
            });

            //username2
            User.findOne({username : username2}, function(err, data2){
                if(data2 != null){   //username2 existe déjà
                    //je regénère un autre code2
                    code2 = Math.floor(Math.random() * 1000);
                    username2 = chars[0] + code2;
                }
            });

            //username3
            User.findOne({username : username3}, function(err, data3){
                if(data3 != null){   //username3 existe déjà
                    //je regénère un autre code3
                    code3 = Math.floor(Math.random() * 100000);
                    username3 = chars[0] + code3;
                }
            });

            var suggestions = {'username1': username1,
                'username2': username2,
                'username3': username3
            };

            res.status(200).json({
                status: 'success',
                message: 'This email is free',
                suggestions: suggestions
            });

        }
    });

};


//permet de vérifier si le username existe déjà
exports.checkUsername = function (req, res){

    User.findOne({username : req.body.username}, function(err, data){
        if(data != null){   //username existe déjà
            res.status(401).json({
                status: 'error',
                message: 'Username already exist'
            });
        }
        else{
            res.status(200).json({
                status: 'success',
                message: 'This username is free'
            });
        }
    });
};

exports.accountConfirmation = function(req, res){
    var re = req.body.uemail;
    // User.findOne({email : re}, function(err, user){
    User.findOne({$and : [{email : re}, {activation_code : req.body.Confirmation_code}]}, function(err, user){
        if(user != null){
            var identificateur = user._id;
            User.findByIdAndUpdate(identificateur, {confirmed: true}, function (err, user) {
                if (err)
                   res.status(401).json({
                       status: 'error',
                       message: user.Username,
                       data: user.email
                   });

                const JWTToken = jwt.sign({email: user.email_or_username, _id: user._id}, 'secret', {expiresIn: '2h'});         //temps auquel on voudrait que le token du user expire
                User.findByIdAndUpdate(user._id, {remember_token: JWTToken}, function (err, user) {
                    
                });
                   res.status(200).json({
                    status: 'successs',
                    message: 'donneeee',
                    token: JWTToken,
                    user: user
                });

            });
                        
        }

        else{
            res.status(401).json({
               status: 'error',
               message: 'noooooo uusseerrrr'
           });
        }
    });
}


// suivante est la fonction pour suspendre le compte d'un utilisateur

    exports.accountSuspension = function(req, res){
        var re = req.body.id;
        User.findByIdAndUpdate(re, {suspended: true}, function (err, user1) {
            if (err)
               res.status(401).json({
                   status: 'error',
                   message: user1.Username,
                   data: user1.email
               });

               res.status(200).json({
                status: 'successs',
                message: 'donneeee',
                user: user1
            });

        });
                            
    }


// suivante est la fonction pour lever la suspension du compte d'un utilisateur de l'application
    exports.accountUnSuspension = function(req, res){
        var re = req.body.id;
        User.findByIdAndUpdate(re, {suspended: false}, function (err, user1) {
            if (err)
               res.status(401).json({
                   status: 'error',
                   message: user1.Username,
                   data: user1.email
               });

               res.status(200).json({
                status: 'successs',
                message: 'donneeee',
                user: user1
            });
        });             
    }
    //fin du code

exports.eee = function(email, nombre){

                /***** Envoyer le code par mail ************/
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            host: "smtp.gmail.com",
            // port: 587,
            // secure: false, // true for 465, false for other ports
            auth: {
              user: "moo.sdkgames@gmail.com", // generated ethereal user
              pass: "ajokfabdzfheudpq" // generated ethereal password
            }
          });

        var mailOptions = {
          from: "moo.sdkgames@gmail.com",//'help123backend',
          to: email,
          subject: 'Account activation',
          text: 'Use this code to reset your password : ',
          html: "<html><p>Welcome on help123. Click on the link below to activate your use account. </p><a title=\"Account validation for help123\" href=\"http://api.visavisbet.com/help123/#/confirm?uemail="+email+"&code="+nombre+"\">Activate your account</a></html>"
        };

        transporter.sendMail(mailOptions, function(error, info){
          if (error) {
            console.log(error);
            res.status(401).json({
                status: 'error',
                message: 'Device not exist'
            });
          } else {
            console.log('Email sent: ' + info.response);
            res.json({
                status: DOMAIN,
                message: 'Users received successfully',
                data: "ok"  
            });
          }
          transporter.close();
        });


        /********* fin envoi mail  *************************/

    
}

// Handle create user actions    permet de save(post) un user
exports.register = function (req, res){
    var user = new User();
    var ladate = new Date();
    ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();
    var key = 'salt_from_the_user_document';
    var plaintext = req.body.password;      //password à crypter
    var cipher = crypto.createCipher('aes-256-cbc', key);
    var decipher = crypto.createDecipher('aes-256-cbc', key);

    cipher.update(plaintext, 'utf8', 'base64');
    var encryptedPassword = cipher.final('base64');

    decipher.update(encryptedPassword, 'base64', 'utf8');
    var decryptedPassword = decipher.final('utf8');
    // var role_id = "";
    var utilisateur = "utilisateur";
    var nombre = Math.floor(Math.random() * 10000);
    var email = req.body.email;

    user.username = req.body.username;
    user.firstname = req.body.firstname;
    user.surname = req.body.surname;
    user.pseudo = "";/*req.body.pseudo;*/
    user.email = email.toLowerCase();
    user.personal_description = "";
    user.gender = "" /*req.body.gender*/;
    user.phone = ""/*req.body.phone*/;
    user.confirmed = false;
    user.suspended = false;
    user.activation_code = nombre;
    user.password = encryptedPassword;/*req.body.password;*/ /*bcrypt.hashSync(req.body.password, saltRounds);*/
    user.experience = "";      //Beginner, Intermediate, Advanced
    user.CV = "";/*req.body.CV;*/
    user.criminal_record =""; 
    user.longitude_location = "";/*req.body.longitude_location;*/
    user.latitude_location = "";/*req.body.latitude_location;*/
    user.location_name = "";
    longitude_city = "";/*req.body.longitude_city;*/
    latitude_city = "";/*req.body.latitude_city;*/
    user.country = ""/*req.body.country*/;
    user.picture = "";
    user.picture_cni = "";
    user.picture_with_cni = "";
    user.title_role = /*req.body.title_role;*/'utilisateur';       //par défaut title_role = utilisateur
    user.slug_role = /*req.body.slug_role;*/ 'user';
    user.fonctionName = "";     //fonctionName = Offer helps ou Ask for helps
    user.online = /*req.body.online;*/ 0;       //1 pour connecté et 0 pour déconnecté
    user.number_helps_asked = 0;
    user.number_helps_offer = 0;
    user.nbre_etoiles = 0;
    user.update_date = new Date(Date.now()).toISOString();
    user.date_connection_failed = ladate;
    user.number_connection_failed = 0;
    user.pwdFailed = false;
    user.role_id = "5be6eeb4119e8b17b547f2d6";

    // res.status(401).json({
    //             status: 'error',
    //             message: 'Fill all the fields',
    //             data: user.username,
    //             firstname: user.firstname,
    //             surname: user.surname,
    //             email: user.email,
    //             password: encryptedPassword,
    //             ladate: ladate,
    //             user: user
    //         });

    user.save(function(err, user){
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Fill all the fields',
                user: user
            });
        else{
             var wallet = new WalletUser();
            var device_id = "5c6a917f9522db5b1e2642e5";
            var user_id = user._id;
            var device_name = "";
            wallet.user_id = user_id;
            wallet.amount = 0;
            wallet.device_id = device_id;
            wallet.name_device = device_name;
            wallet.update_date = new Date(Date.now()).toISOString();

            wallet.save(function(err, wallet){
                if (err)
                    res.status(401).json({
                        status: 'error',
                        message: 'error, wallet not saved',
                        wallet: wallet
                    });
               
            });
            exports.eee(email.toLowerCase(), nombre);
            res.status(200).json({
                status: 'success',
                message: 'Your account has been successfully created! Use the link we sent you by mail to activate',
                data: user
            });
        }
        

    });
    // user.save(function (err, user) {
    //     if (err)
    //         res.status(401).json({
    //             status: 'error',
    //             message: 'Fill all the fields',
    //             user: user
    //         });
    //     exports.eee(email.toLowerCase(), nombre);
    //     res.status(200).json({
    //         status: 'success',
    //         message: 'Your account has been successfully created! Use the link we sent you by mail to activate',
    //         user: user
    //     });
    // });
};


// Handle create user actions    permet de save(post) un user
// exports.register = function (req, res) {
//     var user = new User();
//     var ladate = new Date();
//     ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();

//     var choixsouscategory = new ChoixSousCategory();

//     var languages = new Languages();

//     var key = 'salt_from_the_user_document';
//     var plaintext = req.body.password;      //password à crypter
//     var cipher = crypto.createCipher('aes-256-cbc', key);
//     var decipher = crypto.createDecipher('aes-256-cbc', key);

//     cipher.update(plaintext, 'utf8', 'base64');
//     var encryptedPassword = cipher.final('base64');

//     decipher.update(encryptedPassword, 'base64', 'utf8');
//     var decryptedPassword = decipher.final('utf8');
//     //var role_id = null;
//     var utilisateur = "utilisateur";

//     var device_id = /*"5c61afb55f1e831f77d42609"*/"5c6a917f9522db5b1e2642e5";     //ObjectId de la device ZAR

//     var device_name = "";

//     //je prends le nom de device_id
//     Device.findById(device_id, function(err, data){
//         if(data == null){
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Device not exist'
//                 //data: serviceAskForHelp
//             });
//         }
//         else{
//             device_name = data.name;
//         }
//     });

//     // var nombre = Math.floor(Math.random() * (9999 - 10000 + 1)) + 10000;
//     var nombre = Math.floor(Math.random() * 10000);
//     var email = req.body.email;
//     //var password = bcrypt.hashSync(myPlaintextPassword, saltRounds);

//     user.username = req.body.username ? req.body.username : user.username;
//     user.firstname = req.body.firstname;
//     user.surname = req.body.surname;
//     user.pseudo = "";/*req.body.pseudo;*/
//     user.email = email.toLowerCase();
//     user.personal_description = "";
//     user.gender = "" /*req.body.gender*/;
//     user.phone = ""/*req.body.phone*/;
//     user.confirmed = false;
//     user.suspended = false;
//     user.activation_code = nombre;
//     user.password = encryptedPassword;/*req.body.password;*/ /*bcrypt.hashSync(req.body.password, saltRounds);*/
//     user.experience = req.body.experience;      //Beginner, Intermediate, Advanced
//     user.CV = "";/*req.body.CV;*/
//     //user.CV.data = req.body.data_cv;
//     //user.CV.contentType = req.body.contentType_cv;
//     user.criminal_record =""; /*req.body.criminal_record;*/
//     //user.criminal_record.data = req.body.data_criminal_record;
//     //user.criminal_record.contentType = req.body.contentType_criminal_record;
//     user.longitude_location = "";/*req.body.longitude_location;*/
//     user.latitude_location = "";/*req.body.latitude_location;*/
//     user.location_name = "";
//     //user.location.type_location = req.body.type_location;
//     //user.location.coordinates.longitude = req.body.longitude_location;
//     //user.location.coordinates.latitude = req.body.latitude_location;
//     longitude_city = "";/*req.body.longitude_city;*/
//     latitude_city = "";/*req.body.latitude_city;*/
//     //user.city.type_city = req.body.type_city;
//     //user.city.coordinates.longitude = req.body.longitude_city;
//     //user.city.coordinates.latitude = req.body.latitude_city;
//     user.country = ""/*req.body.country*/;
//     user.picture = req.body.picture;
//     user.picture_cni = req.body.picture_cni;
//     user.picture_with_cni = req.body.picture_with_cni;
//     //user.picture.data = req.body.data_picture;
//     //user.picture.contentType = req.body.contentType_picture;
//     user.title_role = /*req.body.title_role;*/'utilisateur';       //par défaut title_role = utilisateur
//     user.slug_role = /*req.body.slug_role;*/ 'user';
//     user.fonctionName = req.body.fonctionName;     //fonctionName = Offer helps ou Ask for helps
//     //user.fonctions.fonctionName = req.body.fonctionName;
//     user.online = /*req.body.online;*/ 0;       //1 pour connecté et 0 pour déconnecté
//     user.number_helps_asked = 0;
//     user.number_helps_offer = 0;
//     user.nbre_etoiles = 0;
//     user.update_date = new Date(Date.now()).toISOString();
//     user.date_connection_failed = ladate;
//     user.number_connection_failed = 0;
//     user.pwdFailed = false;



//     //capture du role_id
//     /*let RoleNew =*/  Role.findOne({slug: req.body.slug}, function(err, data){
//         if(err){
//             res.status(500).json({
//                 status: 'error',
//                 message: 'Internal Problem Server'
//             });
//         }
//         //res.status(401).json({data: data})
//         if (data != null){

//             user.role_id = data._id;

//         }
//         else{
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Check list roles'
//             });
//         }
//     });

//     user.role_id = "5be6eeb4119e8b17b547f2d6" /*"5c0a4232bc1ed90c74445326"*/; //role = utilisateur (user)

//     //user.roles.title = req.body.title_role;
//     //user.roles.slug = req.body.slug_role;


//     //contrôle si l'email existe déjà
//     /*let userNew =*/ User.findOne({email : email.toLowerCase()}, function(err, data){
//         if(err){
//             res.status(500).json({
//                 status: 'error',
//                 message: 'Internal Problem Server'
//             });
//         }
//         //res.json({data: data._id})
//         else if (data != null){          //pour dire si que cet adresse email est déjà utilisée
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Email already exist'
//             });
//         }
//     });

//     // save the user and check for errors
//     user.save(function (err) {
//         if (err)
//         //res.status(401).json({message: err});
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Fill all the fields'
//             });


//         var user_id = user._id;


//         //ici il me faut lire le json qui prends en entrée les choix des souscatégories
//         //du user. Je vais contrôler pour chaque choix si ça n'existe pas encore dans
//         //souscatégorie
//         //"souscategories" : [
//         //{"sousCategorie_id": value, "sousCategoryName": value},
//         //{"sousCategorie_id": value, "sousCategoryName": value}
//         //                  ]

//         //Je prends le tableau d'objets ici
//         var tableau_souscategories = req.body.souscategories;

//         var choixsouscategoryarray = new Array();

//         //je stocke les objets du tableau dans un tableau
//         for(var i=0;i<tableau_souscategories.length;i++){
//             choixsouscategoryarray[i] = new ChoixSousCategory();

//             choixsouscategoryarray[i].souscategory_id = tableau_souscategories[i].sousCategorie_id;
//             choixsouscategoryarray[i].souscategoryName = tableau_souscategories[i].sousCategoryName;
//             choixsouscategoryarray[i].user_id = user_id;

//             //choixsouscategory.save();
//         }

//         //j'enregistre chaque objet du tableau
//         for(var i=0;i<tableau_souscategories.length;i++){
//             choixsouscategoryarray[i].save();
//         }


//         //ici je dois lire le json qui prends en entrée les différentes langues entrées
//         //par le user. Je vais contrôler pour chaque langue si ça n'existe pas encore pour le user
//         //"langues" : [
//         //{"langue": value1},
//         //{"langue": value2}
//         //                  ]

//         //Je prends le tableau d'objets ici
//         var tableau_languages = req.body.langues;

//         var languages_array = new Array();

//         //je stocke les objets du tableau dans un tableau
//         for(var j=0;j<tableau_languages.length;j++){
//             languages_array[j] = new Languages();

//             languages_array[j].langue = tableau_languages[j].langue;
//             languages_array[j].user_id = user_id;

//             //languages.save();
//         }


//         //j'enregistre chaque objet du tableau
//         for(var j=0;j<tableau_languages.length;j++){
//             languages_array[j].save();
//         }

//         //je créé une fois le wallet du user
//         var wallet = new WalletUser();
//         wallet.user_id = user_id;
//         wallet.amount = 0;
//         wallet.device_id = device_id;
//         wallet.name_device = device_name;
//         wallet.update_date = new Date(Date.now()).toISOString();

//         wallet.save();

//         exports.eee(email.toLowerCase(), nombre);

//         res.status(200).json({
//             status: 'success',
//             message: 'New user created!',
//             data: user
//         });
//     });
// };



// Handle view user info   permet de GET un user à partir de son Id
exports.view = function (req, res) {

    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(user == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }

        //je prends tous les choix des sous-catégories fait par le user
        ChoixSousCategory.find({user_id: req.params.user_id}, function (err, choixsouscategory){
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });


            //je prends toutes les langues choisies du user
            Languages.find({user_id: req.params.user_id}, function(err, langue){
                if (err)
                    res.status(401).json({
                        status: 'error',
                        message: 'Internal Problem Server'
                    });

                var result = [user , choixsouscategory, langue];

                res.status(200).json({
                    status: 'success',
                    message: 'User details loading..',
                    data: result //result[1][0]

                });
            });

        });
    });
};



exports.switchAccount = function (req, res){
    // var user_id = req.body.user_id;
    User.findByIdAndUpdate(req.body.user_id,
        {
            experience: req.body.experience
        },
        function (err, user) {
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            //j'efface toutes les sous-catégories du user et je prends le nouveau tableau JSON envoyé
            ChoixSousCategory.remove({user_id: req.body.user_id}, function (err, user) {});

            var user_id = req.body.user_id;
            //Je prends le tableau d'objets ici
            var tableau_souscategories = req.body.souscategories;

            var choixsouscategoryarray = new Array();

            //je stocke les objets du tableau dans un tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i] = new ChoixSousCategory();

                choixsouscategoryarray[i].souscategory_id = tableau_souscategories[i].sousCategorie_id;
                choixsouscategoryarray[i].souscategoryName = tableau_souscategories[i].sousCategoryName;
                choixsouscategoryarray[i].user_id = user_id;

                //choixsouscategory.save();
            }

            //j'enregistre chaque objet du tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i].save();
            }

            
            User.findByIdAndUpdate(req.body.user_id, {fonctionName: req.body.username}, function (err, user) {
                if (err)
                    res.status(401).json({
                        status: 'error',
                        message: 'User not exist'
                    });
                return res.status(200).json({
                    status: 'success',
                    message: 'skills saved successfully ...',
                    user: user
                }); 
            });  

        }
    );

    
};


// Handle update user info  permet d'update un user
exports.updateNew = function (req, res) {

    var user = new User();
    
    var email = req.body.email;

    //on se rassure si l'email entré par le user n'existe pas encore en prenant bien évidemment
    //le cas où si l'email entré est toujours l'ancien email enregistré alors on ne fait rien
    /*let userNew =*/ User.findOne({email : email.toLowerCase()}, function(err, data){
        if(err)
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(data != null){
            if(data.email != email.toLowerCase()){
                res.status(401).json({
                    status: 'error',
                    message: 'Email already exist'
                });
            }
        }

        //res.json({data: data._id})
    });


    User.findByIdAndUpdate(req.params.user_id,
        {
            username: req.body.username,
            firstname: req.body.firstname,
            surname: req.body.surname,
            slug: req.body.slug,        //slug = user par défaut
            pseudo: req.body.pseudo,
            experience: req.body.experience,
            email : email.toLowerCase(),
            personal_description: req.body.personal_description,
            gender: req.body.gender,
            phone: req.body.phone,
            //password: /*req.body.password*/ bcrypt.hashSync(req.body.password, saltRounds),
            CV: req.body.CV,
            criminal_record: req.body.criminal_record,
            longitude_location: req.body.longitude_location,
            latitude_location: req.body.latitude_location,
            location_name: req.location_name,
            longitude_city: req.body.longitude_city,
            latitude_city: req.body.latitude_city,
            country: req.body.country,
            picture: req.body.picture,
            picture_cni: req.body.picture_cni,
            picture_with_cni: req.body.picture_with_cni,
            //title_role: req.body.title_role,
            //slug_role: req.body.slug_role,
            fonctionName: req.body.fonctionName,
            update_date: new Date(Date.now()).toISOString()
        },
        function (err, user) {
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist....'
                });

            //j'efface toutes les sous-catégories du user et je prends le nouveau tableau JSON envoyé
            ChoixSousCategory.remove({user_id: req.params.user_id}, function (err, user) {});

            var user_id = req.params.user_id;
            //Je prends le tableau d'objets ici
            var tableau_souscategories = req.body.souscategories;

            var choixsouscategoryarray = new Array();

            //je stocke les objets du tableau dans un tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i] = new ChoixSousCategory();

                choixsouscategoryarray[i].souscategory_id = tableau_souscategories[i].sousCategorie_id;
                choixsouscategoryarray[i].souscategoryName = tableau_souscategories[i].sousCategoryName;
                choixsouscategoryarray[i].user_id = user_id;

                //choixsouscategory.save();
            }

            //j'enregistre chaque objet du tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i].save();
            }


            //j'efface toutes les langues du user puis je prends le nouveau tableau JSON envoyé
            Languages.remove({user_id: req.params.user_id}, function (err, user) {});

            //Je prends le tableau d'objets ici
            var tableau_languages = req.body.langues;

            var languages_array = new Array();

            //je stocke les objets du tableau dans un tableau
            for(var j=0;j<tableau_languages.length;j++){
                languages_array[j] = new Languages();

                languages_array[j].langue = tableau_languages[j];
                languages_array[j].user_id = user_id;

                //languages.save();
            }


            //j'enregistre chaque objet du tableau
            for(var j=0;j<tableau_languages.length;j++){
                languages_array[j].save();
            }


            //je capture en BD ls nouvelles données enregistrées
            User.findOne({email: email.toLowerCase()}, function(err, user){


                //j'update aussi username, firstname, number_helps_asked, number_helps_offer, nbre_etoiles et picture du user dans bidModel s'il y en a
                //commençons avec user_id_celui_qui_bid
                var myquery = { user_id_celui_qui_bid: req.params.user_id };
                var newvalues = {$set: {
                    username_celui_qui_bid: req.body.username,
                    firstname_celui_qui_bid: req.body.firstname,
                    location_name_celui_qui_bid: req.location_name,
                    number_helps_asked_celui_qui_bid: user.number_helps_asked,
                    number_helps_offer_celui_qui_bid: user.number_helps_offer,
                    nbre_etoiles_celui_qui_bid: user.nbre_etoiles,
                    picture_celui_qui_bid: req.body.picture
                }
                };

                bid.updateMany(myquery, newvalues, function(err, res) {
                    //if (err){}
                });

                //continuons avec user_id_celui_qui_offre_service
                var myquery = { user_id_celui_qui_offre_service: req.params.user_id };
                var newvalues = {$set: {
                    username_celui_qui_offre_service: req.body.username,
                    firstname_celui_qui_offre_service: req.body.firstname,
                    location_name_celui_qui_offre_service: req.location_name,
                    number_helps_asked_celui_qui_offre_service: user.number_helps_asked,
                    number_helps_offer_celui_qui_offre_service: user.number_helps_offer,
                    nbre_etoiles_celui_qui_offre_service: user.nbre_etoiles,
                    picture_celui_qui_offre_service: req.body.picture
                }
                };

                bid.updateMany(myquery, newvalues, function(err, res) {
                    //if (err){}
                });


                //j'update aussi les données de celui qui a été choisit dans serviceAskForHelp
                var myquery = { user_id_celui_choisit: req.params.user_id };
                var newvalues = {$set: {
                    username_celui_choisit: req.body.username,
                    firstname_celui_choisit: req.body.firstname,
                    location_name_celui_choisit: req.location_name,
                    picture_celui_choisit: req.body.picture
                }
                };

                serviceAskForHelp.updateMany(myquery, newvalues, function(err, res) {
                    //if (err){}
                });

                //HelpNotesModel
                var myquery = { user_id_celui_qui_ecrit: req.params.user_id };
                var newvalues = {$set: {
                    username_celui_qui_ecrit: req.body.username,
                    firstname_celui_qui_ecrit: req.body.firstname
                    //update_date: new Date(Date.now()).toISOString()
                }
                };

                HelpNotes.updateMany(myquery, newvalues, function(err, res) {
                    //if (err){}
                });


                //ScheduleDatesModel
                var myquery = { user_id: req.params.user_id };
                var newvalues = {$set: {
                    username_celui_qui_ecrit: req.body.username,
                    firstname_celui_qui_ecrit: req.body.firstname
                    //update_date: new Date(Date.now()).toISOString()
                }
                };

                ScheduleDates.updateMany(myquery, newvalues, function(err, res) {
                    //if (err){}
                });

                //je prends tous les choix des sous-catégories fait par le user
                ChoixSousCategory.find({user_id: req.params.user_id}, function (err, choixsouscategory){
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });


                    //je prends toutes les langues choisies du user
                    Languages.find({user_id: req.params.user_id}, function(err, langue){
                        if (err)
                            res.status(401).json({
                                status: 'error',
                                message: 'Internal Problem Server'
                            });

                        var result = [user , choixsouscategory, langue];

                        res.status(200).json({
                            status: 'success',
                            message: 'User details loading..',
                            data: result //result[1][0]

                        });
                    });

                });

                //res.status(200).json({
                //    status: 'success',
                //    message: 'User Info updated',
                //    data: user
                //});
            });
        });
};



// Handle update user info  permet d'update un user
exports.update = function (req, res) {

    //var user = new User();

    //User.findByIdAndUpdate(req.params.user_id, {$set: {balance : req.body.balance}}, { new: true }, function (err, user) {
    
    var email = req.body.email;

    User.findById(req.params.user_id, function (err, user) {

        //if (err) return res.send(err);
        //res.send('User udpated.');

        if (err)
            res.send(err);

        var user = new User();
        user.username = req.body.username ? req.body.username : user.username;
        user.pseudo = req.body.pseudo;
        user.email = email.toLowerCase();
        user.gender = req.body.gender;
        user.phone = req.body.phone;
        user.CV.data = req.body.data_cv;
        user.CV.contentType = req.body.contentType_cv;
        user.criminal_record.data = req.body.data_criminal_record;
        user.criminal_record.contentType = req.body.contentType_criminal_record;
        user.location.type_location = req.body.type_location;
        user.location.coordinates.longitude = req.body.longitude_location;
        user.location.coordinates.latitude = req.body.latitude_location;
        user.city.type_city = req.body.type_city;
        user.city.coordinates.longitude = req.body.longitude_city;
        user.city.coordinates.latitude = req.body.latitude_city;
        user.country = req.body.country;
        user.picture.data = req.body.data_picture;
        user.picture.contentType = req.body.contentType_picture;
        user.roles.title = req.body.title_role;
        user.roles.slug = req.body.slug_role;
        user.fonctions.fonctionName = req.body.fonctionName;
        user.online = req.body.online;
        user.passwordresets.code = req.body.code;
        user.passwordresets.confirmation_code = req.body.confirmation_code;
        user.passwordresets.statusvalidity = req.body.statusvalidity;
        user.registrations_fees.status = req.body.status_registration;
        user.registrations_fees.amount = req.body.amount_registration;
        user.wallet.balance = req.body.balance;
        user.notifications.title_notification = req.body.title;
        user.notifications.description = req.body.description_notification;
        user.notifications.read_notification = req.body.read;
        user.notifications.deleted = req.body.deleted;
        user.notifications.type_notification = req.body.type_notification;
        user.advertising.image_ad.data = req.body.data_image_ad;
        user.advertising.image_ad.contentType = req.body.contentType_image_ad;
        user.advertising.description = req.body.description_advertising;
        user.advertising.start_date = req.body.start_date_advertising;
        user.advertising.end_date = req.body.end_date_advertising;
        user.advertising.start_time = req.body.start_time_advertising;
        user.advertising.end_time = req.body.end_time_advertising;
        user.advertising.fees = req.body.fees;
        user.sponsors.logo.data = req.body.data_sponsor;
        user.sponsors.logo.contentType = req.body.contentType_sponsor;
        user.sponsors.description = req.body.description_sponsor;
        user.sponsors.start_date = req.body.start_date_sponsor;
        user.sponsors.end_date = req.body.end_date_sponsor;
        user.sponsors.start_time = req.body.start_time_sponsor;
        user.sponsors.end_time = req.body.end_time_sponsor;
        user.paymentMethod.typePaymentMethodName = req.body.typePaymentMethodName;
        user.paymentMethod.active = req.body.active;
        user.products.categoryName = req.body.categoryName;
        user.products.product.description = req.body.description_product;
        user.products.product.picture.data = req.body.data_product;
        user.products.product.contentType = req.body.contentType_product;
        user.products.product.unitPrice = req.body.unitPrice;
        user.products.product.quantity = req.body.quantity;
        user.products.product.location.type_location = req.body.type_location_product;
        user.products.product.location.longitude = req.body.longitude_product;
        user.products.product.location.latitude = req.body.latitude_product;
        user.products.product.deliveryTime = req.body.deliveryTime_product;
        user.products.product.typeDeliveryTime = req.body.typeDeliveryTime_product;
        user.userBasket.categoryName = req.body.categoryName_userBasket;
        user.userBasket.product.description = req.body.description_userBasket;
        user.userBasket.product.picture.data = req.body.data_userBasket;
        user.userBasket.product.contentType = req.body.contentType_userBasket;
        user.userBasket.product.unitPrice = req.body.unitPrice_userBasket;
        user.userBasket.product.quantity = req.body.quantity_userBasket;
        user.userBasket.product.location.type_location = req.body.type_location_userBasket;
        user.userBasket.product.location.longitude = req.body.longitude_userBasket;
        user.userBasket.product.location.latitude = req.body.latitude_userBasket;
        user.userBasket.product.deliveryTime = req.body.deliveryTime_userBasket;
        user.userBasket.product.typeDeliveryTime = req.body.typeDeliveryTime_userBasket;
        user.userBasket.product.statusInTheBasket = req.body.statusInTheBasket;
        user.transactions.ref = req.body.ref;
        user.transactions.sens = req.body.sens;          //sens = entrant ou sortant
        user.transactions.type_transaction = req.body.type_transaction;       //type_transaction = portemonnaie ou caisse
        user.transactions.amount = req.body.amount;
        user.update_date = new Date(Date.now()).toISOString();

        //user.update(function (err) {
        //    if (err)
        //        res.json(err);
        //    res.json({
        //        message: 'User Info updated',
        //        data: user
        //    });
        //});

//save the user and check for errors
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'User Info updated',
                data: user
            });
        });
    });


    //    var user = new User();
    //    user.username = req.body.username ? req.body.username : user.username;
    //    user.pseudo = req.body.pseudo;
    //    user.email = req.body.email;
    //    user.gender = req.body.gender;
    //    user.phone = req.body.phone;
    //    user.CV.data = req.body.data_cv;
    //    user.CV.contentType = req.body.contentType_cv;
    //    user.criminal_record.data = req.body.data_criminal_record;
    //    user.criminal_record.contentType = req.body.contentType_criminal_record;
    //    user.location.type_location = req.body.type_location;
    //    user.location.coordinates.longitude = req.body.longitude_location;
    //    user.location.coordinates.latitude = req.body.latitude_location;
    //    user.city.type_city = req.body.type_city;
    //    user.city.coordinates.longitude = req.body.longitude_city;
    //    user.city.coordinates.latitude = req.body.latitude_city;
    //    user.country = req.body.country;
    //    user.picture.data = req.body.data_picture;
    //    user.picture.contentType = req.body.contentType_picture;
    //    user.roles.title = req.body.title_role;
    //    user.roles.slug = req.body.slug_role;
    //    user.fonctions.fonctionName = req.body.fonctionName;
    //    user.online = req.body.online;
    //    user.passwordresets.code = req.body.code;
    //    user.passwordresets.confirmation_code = req.body.confirmation_code;
    //    user.passwordresets.statusvalidity = req.body.statusvalidity;
    //    user.registrations_fees.status = req.body.status_registration;
    //    user.registrations_fees.amount = req.body.amount_registration;
    //    user.wallet.balance = req.body.balance;
    //    user.notifications.title_notification = req.body.title;
    //    user.notifications.description = req.body.description_notification;
    //    user.notifications.read_notification = req.body.read;
    //    user.notifications.deleted = req.body.deleted;
    //    user.notifications.type_notification = req.body.type_notification;
    //    user.advertising.image_ad.data = req.body.data_image_ad;
    //    user.advertising.image_ad.contentType = req.body.contentType_image_ad;
    //    user.advertising.description = req.body.description_advertising;
    //    user.advertising.start_date = req.body.start_date_advertising;
    //    user.advertising.end_date = req.body.end_date_advertising;
    //    user.advertising.start_time = req.body.start_time_advertising;
    //    user.advertising.end_time = req.body.end_time_advertising;
    //    user.advertising.fees = req.body.fees;
    //    user.sponsors.logo.data = req.body.data_sponsor;
    //    user.sponsors.logo.contentType = req.body.contentType_sponsor;
    //    user.sponsors.description = req.body.description_sponsor;
    //    user.sponsors.start_date = req.body.start_date_sponsor;
    //    user.sponsors.end_date = req.body.end_date_sponsor;
    //    user.sponsors.start_time = req.body.start_time_sponsor;
    //    user.sponsors.end_time = req.body.end_time_sponsor;
    //    user.paymentMethod.typePaymentMethodName = req.body.typePaymentMethodName;
    //    user.paymentMethod.active = req.body.active;
    //    user.products.categoryName = req.body.categoryName;
    //    user.products.product.description = req.body.description_product;
    //    user.products.product.picture.data = req.body.data_product;
    //    user.products.product.contentType = req.body.contentType_product;
    //    user.products.product.unitPrice = req.body.unitPrice;
    //    user.products.product.quantity = req.body.quantity;
    //    user.products.product.location.type_location = req.body.type_location_product;
    //    user.products.product.location.longitude = req.body.longitude_product;
    //    user.products.product.location.latitude = req.body.latitude_product;
    //    user.products.product.deliveryTime = req.body.deliveryTime_product;
    //    user.products.product.typeDeliveryTime = req.body.typeDeliveryTime_product;
    //    user.userBasket.categoryName = req.body.categoryName_userBasket;
    //    user.userBasket.product.description = req.body.description_userBasket;
    //    user.userBasket.product.picture.data = req.body.data_userBasket;
    //    user.userBasket.product.contentType = req.body.contentType_userBasket;
    //    user.userBasket.product.unitPrice = req.body.unitPrice_userBasket;
    //    user.userBasket.product.quantity = req.body.quantity_userBasket;
    //    user.userBasket.product.location.type_location = req.body.type_location_userBasket;
    //    user.userBasket.product.location.longitude = req.body.longitude_userBasket;
    //    user.userBasket.product.location.latitude = req.body.latitude_userBasket;
    //    user.userBasket.product.deliveryTime = req.body.deliveryTime_userBasket;
    //    user.userBasket.product.typeDeliveryTime = req.body.typeDeliveryTime_userBasket;
    //    user.userBasket.product.statusInTheBasket = req.body.statusInTheBasket;
    //    user.transactions.ref = req.body.ref;
    //    user.transactions.sens = req.body.sens;          //sens = entrant ou sortant
    //    user.transactions.type_transaction = req.body.type_transaction;       //type_transaction = portemonnaie ou caisse
    //    user.transactions.amount = req.body.amount;
    //
    //    var myquery = {email: req.body.email };
    //    var newvalues = { $set: {username: "toto", balance: 100 } };
    //
    //    user.updateOne(myquery, newvalues, function(err, res) {
    //    if (err) //throw err;
    //        res.send(err);
    //        //console.log("1 document updated");
    //    res.json({
    //       message: 'User updated',
    //       data: user
    //    });
    //    //db.close();
    //});



};




// Handle delete user  permet de supprimer un user
// exports.delete = function (req, res) {
//     User.remove({
//         _id: req.params.user_id
//     }, function (err, user) {
//         if (err)
//             res.status(401).json({
//                 status: 'error',
//                 message: 'User not exist'
//             });
//         res.status(200).json({
//             status: "success",
//             message: 'User deleted'
//         });
//     });
// };



//permet au user de se login
exports.login = function (req, res) {
    var key = 'salt_from_the_user_document';
    //var plaintext = req.body.password;
    //var cipher = crypto.createCipher('aes-256-cbc', key);
    var decipher = crypto.createDecipher('aes-256-cbc', key);

    var input = validator.isEmail(req.body.email_or_username);
    // var date_now = new Date(Date.now()).toISOString();

    if(input == true){  //alors dans ce cas c'est l'email qui est saisi

        User.findOne({email: req.body.email_or_username.toLowerCase()}, function(err, user){
            if (err){
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            }

            if(user != null){

                if (user.confirmed == true) {
                    if (user.suspended == false){
                        decipher.update(user.password, 'base64', 'utf8');   //décryptage du password enregistré en BD
                        var decryptedPassword = decipher.final('utf8');

                        if(decryptedPassword != req.body.password){
                            if(user.pwdFailed==true){
                                res.status(401).json({
                                    status: 'error',
                                    message: 'Your account has been blocked because of incorrect password, please reset your password'
                                });
                            }
                            else{
                                var ladate = new Date();
                                ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();
                                if(ladate!=user.date_connection_failed){
                                    User.findByIdAndUpdate(user._id, {number_connection_failed: 0, date_connection_failed: ladate}, function (err, user){
                                        if(err){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Could not update the number of failed connection and the date'
                                            });
                                        }
                                    });
                                }
                                User.findOne({email: req.body.email_or_username.toLowerCase()}, function(err, user){
                                    if(err){
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Unauthorized Access'
                                        });
                                    }
                                    User.findByIdAndUpdate(user._id, {number_connection_failed: user.number_connection_failed+1}, function (err, user){
                                        if(err){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Problem when updating'
                                            });
                                        }
                                    });
                                });
                                
                                    User.findOne({email: req.body.email_or_username.toLowerCase()}, function(err, user){
                                        if(err){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Unauthorized Access'
                                            });
                                        }
                                        if(user.number_connection_failed==4){
                                            // User.findByIdAndUpdate(user._id, {pwdFailed: true}, function (err, user){
                                            User.findByIdAndUpdate(user._id, {pwdFailed: true}, function (err, user){
                                                if(err){
                                                    res.status(401).json({
                                                        status: 'error',
                                                        message: 'Could not update the blocked option'
                                                    });
                                                }
                                                exports.motDePasse(user.email.toLowerCase(), user._id);
                                                res.status(401).json({
                                                    status: 'error',
                                                    message: 'Your account has been blocked because of incorrect password, we sent you a reset email for your password'
                                                });
                                            });
                                        }
                                        else{                                        
                                        counter = 4-user.number_connection_failed;
                                        if(counter<=0){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Unauthorized Access, We sent you a rest link by mail',
                                                user: user
                                            });
                                        }
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Unauthorized Access, ' + ' '+ counter + ' Attempts remaining',
                                            user: user
                                        });
                                    }
                                    });                                   
                                
                            }
                        }
                        else{
                            if(user.pwdFailed==true){
                                res.status(401).json({
                                    status: 'error',
                                    message: 'Your account has been blocked because of incorrect password, please reset your password'
                                });
                            }
                            const JWTToken = jwt.sign({email: user.email_or_username, _id: user._id}, 'secret', {expiresIn: '2h'});         //temps auquel on voudrait que le token du user expire


                            //on doit save le token
                            User.findByIdAndUpdate(user._id, {remember_token: JWTToken}, function (err, user) {
                                //if (err)
                                //    res.status(401).json({
                                //        status: 'error',
                                //        message: 'User not exist'
                                //    });
                                //
                                //res.status(200).json({
                                //    status: 'success',
                                //    message: 'User Info updated',
                                //    data: user
                                //});

                            });
                            var ladate = new Date();
                            ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();
                            User.findByIdAndUpdate(user._id, {number_connection_failed: 0, date_connection_failed: ladate}, function (err, user){
                                if(err){
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'Unauthorized lllllllll Access'
                                    });
                                }
                                return res.status(200).json({
                                    status: 'success',
                                    message: 'Log User ...',
                                    token: JWTToken,
                                    user: user
                                });
                            });

                            
                        }
                    }
                    else{
                        return res.status(401).json({
                            status: 'error',
                            message: 'Sorry dear customer, Your account has been suspended. Please contact our administrator !'
                        });
                    }
                    
                }
                else{                    
                    return res.status(401).json({
                        status: 'error',
                        message: 'you have to activate your account using the mail that has been sent to you'
                    });
                }

                
            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            }

        });

    }
    else{   //alors c'est le username qui est saisi

        User.findOne({username: req.body.email_or_username}, function(err, user){
            if (err){
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            }
            if(user != null){

                if (user.confirmed == true) {

                    decipher.update(user.password, 'base64', 'utf8');   //décryptage du password enregistré en BD
                    var decryptedPassword = decipher.final('utf8');

                    if(decryptedPassword != req.body.password){
                        if(user.pwdFailed==true){
                            res.status(401).json({
                                status: 'error',
                                message: 'Your account has been blocked because of incorrect password, please reset your password'
                            });
                        }
                        else{
                            var ladate = new Date();
                            ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();
                            if(ladate!=user.date_connection_failed){
                                User.findByIdAndUpdate(user._id, {number_connection_failed: 0, date_connection_failed: ladate}, function (err, user){
                                    if(err){
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Could not update the number of failed connection and the date'
                                        });
                                    }
                                });
                            }
                            User.findOne({username: req.body.email_or_username.toLowerCase()}, function(err, user){
                                if(err){
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'Unauthorized Access'
                                    });
                                }
                                User.findByIdAndUpdate(user._id, {number_connection_failed: user.number_connection_failed+1}, function (err, user){
                                    if(err){
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Problem when updating'
                                        });
                                    }
                                });
                            });
                                User.findOne({surname: req.body.email_or_username.toLowerCase()}, function(err, user){
                                    if(err){
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Unauthorized Access'
                                        });
                                    }
                                    if(user.number_connection_failed==4){
                                        User.findByIdAndUpdate(user._id, {pwdFailed: true}, function (err, user){
                                            if(err){
                                                res.status(401).json({
                                                    status: 'error',
                                                    message: 'Could not update the blocked option'
                                                });
                                            }
                                            exports.motDePasse(user.email.toLowerCase(), user._id);
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Your account has been blocked because of incorrect password, we sent you a reset email for your password'
                                            });
                                        });
                                    }
                                    else{
                                    
                                        counter = 4-user.number_connection_failed;
                                        if(counter<=0){
                                            res.status(401).json({
                                                status: 'error',
                                                message: 'Unauthorized Access, We sent you a rest link by mail',
                                                user: user
                                            });
                                        }
                                        res.status(401).json({
                                            status: 'error',
                                            message: 'Unauthorized Access, ' + ' '+ counter + ' Attempts remaining',
                                            user: user
                                        });
                                    }
                                });                                   
                            
                        }
                    }
                    else{
                        if(user.pwdFailed==true){
                            res.status(401).json({
                                status: 'error',
                                message: 'Your account has been blocked because of incorrect password, please reset your password'
                            });
                        }
                        const JWTToken = jwt.sign({email: user.email_or_username, _id: user._id}, 'secret', {expiresIn: '2h'});         //temps auquel on voudrait que le token du user expire


                        //on doit save le token
                        User.findByIdAndUpdate(user._id, {remember_token: JWTToken}, function (err, user) {
                            //if (err)
                            //    res.status(401).json({
                            //        status: 'error',
                            //        message: 'User not exist'
                            //    });
                            //
                            //res.status(200).json({
                            //    status: 'success',
                            //    message: 'User Info updated',
                            //    data: user
                            //});

                        });

                        var ladate = new Date();
                            ladate = ladate.getDay()+''+ladate.getDate()+''+ladate.getMonth()+''+ladate.getFullYear();
                            User.findByIdAndUpdate(user._id, {number_connection_failed: 0, date_connection_failed: ladate}, function (err, user){
                                if(err){
                                    res.status(401).json({
                                        status: 'error',
                                        message: 'Unauthorized lllllllll Access'
                                    });
                                }
                                return res.status(200).json({
                                    status: 'success',
                                    message: 'Log User ...',
                                    token: JWTToken,
                                    user: user
                                });
                            });

                    }
                }
                else{
                    return res.status(401).json({
                        status: 'error',
                        message: 'you have to activate your account using the mail that has been sent to you'
                    });
                }

            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'User not exist'
                });
            }

        });

    }



};


exports.delete = function (req, res) {
    User.deleteOne({
        username: 'b'
    }, function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        res.status(200).json({
            status: "success",
            message: 'User deleted'
        });
    });
};

exports.motDePasse = function(email, userId){
    /***** Envoyer le code par mail ************/
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        host: "smtp.gmail.com",
        // port: 587,
        // secure: false, // true for 465, false for other ports
        auth: {
            user: "moo.sdkgames@gmail.com", // generated ethereal user
            pass: "ajokfabdzfheudpq" // generated ethereal password
        }
    });

    var mailOptions = {
        from: "moo.sdkgames@gmail.com",//'help123backend',
        to: email,
        subject: 'Password reinitialization',
        text: 'Use this code to reset your password : ',
        html: "<html><p>You used all your connection attempts. Click on the link below to reset your password. </p><a title=\"Account validation for help123\" href=\"http://api.visavisbet.com/help123/#/blocpass?userId="+userId+"\">Reinitilire your password</a></html>"
    };

    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            console.log(error);
            res.status(401).json({
                status: 'error',
                message: 'Device not exist'
            });
        } else {
            console.log('Email sent: ' + info.response);
            res.json({
                status: DOMAIN,
                message: 'Users received successfully',
                data: "ok"  
            });
        }
        transporter.close();
    });
    /********* fin envoi mail  *************************/
}

// exports.enlever = function (req, res) {
    
//     Passwordreset.remove({} , function (err, user) {
//         if (err)
//             res.status(401).json({
//                 status: 'error',
//                 message: 'Code does not exist'
//             });
//         res.status(200).json({
//             status: "success",
//             message: 'Code deleted'
//         });
//     });
// };


exports.enlever = function (req, res) {
    
    User.update({ }, { $set: {suspended: false, confirmed: true}} , function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Sorry a problem occure'
            });     
        res.status(200).json({
            status: "success",
            message: 'All your users are unsuspended'
        });
    });
};

exports.mettreAdmin = function(req, res){
    User.findOneAndUpdate({username: 'lan-dry'},
        {
            title_role: 'administrator'
        },
        function(err, result){
            if (err)
            res.status(401).json({
                status: 'error',
                message: 'Sorry a problem occure, cannot define the admin'
            });     
            res.status(200).json({
                status: "success",
                message: 'the role admin has correctly been attributed to the user',
                infos: result
            });
        });
}

// db.users.update( { }, { $set: { suspended: false, confirmed: true } } )