/**
 * Created by christophe on 19/01/08.
 */

// Import choixDeliverAndBillHelpForAService model

ChoixDeliverAndBillHelpForAServiceController = require('./../Models/choixDeliverAndBillHelpForAServiceModel');

// Handle view choix deliver and bill address service user info   permet de GET toutes adresses choisies par le user pour un serviceAskForHelp_id
exports.view = function (req, res) {

    ChoixDeliverAndBillHelpForAServiceController.find({serviceAskForHelp_id:req.params.serviceaskforhelp_id},function(err, choix_address){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Choix address Services details loading..',
            data: choix_address
        });
    });

    //ChoixDeliverAndBillHelpForAServiceController.get(function(err, choix_address){
    //    if (err){
    //        res.status(500).json({
    //            status: 'error',
    //            message: 'Internal Problem Server'
    //        });
    //    }
    //
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Choix address Services details loading..',
    //        data: choix_address
    //    });
    //});

};



