/**
 * Created by christophe on 18/09/26.
 */

// Import service model
Service = require('./../Models/serviceModel');
CategoryService = require('./../Models/categoryServiceModel');

// Handle index actions     permet de get tous les services
exports.index = function (req, res) {
    Service.get(function (err, services) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Services retrieved successfully",
            data: services
        });
    });
};



// Handle create services actions    permet de save(post) un service
exports.new = function (req, res) {
    var service = new Service();
    service.serviceName = req.body.serviceName;
    service.description = req.body.description;
    service.categoryServiceId = req.body.categoryServiceId;
    service.update_date = new Date(Date.now()).toISOString();
    //service.categoryName = null;

    //on se rassure si categoryService existe
    CategoryService.findById(req.body.categoryServiceId, function (err, categoryservice) {
        if (err)
            //res.send(err);
            /*res.status(500).json({
               status: 'error',
               message: 'Internal Problem Server'
            });*/
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });

        if(categoryservice == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });
        }

        //ici on est sûr que la catégorie service Id existe
        //alors on mets dans le modèle service categoryName
        //var service = new Service();
        /*service.serviceName = req.body.serviceName;
        service.description = req.body.description;
        service.categoryServiceId = req.body.categoryServiceId;*/
        service.categoryName = categoryservice.categoryName;
        
        //res.send(service);
        
        //on se rassure si serviceName n'existe pas encore pour la categoryService
    Service.findOne({serviceName:req.body.serviceName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
        if(err)
            //res.send(err);
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(data == null) {   //dans ce cas le serviceName n'existe pas encore pour la categoryService
            // save the service and check for errors
            service.save(function (err) {
                if (err)
                    //res.send(err);
                    res.status(500).json({
                       status: 'error',
                       message: 'Internal Problem Server'     
                    });
                    
                res.status(200).json({
                    status: 'success',
                    message: 'New service created!',
                    data: service
                });
            });
        }
        else{
            res.status(401).json({
                status: 'error',
                message: 'serviceName already exist for this categoryService'
            });
        }    
    });
        
        
        
    });


    
};



// Handle view service info   permet de GET un service à partir de son Id
exports.view = function (req, res) {
    Service.findById(req.params.service_id, function (err, service) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId service not exist'
            });
        }
        else{
            res.json({
                status: 'error',
                message: 'Service details loading..',
                data: service
            });
        }
    });
};



// Handle update service info  permet d'update un service
exports.update = function (req, res) {
    var categoryName = null;

    //on se rassure si categoryService existe
    CategoryService.findById(req.body.categoryServiceId, function (err, categoryservice) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(categoryservice == null) {
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });
        }
        else{
            categoryName = categoryservice.categoryName;
        }
    });

    //on se rassure si serviceName n'existe pas encore pour la categoryService
    Service.findOne({serviceName:req.body.serviceName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
        if(err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(data == null) {   //dans ce cas le serviceName n'existe pas encore pour la categoryService
                
                //on fait alors un update du serviceName
                Service.findByIdAndUpdate(req.params.service_id,
                {
                    serviceName: req.body.serviceName,
                    categoryServiceId: req.body.categoryServiceId,
                    description: req.body.description,
                    categoryName: categoryName,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, service) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if(service == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId Service not exist'
                        });
                    }
                    else{
                            //je capture en BD les nouvelles données enregistrées
                            Service.findOne({serviceName:req.body.serviceName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
                                res.status(200).json({
                                    status: 'success',
                                    message: 'Service Info updated',
                                    data: data
                                });
                            });
                    }
                });
        }
        else{
            res.status(401).json({
                status: 'error',
                message: 'serviceName already exist for this categoryService'
            });
        }    
    });
};



// Handle delete service  permet de supprimer un service
exports.delete = function (req, res) {
    Service.remove({
        _id: req.params.service_id
    }, function (err, service) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId Service not exist'
            });
        }
        else {
            res.status(200).json({
                status: "success",
                message: 'Service deleted'
            });
        }
    });
};

