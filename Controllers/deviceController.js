/**
 * Created by christophe on 19/02/11.
 */

// Import device model
Device = require('./../Models/deviceModel');
WalletUser = require('./../Models/walletUserModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');
Bid = require('./../Models/bidModel');


// Handle index actions     permet de get toutes les devices
exports.index = function (req, res) {
    Device.get(function (err, devices) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Devices retrieved successfully",
            data: devices
        });
    });
};



// Handle create devices actions    permet de save(post) une device (monnaie)
exports.new = function (req, res) {
    var device = new Device();
    device.name = req.body.name;
    device.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le name de device là n'existe pas encore
    Device.findOne({name:req.body.name}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null) { //dans ce cas name device existe déjà
            res.status(401).json({
                status: 'error',
                message: 'This device name already exist'
            });
        }

        // save the device name and check for errors
        device.save(function (err) {
            if (err) {
                res.status(500).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });
            }


            res.status(200).json({
                status: 'success',
                message: 'New device created!',
                data: device
            });
        });
    });
};



// Handle view device info   permet de GET une device à partir de son Id
exports.view = function (req, res) {
    Device.findById(req.params.device_id, function (err, device) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Device ObjectId not exist'
            });

        if (device == null){
            res.status(401).json({
                status: 'error',
                message: 'Device ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Device details loading..',
            data: device
        });
    });
};



// Handle update device info  permet d'update une device
exports.update = function (req, res) {

    //je me rassure si ce nom de device n'existe pas dans le document device
    Device.findOne({name: req.body.name}, function(err, data1){

        if (data1 != null){

            if(data1.name == req.body.name){
                //on ne fait rien et on retourne juste les données
                //je capture en BD les nouvelles données enregistrées
                Device.findOne({_id: req.params.device_id}, function(err, data_new){
                    res.status(200).json({
                        status: 'success',
                        message: 'Device Info updated',
                        data: data_new
                    });
                });
            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'Device name already exist'
                });
            }
        }
        else{

            Device.findByIdAndUpdate(req.params.device_id,
                {
                    name: req.body.name,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, device) {
                    if (err)
                        res.status(401).json({
                            status: 'error',
                            message: 'Device ObjectId not exist'
                        });


                    //je dois me rassurer que nom_langue n'existe pas déjà
                    if(device != null){
                        if(device.name == req.body.name){
                            //on ne fait rien et on retourne juste les données
                            //je capture en BD les nouvelles données enregistrées
                            Device.findOne({_id: req.params.device_id}, function(err, data_new){
                                res.status(200).json({
                                    status: 'success',
                                    message: 'Device Info updated',
                                    data: data_new
                                });
                            });
                        }
                        else{

                            //je me rassure si ce nom de device n'existe pas dans le document device
                            Device.findOne({name: req.body.name}, function(err, data1){

                                /*if (data1 != null){
                                 res.status(401).json({
                                 status: 'error',
                                 message: 'Language name already exist'
                                 });
                                 }*/
                                //else{

                                //ici je update le nom de la device dans Device, dans le wallet de tous les users, dans transactionsModel et dans tansactionWalletUserModel
                                var myquery = { _id: req.params.device_id };
                                var newvalues = {$set: {name: req.body.name} };

                                Device.updateMany(myquery, newvalues, function(err, res) {
                                    //if (err){}
                                });

                                //j'update aussi dans le Wallet de tous les users
                                var myquery2 = {device_id: req.params.device_id};
                                var newvalues2 = {$set: {name_device: req.body.name}};

                                WalletUser.updateMany(myquery2, newvalues2, function(err, res){});

                                //j'update aussi dans transactionsModel
                                var myquery3 = {device_id: req.params.device_id};
                                var newvalues3 = {$set: {device_name: req.body.name}};

                                Transactions.updateMany(myquery3, newvalues3, function(err, res){});

                                //j'update aussi dans transactionWalletUserModel
                                var myquery4 = {device_id: req.params.device_id};
                                var newvalues4 = {$set: {device_name: req.body.name}};

                                TransactionWalletUser.updateMany(myquery4, newvalues4, function(err, res){});

                                //j'update aussi dans serviceAskForHelpModel
                                var myquery5 = {device_id: req.params.device_id};
                                var newvalues5 = {$set: {device_name: req.body.name}};

                                ServiceAskForHelp.updateMany(myquery5, newvalues5, function(err, res){});


                                //j'update aussi dans bidModel
                                var myquery6 = {device_id: req.params.device_id};
                                var newvalues6 = {$set: {device_name: req.body.name}};

                                Bid.updateMany(myquery6, newvalues6, function(err, res){});


                                //je capture en BD les nouvelles données enregistrées
                                Device.findOne({_id: req.params.device_id}, function(err, data){
                                    res.status(200).json({
                                        status: 'success',
                                        message: 'Device Info updated',
                                        data: data
                                    });
                                });
                                //}

                            });
                        }
                    }
                });

        }

    });





};



// Handle delete device   permet de supprimer une device
exports.delete = function (req, res) {
    //je dois me rassurer que cette device n'est pas encore utilisée par un user
    WalletUser.findOne({device_id: device._id}, function(err, devices){
        //if (err){
        //    res.status(401).json({
        //        status: 'error',
        //        message: 'Device is already used for an user. Impossible to delete !!!'
        //    });
        //}

        if (devices != null){ //ça veut dire que la device a déjà été utilisée par un user
            res.status(401).json({
                status: 'error',
                message: 'Device is already used for an user. Impossible to delete !!!'
            });
        }
        else{
            Device.remove({
                _id: req.params.device_id
            }, function (err, device) {
                if (err)
                    res.status(500).json({
                        status: 'error',
                        message: 'Internal Problem Server'
                    });

                if(device == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'ObjectId Device not exist'
                    });
                }
                else{
                    res.status(200).json({
                        status: 'success',
                        message: 'Device deleted'
                    });
                }
            });

            //res.status(200).json({
            //    status: 'success',
            //    message: 'Device deleted'
            //});
        }
    });




};


