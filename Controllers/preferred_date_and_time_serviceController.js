/**
 * Created by christophe on 19/01/08.
 */

// Import preferred_date_and_time_service model

Preferred_date_and_time_service = require('./../Models/preferred_date_and_time_serviceModel');

// Handle view preferred date and time service user info   permet de GET toutes les dates et heures préférées d'un serviceAskForHelp_id
exports.view = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    Preferred_date_and_time_service.find({serviceAskForHelp_id:req.params.serviceaskforhelp_id},function(err, preferred_date_and_time){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Preferred Date and Time Services details loading..',
            data: preferred_date_and_time
        });
    }).sort(mysort);

    //Preferred_date_and_time_service.get(function(err, preferred_date_and_time){
    //    if (err){
    //        res.status(500).json({
    //            status: 'error',
    //            message: 'Internal Problem Server'
    //        });
    //    }
    //
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Preferred Date and Time Services details loading..',
    //        data: preferred_date_and_time
    //    });
    //});


};



