/**
 * Created by Landry on 02/10/2020.
 */


// Import notifications model
Notifications = require('./../Models/notificationsModel');
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');
User = require('./../Models/userModel');
var randomInt = require('random-int');      //pour faire des random sur les entiers
var nodemailer = require('nodemailer');     //pour l'envoi des mails
//const bcrypt = require('bcrypt');    //pour crypter les passwords
//const saltRounds = 10;      //constante pour la taille du password crypté

var crypto = require('crypto');

// permet de créer une nouvelle notification
exports.new = function (req, res){
	var notifications = new Notifications();
	notifications.serviceAskForHelp_id = req.body.serviceAskForHelp_id;
	notifications.content = req.body.content;
	notifications.username = "";
	notifications.read = false;
	notifications.create_date = new Date(Date.now()).toISOString();
	notifications.update_date = new Date(Date.now()).toISOString();
	ServiceAskForHelp.findById(req.body.serviceAskForHelp_id, function (err, serviceAskForHelp) {
		if(err)
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        if (serviceAskForHelp == null){
            res.status(401).json({
                status: 'error',
                message: 'serviceAskForHelp ObjectId not exist'
            });
        }
        	 
        	var user_id = serviceAskForHelp.user_id;
        	notifications.user_id = user_id;
        	        	
        	notifications.save(function(err, notifications){
                if(err){
                    res.status(401).json({
                        status: 'error',
                        message: 'The notification couldn\'t be saved'
                    });
                }
                res.status(200).json({
                    status: 'success',
                    message: 'The notification has been succesfully saved!',
                    data: notifications
                });

            });

    });
};

exports.getNotifications = function(req, res){
	Notifications.find({user_id: req.params.user_id}, function (err, notifications) {
        if (notifications == null){
            res.status(401).json({
                status: 'error',
                message: 'Notifications ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Notifications details loading..',
            data: notifications
        });
    });

 	// Notifications.get(function (err, notifications) {
  //       if (err) {
  //           res.status(401).json({
  //               status: 'error',
  //               message: 'Internal Problem Server'
  //           });
  //       }
  //       res.json({
  //           status: 'success',
  //           message: 'notifications retrieved successfully',
  //           data: notifications          //users avec s ici représente la base de données users
  //       });
  //   });
};


exports.readNotifications = function (req, res) {
	Notifications.findByIdAndUpdate(req.params.notif_id,
    {
        read: true,
        update_date: new Date(Date.now()).toISOString()
    },
    function (err, data) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Could not be able to read the notification'
            });

        //je capture en BD les nouvelles notifications
        Notifications.find({user_id: data.user_id}, function(err, notifications){
            res.status(200).json({
                status: 'success',
                message: 'Notification read successfully',
                data: notifications
            });
        });
    });
}


exports.delete = function (req, res) {
    Notifications.deleteOne({
        _id: req.params.notif_id
    }, function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Note not exist'
            });
        res.status(200).json({
            status: "success",
            message: 'Note deleted successfully'
        });
    });
};