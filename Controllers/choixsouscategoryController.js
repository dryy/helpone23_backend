/**
 * Created by christian on 18/12/07.
 */


Choixsouscategory = require('./../Models/choixsouscategoryModel');
Languages = require('./../Models/languagesModel');


// Handle index actions     permet de get tous les users
exports.index_choix = function (req, res) {
    Choixsouscategory.get(function (err, choixsouscategory) {
        if (err) {
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.json({
            status: 'success',
            message: 'Choose Sous Category retrieved successfully',
            data: choixsouscategory
        });
    });
};


exports.index_langue = function (req, res) {
    Languages.get(function (err, languages) {
        if (err) {
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.json({
            status: 'success',
            message: 'Languages retrieved successfully',
            data: languages
        });
    });
};



