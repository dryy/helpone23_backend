/**
 * Created by christophe on 19/01/22.
 */

// Import bid model
Bid = require('./../Models/bidModel');
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');
ChoixDeliverAndBillHelpForAService = require('./../Models/choixDeliverAndBillHelpForAServiceModel');
Image_service = require('./../Models/image_serviceModel');
Preferred_date_and_time_service = require('./../Models/preferred_date_and_time_serviceModel');
User = require('./../Models/userModel');
WalletUser = require('./../Models/walletUserModel');
Argent_en_attente_pour_finition_service = require('./../Models/argent_en_attente_pour_finition_serviceModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
CategoryService = require('./../Models/categoryServiceModel');
SousCategory = require('./../Models/souscategoryModel');
AddressUser = require('./../Models/addressUserModel');
Role = require('./../Models/roleModel');
ChoixSousCategory = require('./../Models/choixsouscategoryModel');
Languages = require('./../Models/languagesModel');



// permet d'update la location où le user se trouve pour l'aide
exports.update_location = function (req, res) {
    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }

        if(data != null){
            User.findByIdAndUpdate(req.params.user_id,
                {
                    longitude_location: req.body.longitude_location,
                    latitude_location: req.body.latitude_location,
                    location_name: req.body.location_name,
                    update_date: new Date(Date.now()).toISOString()

                }, function(err, result){
                    User.findById(req.params.user_id, function(err, result){
                        res.status(200).json({
                            status: 'success',
                            message: 'User details loading..',
                            data: result //result[1][0]

                        });
                    });
                });
        }
    });

};



//permet d'update les skills (compétences d'un user) qui sont enregistrées dans choixsouscategoryModel
exports.update_skill = function (req, res) {
    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }

        if(data != null){
            //j'efface toutes les sous-catégories du user et je prends le nouveau tableau JSON envoyé
            ChoixSousCategory.remove({user_id: req.params.user_id}, function (err, user) {});

            var user_id = req.params.user_id;
            //Je prends le tableau d'objets ici
            var tableau_souscategories = req.body.souscategories;



            var choixsouscategoryarray = new Array();

            //je stocke les objets du tableau dans un tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i] = new ChoixSousCategory();

                choixsouscategoryarray[i].souscategory_id = tableau_souscategories[i].sousCategorie_id;
                choixsouscategoryarray[i].souscategoryName = tableau_souscategories[i].sousCategoryName;
                choixsouscategoryarray[i].user_id = user_id;

                //choixsouscategory.save();
            }

            //res.status(200).json({
            //    status: 'success',
            //    message: 'Skills details loading..',
            //    data: choixsouscategoryarray
            //});

            //j'enregistre chaque objet du tableau
            for(var i=0;i<tableau_souscategories.length;i++){
                choixsouscategoryarray[i].save(function(){});
            }

            //puis je dois renvoyer tous les choix faits par le user pour ses skills (compétences)
            //ChoixSousCategory.find({user_id: req.params.user_id}, function(err, choixsouscategoryresult){
            //    if (err){
            //        res.status(500).json({
            //            status: 'error',
            //            message: 'Internal Problem Server'
            //        });
            //    }
            //
            //    res.status(200).json({
            //        status: 'success',
            //        message: 'Skills details loading..',
            //        data: choixsouscategoryresult
            //    });
            //});

            res.status(200).json({
                status: 'success',
                message: 'Skills details loading..',
                data: choixsouscategoryarray
            });

        }
    });

};



//permet de get tous les services différents de celui qui est connecté (user) avec pour statut 'in_progress'
exports.get_service = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le user existe
    User.findById(req.params.user_id, function(err, data){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'User not exist'
            });
        }
    });

    //on renvoit tous les services en cours qui ne sont pas créés par le user connecté
    ServiceAskForHelp.find({user_id:{'$ne':req.params.user_id},status:'in_progress'},function(err, service){

        res.status(200).json({
            status: 'success',
            message: 'Services details loading..',
            data: service
        });


    }).sort(mysort);

    //var Doc = new Array();
    //var result = [];
    ////v//ar resultat =
    //var i =0;
    //
    //ServiceAskForHelp.find({user_id:{'$eq':req.params.user_id},status:'in_progress'},function(err, Doc){
    //    Doc.forEach(function(myDoc){
    //        //je recherche pour le user correspondant
    //        User.find({_id: myDoc.user_id}, function(err, user){
    //            //result[i] = new User();
    //            result.push(user);
    //            //i = i+1;
    //
    //            //res.status(401).json({
    //            //    status: 'success',
    //            //    message: 'Services details loading..',
    //            //    data: user/*myDoc.user_id*/
    //            //});
    //
    //            var resultat = [myDoc, result];
    //
    //            res.status(401).json({
    //                status: 'success',
    //                message: 'Services details loading..',
    //                data: resultat/*resultat[1][0][0]*//*myDoc.user_id*/
    //            });
    //
    //        });
    //
    //        //res.status(401).json({
    //        //    status: 'success',
    //        //    message: 'Services details loading..',
    //        //    data: result/*myDoc.user_id*/
    //        //});
    //
    //    });
    //
    //
    //});




    //var result = ServiceAskForHelp.aggregate([
    //    {
    //        $lookup:
    //        {
    //            from: "User",
    //            localField: "user_id",
    //            foreignField: "_id",
    //            as: "userarray"
    //        }
    //    }
    //], function(err, resultat){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Services details loading..',
    //        data: resultat
    //    });
    //
    //    //console.log(err, resultat)
    //
    //});


};


//permet de get tous les bidders d'un service donné
exports.get_bidders = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le service existe
    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function(err, service){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            //je récupère tous les bidders dans bidModel
            Bid.find({serviceAskForHelpId:req.params.serviceaskforhelp_id},function(err, bidders){
                res.status(200).json({
                    status: 'success',
                    message: 'Bidders details loading..',
                    data: bidders
                });
            });
        }
    });
};


//permet de save le bid d'un user
exports.saveBid = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    var bid = new Bid();
    bid.serviceAskForHelpId = req.body.serviceAskForHelp_id;
    bid.user_id_celui_qui_bid = req.body.user_id_celui_qui_bid;
    bid.user_id_celui_qui_offre_service = req.body.user_id_celui_qui_offre_service;
    bid.price_bid = req.body.price_bid;
    bid.date_livraison = req.body.date_livraison;
    bid.time_livraison = req.body.time_livraison;
    bid.description_proposition = req.body.description;
    bid.status_service = 'in_progress';


    //on se rassure si le service existe
    ServiceAskForHelp.findById(req.body.serviceAskForHelp_id, function(err, service){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            //je me rassure si le user_id_celui_qui_bid existe
            User.findById(req.body.user_id_celui_qui_bid, function(err, user_celui_qui_bid){
                if(user_celui_qui_bid == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'Bidder User not exist'
                    });
                }
                else{
                    //je récupère ces paramètres
                    bid.username_celui_qui_bid = user_celui_qui_bid.username;
                    bid.firstname_celui_qui_bid = user_celui_qui_bid.firstname;
                    bid.location_name_celui_qui_bid = user_celui_qui_bid.location_name;
                    bid.number_helps_asked_celui_qui_bid = user_celui_qui_bid.number_helps_asked;
                    bid.number_helps_offer_celui_qui_bid = user_celui_qui_bid.number_helps_offer;
                    bid.nbre_etoiles_celui_qui_bid = user_celui_qui_bid.nbre_etoiles;
                    bid.picture_celui_qui_bid = user_celui_qui_bid.picture;

                    //je me rassure si le user_id_celui_qui_offre_service existe
                    User.findById(req.body.user_id_celui_qui_offre_service, function(err, user_celui_qui_offre_service){
                        if(user_celui_qui_offre_service == null){
                            res.status(401).json({
                                status: 'error',
                                message: 'Bidder User not exist'
                            });
                        }
                        else{
                            //je récupère ces paramètres
                            bid.username_celui_qui_offre_service = user_celui_qui_offre_service.username;
                            bid.firstname_celui_qui_offre_service = user_celui_qui_offre_service.firstname;
                            bid.location_name_celui_qui_offre_service = user_celui_qui_offre_service.location_name;
                            bid.number_helps_asked_celui_qui_offre_service = user_celui_qui_offre_service.number_helps_asked;
                            bid.number_helps_offer_celui_qui_offre_service = user_celui_qui_offre_service.number_helps_offer;
                            bid.nbre_etoiles_celui_qui_offre_service = user_celui_qui_offre_service.nbre_etoiles;
                            bid.picture_celui_qui_offre_service = user_celui_qui_offre_service.picture;


                            //Enregistrement des infos du serviceAskForHelp dans bidModel.js
                            bid.categoryService_id = service.categoryService_id;
                            bid.sousCategory_id = service.sousCategory_id;
                            bid.description = service.description;
                            bid.min_price = service.min_price;
                            bid.max_price = service.max_price;
                            //bid.nombre_bids = service.nombre_bids;
                            //bid.Avg_bids = service.Avg_bids;
                            bid.nom_service = service.nom_service;
                            bid.city_deliver_help = service.city_deliver_help;
                            bid.preferred_date_service = service.preferred_date_service;
                            bid.preferred_time_service = service.preferred_time_service;
                            bid.device_id = service.device_id;
                            bid.device_name = service.device_name;
                            bid.etoiles_donnees_par_offer_service = service.etoiles_donnees_par_offer_service;
                            bid.commentaire_offer_service = service.commentaire_offer_service;



                            //maintenant j'enregsitre le bid du user
                            bid.save(function(err, bidder){

                                //je mets à jour nombre_bids et Avg_bids dans ServiceAskForHelpModel
                                //je sélectionne tous les bids faits pour le serviceAskForHelp
                                Bid.find({serviceAskForHelpId:req.body.serviceAskForHelp_id}, function(err, bids){
                                    var somme_bid = 0;
                                    for(var i=0; i<bids.length; i++){
                                        somme_bid = somme_bid + bids[i].price_bid;
                                    }

                                    var quotient = Math.floor(somme_bid/bids.length);
                                    var remainder = somme_bid % bids.length;

                                    ServiceAskForHelp.findByIdAndUpdate(req.body.serviceAskForHelp_id,
                                        {
                                            nombre_bids: bids.length,
                                            Avg_bids: quotient,
                                            update_date: new Date(Date.now()).toISOString()
                                        }
                                        ,function(err, result){});

                                    bid.nombre_bids = bids.length;
                                    bid.Avg_bids = quotient;

                                    //je mets aussi nombre_bids et Avg_bids dans bidModel de tous les users ayant biddé pour le serviceAshForHelp_id
                                    var myquery = { serviceAskForHelpId: req.body.serviceAskForHelp_id };
                                    var newvalues = {$set: {
                                        nombre_bids: bids.length,
                                        Avg_bids: quotient
                                    }
                                    };

                                    Bid.updateMany(myquery, newvalues, function(err, res) {
                                        //if (err){}
                                    });


                                });


                                //je récupère tous les bidders dans bidModel
                                res.status(200).json({
                                    status: 'success',
                                    message: 'New Bidder User created!',
                                    data: bidder
                                });

                            });

                        }
                    });

                }
            });

        }
    });
};



//permet de mettre à jour number_helps_asked, number_helps_offer, nbre_etoiles et location_name pour un user
exports.mettre_a_jour_user = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le service existe
    ServiceAskForHelp.find({user_id:req.params.user_id}, function(err, service){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            var number_helps_asked = service.length;
            var number_helps_offer = 0;
            var nbre_etoiles = 0;

            User.findByIdAndUpdate(req.params.user_id,
                {
                    number_helps_asked: number_helps_asked,
                    number_helps_offer: number_helps_offer,
                    nbre_etoiles: nbre_etoiles
                },
                function(err, data){
                    var myquery = { user_id: req.params.user_id };
                    var newvalues = {$set: {
                        nbre_etoiles: nbre_etoiles
                    }
                    };

                    if(data.location_name == null){
                        var location_name = "";
                        User.findByIdAndUpdate(req.params.user_id,
                            {
                                location_name: location_name
                            },
                            function(err, resultat){});
                    }

                    ServiceAskForHelp.updateMany(myquery, newvalues, function(err, res) {
                        //if (err){}
                    });

                    User.findById(req.params.user_id,function(err, data){
                        res.status(200).json({
                            status: 'error',
                            message: 'User and Service updated!',
                            data: data
                        });
                    });



                });

        }
    });
};


//permet de mettre à jour nom_service dans le service n'ayant pas de nom
exports.mettre_a_jour_nom_service = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    //on se rassure si le service existe
    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id, function(err, service){
        //if(err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{

            SousCategory.findById(service.sousCategory_id,function( err, souscategory){
                if (souscategory == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'SousCategory Service not exist'
                    });
                }
                else{
                    //je prends en paramètre le nom de la souscategory comme nom_service
                    var nom_service = souscategory.souscategoryName + ' '+ Math.random().toString(36).substr(2, 2) + '-'+ Math.floor(Math.random() * 100000);


                    ChoixDeliverAndBillHelpForAService.find({serviceAskForHelp_id: req.params.serviceaskforhelp_id},function(err, choix){
                        if(choix.length != 0){
                            var tableau_deliverandbillhelpforaservice = choix;
                            var location_name = "";
                            var city_deliver_help = null;
                            for(var i=0;i<choix.length;i++){
                                if(tableau_deliverandbillhelpforaservice[i].deliver_help_to_this_address == 1){
                                    //pour dire que c'est cette adresse qui a été sélectionnée
                                    city_deliver_help = tableau_deliverandbillhelpforaservice[i].city;
                                }
                            }

                            if(city_deliver_help == null){
                                city_deliver_help = location_name;
                            }

                        }
                        else{
                            city_deliver_help = "";
                        }


                        Preferred_date_and_time_service.find({serviceAskForHelp_id: req.params.serviceaskforhelp_id},function(err, preferred){
                            if(preferred.length != 0){
                                //continuons avec preferred_date_service et preferred_time_service
                                var tableau_preferred_date_and_time_service = preferred;

                                //j'initialise preferred_date_service et preferred_time_service dans l'objet serviceAskForHelp
                                var preferred_date_service = preferred[0].date;
                                var preferred_time_service = preferred[0].time;

                                //res.status(200).json({
                                //    status: 'success',
                                //    message: 'Preferred!',
                                //    data: preferred
                                //});

                            }
                            else{
                                preferred_date_service = "";
                                preferred_time_service = "";
                            }




                            //j'update maintenant
                            ServiceAskForHelp.findByIdAndUpdate(req.params.serviceaskforhelp_id,
                                {
                                    nom_service: nom_service,
                                    nombre_bids: 0,
                                    Avg_bids: 0,
                                    city_deliver_help: city_deliver_help,
                                    preferred_date_service: preferred_date_service,
                                    preferred_time_service: preferred_time_service
                                },
                                function(err, result){
                                    ServiceAskForHelp.findById(req.params.serviceaskforhelp_id,function(err,result){
                                        res.status(200).json({
                                            status: 'success',
                                            message: 'Service Name updated!',
                                            data: result
                                        });
                                    });


                                });


                        });


                    });


                }
            });

        }
    });
};




//permet d'ajouter certains champs de serviceAskForHelp dans bidModel; ceci pour les services déjà créés
exports.update_fields_bidModel = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    ServiceAskForHelp.findById(req.params.serviceAskForHelp_id, function(err, service){

        if(service == null){
            res.status(401).json({
                status: 'error',
                message: 'Service not exist'
            });
        }
        else{
            var myquery = {serviceAskForHelpId: req.params.serviceAskForHelp_id};
            var newvalues = {$set: {
                categoryService_id: service.categoryService_id,
                sousCategory_id: service.sousCategory_id,
                description: service.description,
                min_price: service.min_price,
                max_price: service.max_price,
                nombre_bids: service.nombre_bids,
                Avg_bids: service.Avg_bids,
                nom_service: service.nom_service,
                city_deliver_help: service.city_deliver_help,
                preferred_date_service: service.preferred_date_service,
                preferred_time_service: service.preferred_time_service,
                device_id: service.device_id,
                device_name: service.device_name,
                etoiles_donnees_par_offer_service: service.etoiles_donnees_par_offer_service,
                commentaire_offer_service: service.commentaire_offer_service
            }
            };

            Bid.updateMany(myquery, newvalues, function(err, res){
                //res.status(200).json({
                //    status: 'success',
                //    message: 'Bid updated successful !!!',
                //    data: res
                //});

            });

            Bid.find({serviceAskForHelpId: req.params.serviceAskForHelp_id}, function (err, new_bid){
                res.status(200).json({
                    status: 'success',
                    message: 'Bid updated successful !!!',
                    data: new_bid
                });
            });


        }
    });
};
