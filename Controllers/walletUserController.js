/**
 * Created by christophe on 19/01/04.
 */

// Import walletUser model
WalletUser = require('./../Models/walletUserModel');
User = require('./../Models/userModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
Device = require('./../Models/deviceModel');


// Handle view wallet user info   permet de GET le portefeuille du user
exports.view = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien

    WalletUser.find({user_id: req.params.user_id}, function(err, wallet){
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        res.status(200).json({
            status: 'success',
            message: 'Wallet User details loading..',
            data: wallet
        });
    }).sort(mysort);

};






// Handle create wallet user actions    permet de charger le portefeuille du user
exports.new = function (req, res) {
    var wallet = new WalletUser();
    wallet.user_id = req.body.user_id;
    wallet.amount = parseInt(req.body.amount);

    var methodpayment_id = req.body.methodpayment_id;
    var methodpayment_name = req.body.methodpayment_name;

    wallet.update_date = new Date(Date.now()).toISOString();

    var device_id = "5c6a917f9522db5b1e2642e5";         //ici je renseigne l'ObjectId de la device créé
    wallet.device_id = device_id;

    var name_device = "";

    //je contrôle si methodpayment_name = 'Carte Visa' ou methodpayment_name = 'Wallet' alors je renvoie un message d'erreur
    //où le user doit choisir la méthode de paiement = PayPal
    if (methodpayment_name == 'Carte Visa' /*|| methodpayment_name == 'Carte Visa'*/){
      //message d'erreur
      res.status(401).json({
                    status: 'error',
                    message: 'Please choose the PayPal payment method to recharge your account !!!'
                });
    }
    else if(methodpayment_name == 'Wallet'){
      //message d'erreur
      res.status(401).json({
                    status: 'error',
                    message: 'Please choose the PayPal payment method to recharge your account !!!'
                });
    }
    else{


              //puis on cherche le name_device
              Device.findById(device_id,function(err, device){
              name_device = device.name;
              wallet.name_device = name_device;

              var transactions = new Transactions();
              var transactionWalletUser = new TransactionWalletUser();


              //on se rassure si le user existe
              User.findOne({_id:req.body.user_id}, function(err, data_user){
                  if(err) {
                      res.status(500).json({
                          status: 'error',
                          message: 'Internal Problem Server'
                      });
                  }

                  if(data_user == null) { //alors le user n'existe pas
                      res.status(401).json({
                          status: 'error',
                          message: 'User not exist'
                      });
                  }
                  else{

                      //on se rassure s'il y a déjà un enregistrement propre au user pour son solde portemonnaie
                      WalletUser.findOne({user_id:req.body.user_id, device_id:device_id}, function(err, data){
                          if(err) {
                              res.status(500).json({
                                  status: 'error',
                                  message: 'Internal Problem Server'
                              });
                          }

                          if(data != null) { //alors il y a une occurence d'enregistrement et on doit tout simplement faire un update

                              var myquery = { user_id: req.body.user_id, device_id: device_id };

                              WalletUser.findByIdAndUpdate(data._id,
                                  {
                                      amount: data.amount + parseInt(req.body.amount),
                                      update_date: new Date(Date.now()).toISOString()
                                  },
                                  function (err, data_wallet) {
                                      if (err)
                                          res.status(500).json({
                                              status: 'error',
                                              message: 'Internal Problem Server'
                                          });

                                      var reference_transaction = 'Cash-deposit-' + /*Math.random().toString(36).substr(2, 2) + '-'+*/ Math.floor(Math.random() * 1000000000000000);

                                      //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                      transactions.type_transaction = 'wallet';
                                      transactions.amount = parseInt(req.body.amount);
                                      transactions.sens = 'entrant';
                                      transactions.user_id = req.body.user_id;
                                      transactions.reference_transaction = reference_transaction;
                                      transactions.device_id = device_id;
                                      transactions.device_name = name_device;
                                      transactions.methodpayment_id = methodpayment_id;
                                      transactions.methodpayment_name = methodpayment_name;
                                      transactions.save();

                                      transactionWalletUser.user_id = req.body.user_id;
                                      transactionWalletUser.amount = parseInt(req.body.amount);
                                      transactionWalletUser.sens = 'entrant';
                                      transactionWalletUser.reference_transaction = reference_transaction;
                                      transactionWalletUser.device_id = device_id;
                                      transactionWalletUser.device_name = name_device;
                                      transactionWalletUser.methodpayment_id = methodpayment_id;
                                      transactionWalletUser.methodpayment_name = methodpayment_name;
                                      transactionWalletUser.save();


                                      WalletUser.findOne({user_id: req.body.user_id, device_id: device_id}, function(err, data_new){
                                          res.status(200).json({
                                              status: 'success',
                                              message: 'New Wallet User created!',
                                              data: data_new
                                          });

                                      });



                                  });

                          }
                          else{
                              // save wallet user and check for errors
                              wallet.save(function (err) {
                                  if (err) {
                                      res.status(500).json({
                                          status: 'error',
                                          message: 'Internal Problem Server'
                                      });
                                  }

                                  var reference_transaction = 'Cash-deposit-' + /*Math.random().toString(36).substr(2, 2) + '-'+*/ Math.floor(Math.random() * 1000000000000000);

                                  //puis j'enregistre la transaction effectuée dans Transactions et transactionWalletUser
                                  transactions.type_transaction = 'wallet';
                                  transactions.amount = parseInt(req.body.amount);
                                  transactions.sens = 'entrant';
                                  transactions.user_id = req.body.user_id;
                                  transactions.reference_transaction = reference_transaction;
                                  transactions.device_id = device_id;
                                  transactions.device_name = name_device;
                                  transactions.methodpayment_id = methodpayment_id;
                                  transactions.methodpayment_name = methodpayment_name;
                                  transactions.save();

                                  transactionWalletUser.user_id = req.body.user_id;
                                  transactionWalletUser.amount = parseInt(req.body.amount);
                                  transactionWalletUser.sens = 'entrant';
                                  transactionWalletUser.reference_transaction = reference_transaction;
                                  transactionWalletUser.device_id = device_id;
                                  transactionWalletUser.device_name = name_device;
                                  transactionWalletUser.methodpayment_id = methodpayment_id;
                                  transactionWalletUser.methodpayment_name = methodpayment_name;
                                  transactionWalletUser.save();

                                  WalletUser.findOne({user_id: req.body.user_id}, function(err, data_new){

                                      res.status(200).json({
                                          status: 'success',
                                          message: 'New Wallet User created!',
                                          data: data_new
                                      });

                                  });


                              });
                          }


                      });

                  }
              });


          });





    }

    







};



exports.mettre_a_zero = function (req, res) {

    var name_device = "ZAR";
    var value = 4000;

    WalletUser.findByIdAndUpdate(req.body.user_id,
        {
            amount: value
        },
        function(err, data){
            res.status(200).json({
                status: 'success',
                message: 'Wallet User updated!',
                data: data
            });

        });

};


exports.delete_device_wallet = function (req, res) {

    var name_device = "ZAR";
    var value = 4000;

    WalletUser.remove({_id:req.params.wallet_id}, function(err, data){
            res.status(200).json({
                status: 'success',
                message: 'Device Deleted !',
                data: data
            });

        });

};
