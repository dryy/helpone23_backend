/**
 * Created by christophe on 18/11/23.
 */

// Import souscategory model
Souscategory = require('./../Models/souscategoryModel');
CategoryService = require('./../Models/categoryServiceModel');

// Handle index actions     permet de get toutes les sous-catégories
exports.index = function (req, res) {
    Souscategory.get(function (err, souscategory) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Souscategories retrieved successfully",
            data: souscategory
        });
    });
};



// Handle create souscategory actions    permet de save(post) une sous-catégorie d'une catégorie
exports.new = function (req, res) {
    var souscategory = new Souscategory();
    souscategory.souscategoryName = req.body.souscategoryName;
    souscategory.logo = req.body.logo;
    souscategory.description = req.body.description;
    souscategory.categoryServiceId = req.body.categoryServiceId;
    souscategory.update_date = new Date(Date.now()).toISOString();
    //service.categoryName = null;

    //on se rassure si categoryService existe
    CategoryService.findById(req.body.categoryServiceId, function (err, categoryservice) {
        if (err)
        //res.send(err);
        /*res.status(500).json({
         status: 'error',
         message: 'Internal Problem Server'
         });*/
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });

        if(categoryservice == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });
        }

        //ici on est sûr que la catégorie service Id existe
        //alors on mets dans le modèle service categoryName
        //var service = new Service();
        /*service.serviceName = req.body.serviceName;
         service.description = req.body.description;
         service.categoryServiceId = req.body.categoryServiceId;*/
        souscategory.categoryName = categoryservice.categoryName;
        souscategory.description_category = categoryservice.description_category;
        souscategory.logo_category = categoryservice.logo_category;

        //res.send(service);

        //on se rassure si souscategoryName n'existe pas encore pour la categoryService
        Souscategory.findOne({souscategoryName:req.body.souscategoryName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
            if(err)
            //res.send(err);
                res.status(500).json({
                    status: 'error',
                    message: 'Internal Problem Server'
                });

            if(data == null) {   //dans ce cas le souscategoryName n'existe pas encore pour la categoryService
                // save the souscategoryName and check for errors
                souscategory.save(function (err) {
                    if (err)
                    //res.send(err);
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    res.status(200).json({
                        status: 'success',
                        message: 'New souscategory created!',
                        data: souscategory
                    });
                });
            }
            else{
                res.status(401).json({
                    status: 'error',
                    message: 'souscategoryName already exist for this categoryService'
                });
            }
        });
    });
};



//permet de get les sous-categories d'une catégorie
exports.getSouscategoryCategory = function (req, res) {

    Souscategory.find({categoryServiceId:req.body.categoryServiceId}, function(err, data){
        if(err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Souscategories retrieved successfully',
            data: data
        });

    });
};



// Handle view souscategory info   permet de GET une sous-catégorie à partir de son Id
exports.view = function (req, res) {
    Souscategory.findById(req.params.souscategory_id, function (err, souscategory) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(souscategory == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId souscategory not exist'
            });
        }
        else{
            res.json({
                status: 'success',
                message: 'Sous category details loading..',
                data: souscategory
            });
        }
    });
};




// Handle update souscategory info  permet d'update une souscategorie d'une catégorie
exports.update = function (req, res) {
    var souscategoryName = null;

    //on se rassure si categoryService existe
    CategoryService.findById(req.body.categoryServiceId, function (err, categoryservice) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(categoryservice == null) {
            res.status(401).json({
                status: 'error',
                message: 'ObjectId categoryservice not exist'
            });
        }
        else{
            categoryName = categoryservice.categoryName;
            description_category = categoryservice.description_category;
            logo_category = categoryservice.logo_category;
        }
    });

    //on se rassure si souscategoryName n'existe pas encore pour la categoryService
    Souscategory.findOne({souscategoryName:req.body.souscategoryName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
        if(err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(data == null) {   //dans ce cas le souscategoryName n'existe pas encore pour la categoryService

            //on fait alors un update de souscategoryName
            Souscategory.findByIdAndUpdate(req.params.souscategory_id,
                {
                    souscategoryName: req.body.souscategoryName,
                    categoryServiceId: req.body.categoryServiceId,
                    logo: req.body.logo,
                    description: req.body.description,
                    categoryName: categoryName,
                    description_category: description_category,
                    logo_category: logo_category,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, souscategory) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if(souscategory == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId souscategory not exist'
                        });
                    }
                    else{
                        //je capture en BD les nouvelles données enregistrées
                        Souscategory.findOne({souscategoryName:req.body.souscategoryName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
                            res.status(200).json({
                                status: 'success',
                                message: 'Souscategory Info updated',
                                data: data
                            });
                        });
                    }
                });
        }
        else if(data._id == req.params.souscategory_id){ //alors c'est le même et on fait l'update

            //on fait alors un update de souscategoryName
            Souscategory.findByIdAndUpdate(req.params.souscategory_id,
                {
                    souscategoryName: req.body.souscategoryName,
                    categoryServiceId: req.body.categoryServiceId,
                    logo: req.body.logo,
                    description: req.body.description,
                    categoryName: categoryName,
                    description_category: description_category,
                    logo_category: logo_category,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, souscategory) {
                    if (err)
                        res.status(500).json({
                            status: 'error',
                            message: 'Internal Problem Server'
                        });

                    if(souscategory == null){
                        res.status(401).json({
                            status: 'error',
                            message: 'ObjectId souscategory not exist'
                        });
                    }
                    else{
                        //je capture en BD les nouvelles données enregistrées
                        Souscategory.findOne({souscategoryName:req.body.souscategoryName, categoryServiceId:req.body.categoryServiceId}, function(err, data){
                            res.status(200).json({
                                status: 'success',
                                message: 'Souscategory Info updated',
                                data: data
                            });
                        });
                    }
                });

        }
        else{
            res.status(401).json({
                status: 'error',
                message: 'souscategoryName already exist for this categoryService'
            });
        }
    });
};




// Handle delete souscategory  permet de supprimer une sous-categorie
exports.delete = function (req, res) {
    Souscategory.remove({
        _id: req.params.souscategory_id
    }, function (err, souscategory) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(souscategory == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId Souscategory not exist'
            });
        }
        else {
            res.status(200).json({
                status: "success",
                message: 'Souscategory deleted'
            });
        }
    });
};



//permet de get la catégorie d'une sous-catégorie
exports.getCategorySousCategory = function (req, res) {
    Souscategory.findById(req.params.souscategory_id, function (err, souscategory) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(souscategory == null) {
            res.status(401).json({
                status: 'error',
                message: 'ObjectId souscategory not exist'
            });
        }
        else{
            res.status(200).json({
                status: 'success',
                message: 'Sous category details loading..',
                data: souscategory
            });
        }
    });
};


//permet d'ajouter les champs description_category et logo_category dans souscategoryModel
exports.add_field_in_souscategoryModel = function (req, res) {
        Souscategory.findById(req.params.souscategory_id, function (err, souscategory) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(souscategory == null) {
            res.status(401).json({
                status: 'error',
                message: 'ObjectId souscategory not exist'
            });
        }
        else{
                //je récupère les infos de la categoryServcie
                CategoryService.findById(souscategory.categoryServiceId, function(err, category){
                    var description_category = category.description;
                    var logo_category = category.logo;

                    //maintenant j'update ça dans la souscategory
                    Souscategory.findByIdAndUpdate(req.params.souscategory_id,
                        {
                            description_category: description_category,
                            logo_category: logo_category
                        },
                        function(err, new_souscategory){
                            res.status(200).json({
                                status: 'success',
                                message: 'Sous category details loading..',
                                data: new_souscategory
                            });
                    });

                });
        }
    });
};
