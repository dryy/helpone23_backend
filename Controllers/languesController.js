/**
 * Created by christophe on 18/12/12.
 */

// Import langues model
Langue = require('./../Models/languesModel');
Language = require('./../Models/languagesModel');


// Handle index actions     permet de get toutes les langues
exports.index = function (req, res) {
    Langue.get(function (err, langues) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Langues retrieved successfully",
            data: langues
        });
    });
};



// Handle create langues actions    permet de save(post) une langue
exports.new = function (req, res) {
    var langue = new Langue();
    langue.nom_langue = req.body.nom_langue;
    langue.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le nom de la langue n'existe pas encore parmis les langues
    Langue.findOne({nom_langue:req.body.nom_langue}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null) { //dans ce cas nom_langue existe déjà
            res.status(401).json({
                status: 'error',
                message: 'This language name already exist'
                });
        }
        
        // save the langue and check for errors
        langue.save(function (err) {
            if (err) {
                res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
                });  
            }
                
                
            res.status(200).json({
                status: 'success',
                message: 'New language created!',
                data: langue
            });
        });
    });
};



// Handle view language info   permet de GET une langue à partir de son Id
exports.view = function (req, res) {
    Langue.findById(req.params.langue_id, function (err, langue) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Language ObjectId not exist'
            });

        if (langue == null){
            res.status(401).json({
                status: 'error',
                message: 'Language ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Language details loading..',
            data: langue
        });
    });
};



// Handle update language info  permet d'update une langue
exports.update = function (req, res) {

    //je me rassure si ce nom de langue n'existe pas dans le document Langues
    Langue.findOne({nom_langue: req.body.nom_langue}, function(err, data1){   

        if (data1 != null){
                                
            if(data1.nom_langue == req.body.nom_langue){
                //on ne fait rien et on retourne juste les données
                //je capture en BD les nouvelles données enregistrées
                Langue.findOne({_id: req.params.langue_id}, function(err, data_new){
                    res.status(200).json({
                        status: 'success',
                        message: 'Language Info updated',
                        data: data_new
                        });
                    });
            }
            else{           
                    res.status(401).json({
                        status: 'error',
                        message: 'Language name already exist'
                        }); 
                    }    
            }
        else{

            Langue.findByIdAndUpdate(req.params.langue_id,
                {
                    nom_langue: req.body.nom_langue,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, langue) {
                    if (err)
                        res.status(401).json({
                            status: 'error',
                            message: 'Language ObjectId not exist'
                        });


                    //je dois me rassurer que nom_langue n'existe pas déjà
                    if(langue != null){
                        if(langue.nom_langue == req.body.nom_langue){
                            //on ne fait rien et on retourne juste les données
                            //je capture en BD les nouvelles données enregistrées
                            Langue.findOne({_id: req.params.langue_id}, function(err, data_new){
                                res.status(200).json({
                                    status: 'success',
                                    message: 'Language Info updated',
                                    data: data_new
                                });
                            });
                        }
                        else{

                            //je me rassure si ce nom de langue n'existe pas dans le document Langues
                            Langue.findOne({nom_langue: req.body.nom_langue}, function(err, data1){

                                /*if (data1 != null){
                                 res.status(401).json({
                                 status: 'error',
                                 message: 'Language name already exist'
                                 });
                                 }*/
                                //else{

                                //ici je update le nom de la langue ainsi que dans tous les choix de langue fait par les users
                                var myquery = { _id: req.params.langue_id };
                                var newvalues = {$set: {nom_langue: req.body.nom_langue} };

                                Langue.updateMany(myquery, newvalues, function(err, res) {
                                    //if (err){}
                                });

                                //j'update aussi chez tous les users ayant fait ce choix de langue
                                var myquery2 = {langue: req.body.nom_langue};
                                var newvalues2 = {$set: {langue: req.body.nom_langue}};

                                Language.updateMany(myquery2, newvalues2, function(err, res){});

                                //je capture en BD les nouvelles données enregistrées
                                Langue.findOne({_id: req.params.langue_id}, function(err, data){
                                    res.status(200).json({
                                        status: 'success',
                                        message: 'Language Info updated',
                                        data: data
                                    });
                                });
                                //}

                            });
                        }
                    }
                });

        }
            
    });





};



// Handle delete language   permet de supprimer une langue
exports.delete = function (req, res) {
    Langue.remove({
        _id: req.params.langue_id
    }, function (err, langue) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(langue == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId Language not exist'
            });
        }
        else{
            
            //je dois me rassurer que cette langue n'est pas encore utilisée dans le choix d'un user
            Language.findOne({langue: langue.nom_langue}, function(err, language){
                    if (err){
                       res.status(401).json({
                            status: 'error',
                            message: 'Language is already used for an user. Impossible to delete !!!'
                        }); 
                    }

                    if (language != null){ //ça veut dire que la langue a déjà été utilisée dans le choix d'un user
                        res.status(401).json({
                            status: 'error',
                            message: 'Language is already used for an user. Impossible to delete !!!'
                        });
                    }
                    else{
                            res.status(200).json({
                            status: "success",
                            message: 'Language deleted'
                             });
                    } 
            });

        }
    });
};


