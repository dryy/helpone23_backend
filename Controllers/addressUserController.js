/**
 * Created by christophe on 19/01/05.
 */

// Import addressUser model
AddressUser = require('./../Models/addressUserModel');
ChoixDeliverAndBillHelpForAService = require('./../Models/choixDeliverAndBillHelpForAServiceModel');


// Handle view address user info    permet de GET les différentes adresses du User
exports.view = function (req, res) {

    AddressUser.find({user_id: req.params.user_id}, function(err, address){
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        res.status(200).json({
            status: 'success',
            message: 'Address User details loading..',
            data: address
        });
    });

};


// Handle view a particular address user info    permet de GET l'info d'une adresse particulière
exports.view_address = function (req, res) {

    AddressUser.findById(req.params.addressUser_id, function(err, address){
        //if (err)
        //    res.status(500).json({
        //        status: 'error',
        //        message: 'Internal Problem Server'
        //    });

        if(address == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId Address not exist'
            });
        }
        else{
            res.status(200).json({
                status: 'success',
                message: 'Address details loading..',
                data: address
            });
        }

    });

};


// Handle create address user actions    permet d'enregistrer une adresse pour un user
exports.new = function (req, res) {
    var addressuser = new AddressUser();
    addressuser.user_id = req.body.user_id;
    addressuser.address_title = req.body.address_title;
    addressuser.address = req.body.address;
    addressuser.zip_code_postal = req.body.zip_code_postal;
    addressuser.telephone = req.body.telephone;
    addressuser.city = req.body.city;
    addressuser.email = req.body.email;
    addressuser.update_date = new Date(Date.now()).toISOString();


    //on se rassure si address_title est unique pour le user en question (user_id)
    AddressUser.findOne({user_id: req.body.user_id, address_title: req.body.address_title}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null){
            res.status(401).json({
                status: 'error',
                message: 'This address title already exist for this user. Please change the address title and try again !!!'
            });
        }
        else{

            // save address user and check for errors
            addressuser.save(function (err) {
                if (err) {
                    res.status(500).json({
                        status: 'error',
                        message: 'Internal Problem Server'
                    });
                }

                AddressUser.findOne({user_id: req.body.user_id, address_title: req.body.address_title}, function(err, data_new){
                    res.status(200).json({
                        status: 'success',
                        message: 'New Address User created!',
                        data: data_new
                    });
                });
            });

        }
    });
};


// Handle update Address User info  permet d'update l'adresse d'un user
exports.update = function (req, res) {
    var user_id = req.body.user_id;
    var address_title = req.body.address_title;
    var address = req.body.address;
    var zip_code_postal = req.body.zip_code_postal;
    var telephone = req.body.telephone;
    var city = req.body.city;
    var email = req.body.email;
    var addressUser_id = req.params.addressUser_id;

    //on vérifie si les infos entrées n'existent pas déjà ou sont les mêmes pour addressUser_id

    AddressUser.findOne({user_id:user_id, address_title:address_title}, function(err, data_new){
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (data_new != null){
            //je me rassure si address_title c'est pour addressUser_id
            if(data_new._id != addressUser_id){
                //alors address_title existe déjà
                res.status(401).json({
                    status: 'error',
                    message: 'This address title is already used by this user. Please change the address title and try again !!!'
                });
            }
            else{
                //alors c'est la même adresse et on fait l'update
                AddressUser.findByIdAndUpdate(addressUser_id,
                    {
                        address_title: address_title,
                        address: address,
                        zip_code_postal: zip_code_postal,
                        telephone: telephone,
                        city: city,
                        email: email,
                        update_date: new Date(Date.now()).toISOString()
                    },
                    function (err, data) {
                        if (err)
                            res.status(401).json({
                                status: 'error',
                                message: 'Address User ObjectId not exist'
                            });

                        //je capture en BD les nouvelles données enregistrées
                        AddressUser.findOne({_id: addressUser_id}, function(err, data_address){
                            res.status(200).json({
                                status: 'success',
                                message: 'Address User Info updated',
                                data: data_address
                            });
                        });
                    });
            }
        }
        else{
            AddressUser.findByIdAndUpdate(addressUser_id,
                {
                    address_title: address_title,
                    address: address,
                    zip_code_postal: zip_code_postal,
                    telephone: telephone,
                    city: city,
                    email: email,
                    update_date: new Date(Date.now()).toISOString()
                },
                function (err, data) {
                    if (err)
                        res.status(401).json({
                            status: 'error',
                            message: 'Address User ObjectId not exist'
                        });

                    //je capture en BD les nouvelles données enregistrées
                    AddressUser.findOne({_id: addressUser_id}, function(err, data_address){
                        res.status(200).json({
                            status: 'success',
                            message: 'Address User Info updated',
                            data: data_address
                        });
                    });
                });
        }
    });


};



// Handle delete address user  permet de supprimer l'adresse d'un user
exports.delete = function (req, res) {
    //je vérifie si addressUser_id est utilisée pour un service en cours
    ChoixDeliverAndBillHelpForAService.findOne({addressUser_id: req.params.addressUser_id}, function(err, data){
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if (data != null){
            //alors l'adresse est déjà utilisée dans un service
            res.status(500).json({
                status: 'error',
                message: 'This address is already used in a service. It is not possible to delete this address'
            });
        }

    });

    AddressUser.findOne({_id: req.params.addressUser_id}, function(err, result){
        if (result == null){
            res.status(401).json({
                status: 'error',
                message: 'Address User not exist'
            });
        }
    });

    AddressUser.remove({
        _id: req.params.addressUser_id
    }, function (err, user) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Address User not exist'
            });

        res.status(200).json({
            status: "success",
            message: 'Address User deleted'
        });
    });
};
