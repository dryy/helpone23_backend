/**
 * Created by christophe on 18/11/20.
 */

// Import logo model
Logo = require('./../Models/logoModel');


// Handle index actions     permet de get tous les logos
exports.index = function (req, res) {
    Logo.get(function (err, logos) {
        if (err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }
        res.status(200).json({
            status: "success",
            message: "Logos retrieved successfully",
            data: logos
        });
    });
};



// Handle create role actions    permet de save(post) un role
exports.new = function (req, res) {
    var role = new Role();
    role.title = req.body.title;
    role.slug = req.body.slug;
    role.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le titre ou le slug du rôle n'existent pas encore dans rôle
    Role.findOne({title:req.body.title/*, slug:req.body.slug*/}, function(err, data){
        if(err) {
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        if(data != null) { //dans ce cas title existe déjà
            res.status(401).json({
                status: 'error',
                message: 'title already exist'
                });
        }
        else{
            //je me rassure si slug n'existe pas encore
            Role.findOne({slug: req.body.slug}, function(err, data_slug){
                if(data_slug != null){
                    res.status(401).json({
                        status: 'error',
                        message: 'slug already exist'
                    });
                }
                else{
                        // save the role and check for errors
                        role.save(function (err) {
                            if (err)
                                res.status(500).json({
                                    status: 'error',
                                    message: 'Internal Problem Server'
                                });
                            res.status(200).json({
                                status: 'success',
                                message: 'New role created!',
                                data: role
                            });
                        });
                }
            });
        }
        
    });
};



// Handle view role info   permet de GET un role à partir de son Id
exports.view = function (req, res) {
    Role.findById(req.params.role_id, function (err, role) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Role ObjectId not exist'
            });
        res.status(200).json({
            status: 'success',
            message: 'Role details loading..',
            data: role
        });
    });
};



// Handle update role info  permet d'update un role
exports.update = function (req, res) {
    Role.findByIdAndUpdate(req.params.role_id,
        {
            title: req.body.title,
            slug: req.body.slug,
            update_date: new Date(Date.now()).toISOString()
        },
        function (err, role) {
            if (err)
                res.status(401).json({
                    status: 'error',
                    message: 'Role ObjectId not exist'
                });


            //je dois me rassurer que title et slug n'existent pas déjà
            if(role != null){
                if(role.title == req.body.title && role.slug == req.body.slug){
                    //on ne fait rien et on retourne juste les données
                    //je capture en BD les nouvelles données enregistrées
                    Role.findOne({_id: req.params.role_id}, function(err, data_new){
                        res.status(200).json({
                            status: 'success',
                            message: 'Role Info updated',
                            data: data_new
                        });
                    });
                }
            }

            //je dois me rassurer que title et slug n'existent pas déjà
            Role.findOne({title: req.body.title/*, slug: req.body.slug*/}, function(err, data_role){
                if (data_role != null){
                    //je me rassure si c'est bien ce qui était là
                    if(data_role._id == req.params.role_id){
                        //avant de faire l'update je dois me rassurer que slug n'existe pas encore
                        //je fais de même avec slug
                        Role.findOne({slug: req.body.slug},function(err, data_slug){
                            if(data_slug != null){
                                //je me rassurre bien que c'est ce qui était là
                                if(data_slug._id == req.params.role_id){
                                    //je fais tout simplement un update
                                    var myquery = { role_id: req.params.role_id };
                                    var newvalues = {$set: {title_role: req.body.title, slug_role: req.body.slug} };
                                            
                                    User.updateMany(myquery, newvalues, function(err, res) {
                                                //if (err){}
                                    });

                                    //je capture en BD les nouvelles données enregistrées
                                    Role.findOne({_id: req.params.role_id}, function(err, data){
                                        res.status(200).json({
                                            status: 'success',
                                            message: 'Role Info updated',
                                            data: data
                                        });
                                    }); 
                                }
                                else{
                                        //je dis slug existe déjà
                                        res.status(401).json({
                                         status: 'error',
                                         message: 'slug already exist'
                                         });      
                                }
                            }
                        });    

                    }
                    else{
                        //je dis title existe déjà
                        res.status(401).json({
                         status: 'error',
                         message: 'title already exist'
                         });  
                    }
                }
                else{
                        //je dois aussi faire les updates chez tous les users ayant ce même rôle
                        var myquery = { role_id: req.params.role_id };
                        var newvalues = {$set: {title_role: req.body.title, slug_role: req.body.slug} };
                                
                        User.updateMany(myquery, newvalues, function(err, res) {
                                    //if (err){}
                        });

                        //je capture en BD les nouvelles données enregistrées
                        Role.findOne({_id: req.params.role_id}, function(err, data){
                            res.status(200).json({
                                status: 'success',
                                message: 'Role Info updated',
                                data: data
                            });
                        }); 
                }     
            });    
        });
};



// Handle delete role  permet de supprimer un role
exports.delete = function (req, res) {
    Role.remove({
        _id: req.params.role_id
    }, function (err, role) {
        if (err)
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });

        if(role == null){
            res.status(401).json({
                status: 'error',
                message: 'ObjectId Role not exist'
            });
        }
        else{
            
            //je dois me rassurer que ce rôle n'est pas encore utilisé pour un user
            User.findOne({role_id: req.params.role_id}, function(err, user){
                    if (err){
                       res.status(401).json({
                            status: 'error',
                            message: 'Role is already used for an user. Impossible to delete !!!'
                        }); 
                    }

                    if (user != null){ //ça veut dire que le rôle a déjà été utilisé pour un user
                        res.status(401).json({
                            status: 'error',
                            message: 'Role is already used for an user. Impossible to delete !!!'
                        });
                    }
                    else{
                            res.status(200).json({
                            status: "success",
                            message: 'Role Service deleted'
                             });
                    } 
            });

        }
    });
};



//permet de changer le rôle d'un user
exports.update_role_user = function (req, res) {

    var user = new User();

    Role.findById(req.body.role_id, function (err, role) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'Role ObjectId not exist'
            });
        
        if (role != null){

            User.findByIdAndUpdate(req.params.user_id,
            {
                role_id: req.body.role_id,
                title_role: role.title,
                slug_role: role.slug,
                update_date: new Date(Date.now()).toISOString()
            },
            function (err, user) {
                if (err)
                    res.status(401).json({
                        status: 'error',
                        message: 'User not exist'
                    });

                //je capture en BD ls nouvelles données enregistrées
                User.findById(req.params.user_id, function(err, user){
                    res.status(200).json({
                        status: 'success',
                        message: 'Role Info updated',
                        data: user
                    });
                });
            });


        }
        else{
                res.status(401).json({
                    status: 'error',
                    message: 'Role ObjectId not exist'
                });
        }

    });


    
};



//permet d'update le champs role_id dans le Modèle user
exports.update_role_id = function (req, res) {
    
    var myquery = { title_role: req.body.title };

    Role.findOne({title: req.body.title},function(err, data){
        var newvalues = {$set: {role_id: data._id} };

        User.updateMany(myquery, newvalues, function(err, data_new) {
                        if (err){
                            res.status(401).json({error: err});
                        }

                        res.status(401).json({result: data_new});
        });

    });
};
