/**
 * Created by christophe on 19/01/08.
 */

// Import image_service model

Image_service = require('./../Models/image_serviceModel');

// Handle view images services info   permet de GET toutes les images d'un serviceAskForHelp_id
exports.view = function (req, res) {
    var mysort = { create_date: -1 };   //va permettre de renvoyer les services du plus récent au plus ancien


    Image_service.find({serviceAskForHelp_id:req.params.serviceaskforhelp_id},function(err, image){
        if (err){
            res.status(500).json({
                status: 'error',
                message: 'Internal Problem Server'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'Images Services details loading..',
            data: image
        });
    }).sort(mysort);

    //Image_service.get(function(err, image){
    //    res.status(200).json({
    //        status: 'success',
    //        message: 'Images Service details loading..',
    //        data: image
    //    });
    //});

};



