/**
 * Created by christophe on 19/03/01.
 */

// Import ScheduleDates model
ScheduleDates = require('./../Models/ScheduleDatesModel');
Device = require('./../Models/deviceModel');
WalletUser = require('./../Models/walletUserModel');
Transactions = require('./../Models/transactionsModel');
TransactionWalletUser = require('./../Models/transactionWalletUserModel');
HelpNotes = require('./../Models/HelpNotesModel');
User = require('./../Models/userModel');
ServiceAskForHelp = require('./../Models/serviceAskForHelpModel');

var nodemailer = require('nodemailer');     //pour l'envoi des mails




// Handle create scheduledates actions    permet de save(post) une schedule date par rapport à un service (interface Bid Awarded  - Schedules dates - Add.png)
exports.new = function (req, res) {
    var scheduledates = new ScheduleDates();
    scheduledates.serviceAskForHelpId = req.body.serviceAskForHelpId;
    scheduledates.user_id = req.body.user_id;
    scheduledates.date_schedule = req.body.date_schedule;
    scheduledates.time_schedule = req.body.time_schedule;
    scheduledates.status_schedule = "waiting";  //en attente
    scheduledates.update_date = new Date(Date.now()).toISOString();


    //on se rassure si le service existe
    ServiceAskForHelp.findById(req.body.serviceAskForHelpId, function(err, data){

        if(data == null){
            res.status(401).json({
                status: 'error',
                message: 'Service Ask For help not exist'
            });
        }
        else{
            var status_service = data.status;

            scheduledates.status_service = status_service;

            //je récupère le user
            User.findById(req.body.user_id, function(err, user){
                if(user == null){
                    res.status(401).json({
                        status: 'error',
                        message: 'User not exist'
                    });
                }
                else
                {
                    var username = user.username;
                    var firstname = user.firstname;

                    scheduledates.username = username;
                    scheduledates.firstname = firstname;
                    
                    //je dois notifier par email le travailleur
                    if(data.user_id == req.body.user_id){
                        //alors c'est l'employeur qui écrit le schedule date et je dois notifier le worker par mail
                        User.findById(data.user_id_celui_choisit, function(err, new_user){
                            if(new_user != null){
                                var email_travailleur = new_user.email;
                                
                                /****** Envoyer le code par mail *************/

                                var transporter = nodemailer.createTransport({
                                  service: 'gmail',
                                  auth: {
                                    user: 'help123backend@gmail.com',
                                    pass: '123help@2018'
                                  }
                                });
                        
                                var mailOptions = {
                                  from: 'help123backend@gmail.com',
                                  to: email_travailleur,
                                  subject: 'Schedule Date Notification',
                                  text: 'Check your account to see the new schedule date from your employer. Service name : ' + data.nom_service
                                };
                        
                                transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                    console.log(error);
                                  } else {
                                    console.log('Email sent: ' + info.response);
                                  }
                                });
                        
                        
                                /********** fin envoi mail  **************************/
                                
                                
                            }
                        });
 
                    }
                    
                    
                    //je dois notifier par email l'employeur
                    if(data.user_id_celui_choisit == req.body.user_id){
                        //alors c'est le travailleur qui écrit le schedule date et je dois notifier l'employeur par mail
                        User.findById(data.user_id, function(err, new_user){
                            if(new_user != null){
                                var email_employeur = new_user.email;
                                
                                /****** Envoyer le code par mail *************/

                                var transporter = nodemailer.createTransport({
                                  service: 'gmail',
                                  auth: {
                                    user: 'help123backend@gmail.com',
                                    pass: '123help@2018'
                                  }
                                });
                        
                                var mailOptions = {
                                  from: 'help123backend@gmail.com',
                                  to: email_employeur,
                                  subject: 'Schedule Date Notification',
                                  text: 'Check your account to see the new schedule date from your worker. Service name : ' + data.nom_service
                                };
                        
                                transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                    console.log(error);
                                  } else {
                                    console.log('Email sent: ' + info.response);
                                  }
                                });
                        
                        
                                /********** fin envoi mail  **************************/
                                
                                
                            }
                        });
 
                    }


                    scheduledates.save(function(err, scheduledates){
                        res.status(200).json({
                            status: 'success',
                            message: 'New ScheduleDate created!',
                            data: scheduledates
                        });

                    });
                }
            });
        }
    });
};



// Handle view ScheduleDate info   permet de GET un ScheduleDate à partir de son Id
exports.view = function (req, res) {
    ScheduleDates.findById(req.params.scheduledate_id, function (err, scheduledate) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });

        if (scheduledate == null){
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'ScheduleDate details loading..',
            data: scheduledate
        });
    });
};


// Handle view All ScheduleDates services info   permet de GET tous les ScheduleDates d'un Service Ask For Help
exports.view_schedules = function (req, res) {
    ScheduleDates.find({serviceAskForHelpId:req.params.serviceAskForHelpId}, function (err, scheduledate) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });

        if (scheduledate == null){
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });
        }

        res.status(200).json({
            status: 'success',
            message: 'ScheduleDates details loading..',
            data: scheduledate
        });
    });
};


// permet d'approuver (approved) ou de revoquer (revoked) un schedule date
exports.update_status_schedule = function (req, res) {
    var approved = parseInt(req.body.approved);   //0 pour non et 1 pour oui
    var revoked = parseInt(req.body.revoked);     //0 pour non et 1 pour oui

    //je me rassure que le ScheduleDate existe
    ScheduleDates.findById(req.params.scheduledate_id, function (err, scheduledate) {
        if (err)
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });

        if (scheduledate == null){
            res.status(401).json({
                status: 'error',
                message: 'ScheduleDate ObjectId not exist'
            });
        }
        else{
            var status_schedule;
            if(approved == 1){
                status_schedule = "approved";
            }

            if(revoked == 1){
                status_schedule = "revoked";
            }

            ScheduleDates.findByIdAndUpdate(req.params.scheduledate_id,
                {
                    status_schedule: status_schedule,
                    update_date: new Date(Date.now()).toISOString()
                },
            function(err, scheduledate_update){
                //je notifie par mail le worker
                ServiceAskForHelp.findById(scheduledate.serviceAskForHelpId, function(err, service){
                    if(service != null){
                        User.findById(service.user_id_celui_choisit, function (err, new_user){
                            var email_travailleur = new_user.email;
                            
                            
                            /****** Envoyer le code par mail *************/

                                var transporter = nodemailer.createTransport({
                                  service: 'gmail',
                                  auth: {
                                    user: 'help123backend@gmail.com',
                                    pass: '123help@2018'
                                  }
                                });
                        
                                var mailOptions = {
                                  from: 'help123backend@gmail.com',
                                  to: email_travailleur,
                                  subject: 'Schedule Date Notification',
                                  text: 'Check your account to see if the schedule date is approved or revoked from your employer. Service name : ' + data.nom_service
                                };
                        
                                transporter.sendMail(mailOptions, function(error, info){
                                  if (error) {
                                    console.log(error);
                                  } else {
                                    console.log('Email sent: ' + info.response);
                                  }
                                });
                        
                        
                                /********** fin envoi mail  **************************/
                            
                            
                        });
                    }
                    
                });
                
            });

            //je récupère les nouvelles données
            ScheduleDates.findById(req.params.scheduledate_id, function(err, new_data){
                res.status(200).json({
                    status: 'success',
                    message: 'ScheduleDate updated !',
                    data: new_data
                });
            });
        }


    });
};



