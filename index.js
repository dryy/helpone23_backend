/**
 * Created by christophe on 18/09/24.
 */


// FileName: index.js
// Import express
//let express = require('express')
//
//// Import routes
//let apiRoutes = require("./api-routes")
//
//// Initialize the app
//let app = express();
//// Setup server port
//var port = process.env.PORT || 8080;
//
//
//// Use Api routes in the App
//app.use('/api', apiRoutes);
//
//// Send message for default URL
//app.get('/', (req, res) => res.send('Hello World with Express'));
//// Launch app to listen to specified port
//app.listen(port, function () {
//    console.log("Running RestHub on port " + port);
//});

// const escomplex = require('escomplex');
// const result = escomplex.analyse(source, options);
// Import express
let express = require('express');

//var express = require('express');
var cors = require('cors');

// Initialize the app
let app = express();

//var app = express();

//app.user(bodyParser.json());
// after the code that uses bodyParser and other cool stuff
var originsWhitelist = [
  'http://localhost:4200',      //this is my front-end url for development
   'http://api.visavisbet.com'
];
var corsOptions = {
  origin: function(origin, callback){
        var isWhitelisted = originsWhitelist.indexOf(origin) !== -1;
        callback(null, isWhitelisted);
  },
  credentials:true
}
//here is the magic
app.use(cors(corsOptions));  

app.use(function(req, res, next) {
       res.header("Access-Control-Allow-Origin", "*");
       res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
       res.header('Access-Control-Allow-Methods', 'PUT, POST, GET, DELETE, OPTIONS, PATCH');
          next();
    });



// Import express
//let express = require('express');

// Import Body parser
let bodyParser = require('body-parser');  //librairie qui permet de parser une chaîne en JSON

// Import Mongoose
let mongoose = require('mongoose');

// Initialize the app
//let app = express();

// Import routes
let apiRoutes = require("./api-routes")

//Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));    //ceci permet de gérer les tailles des json en entrée très grand
app.use(bodyParser.json({limit: '50mb'}));                           //ceci permet de gérer les tailles des json en entrée très grand

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://localhost/help123');
//mongoose.connect('mongodb://Help123:helpbit1234@mongodb@cluster0help123test-a6sj1.mongodb.net/test?retryWrites=true');
//mongoose.connect('mongodb://Help123:helpbit1234@cluster0help123test-shard-00-00-a6sj1.mongodb.net:27017,cluster0help123test-shard-00-01-a6sj1.mongodb.net:27017,cluster0help123test-shard-00-02-a6sj1.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0help123test-shard-0&authSource=admin&retryWrites=true');
var db = mongoose.connection;

// Setup server port
var port = process.env.PORT || 8080;

// Send message for default URL
app.get('/', (req, res) => res.send('Hello World with Express'));

// Use Api routes in the App
app.use('/api', apiRoutes)

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running Help123 on port " + port);
});


