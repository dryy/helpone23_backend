/**
 * Created by christophe on 18/09/24.
 */

// userModel.js

var mongoose = require('mongoose');


// Setup schema
var userSchema = mongoose.Schema({
    username:{type: String,required: true},
    pseudo: String,
    firstname: String,
    surname: String,
    email:{type: String,required: true,unique: true,index: true},
    personal_description: String,
    gender:{type:String/*,required: true*/} ,
    phone: {type:String/*,required: true*/},
    password: {type:String,required:true},
    experience: String,     //Beginner, Intermediate, Advanced
    CV: String,
    confirmed: Boolean,
    activation_code: Number,
    Confirmation_code: String,
    suspended: Boolean, // is true if the user is suspended by the admin and false if not
    //CV:{data: String,contentType: String},
    //criminal_record:{data: String,contentType: String},
    criminal_record: String,
    number_connection_failed: Number,
    date_connection_failed: String,
    pwdFailed: Boolean,
    /*location:{
        type_location: String,   //type = point
        coordinates:{longitude: Number, latitude: Number} 
    },*/
    longitude_location: Number,
    latitude_location: Number,
    location_name: String,
    /*city:{
        type_city: String,   //type = point
        coordinates:{longitude: Number, latitude: Number} 
    },*/
    longitude_city: String,
    latitude_city: String,
    country: String,
    picture: String,
    picture_cni: String, //id card picture of the user
    picture_with_cni: String, //picture showing the user with their ID Card
    //picture:{data: String,contentType: String},
    //roles:{title: String,slug: String},
    role_id: {type: String, required:true},
    title_role: String,
    slug_role: String,
    fonctionName: String,   // fonctionName = Offer helps ou Ask for helps
    //fonctions:{fonctionName: String},
    online: Number,          //online = 1 connecté ou 0 non connecté
    remember_token: String,
    number_helps_asked: Number,
    number_helps_offer: Number,
    nbre_etoiles: Number,
    /*passwordresets:{
        code: String,
        confirmation_code: Number,
        statusvalidity: Number,
        create_date: {type: Date, default: Date.now}
    },
    registrations_fees:{
        status: String,
        amount: Number,
        create_date: {type: Date, default: Date.now}
    },
    wallet: {
        balance:{type: Number,required: true},
        create_date:{type: Date,default: Date.now}
    },
    notifications:{
        title_notification: String,
        description: String,
        read_notification: Number,        //read = 1 pour lu et 0 pour non lu
        deleted: Number,     //deleted = 1 pour supprimer et 0 pour non supprimer
        type_notification: String,
        create_date:{type: Date,default: Date.now}   
    },
    advertising:{
        image_ad:{data: String,contentType: String},
        description: String,
        start_date: Date,
        end_date: Date,
        start_time: Date,
        end_time: Date,
        fees: String,
        create_date:{type: Date,default: Date.now}  
    },
    sponsors:{
        logo:{data: String,contentType: String},
        description: String,
        start_date: Date,
        end_date: Date,
        start_time: Date,
        end_time: Date,
        create_date:{type: Date,default: Date.now} 
    },
    paymentMethod:{
        typePaymentMethodName: String,
        active: Number,
        create_date:{type: Date,default: Date.now}
    },
    products:{
        categoryName: String,
        product:{
            description: String,
            picture:{data: String,contentType: String},
            unitPrice: Number,
            quantity: Number,
            location:{
                type_location: String,   //type = point
                coordinates:{longitude: Number, latitude: Number} 
            },
            deliveryTime: Number,
            typeDeliveryTime: String,       //typeDeliveryTime = seconds, minutes, hours, days, months, years
            create_date:{type: Date,default: Date.now}  
        }
    },
    userBasket:{
        categoryName: String,
        product:{
            description: String,
            picture:{data: String,contentType: String},
            unitPrice: Number,
            quantity: Number,
            location:{
                type_location: String,   //type = point
                coordinates:{longitude: Number, latitude: Number} 
            },
            deliveryTime: Number,
            typeDeliveryTime: String,       //typeDeliveryTime = seconds, minutes, hours, days, months, years
            statusInTheBasket: Number,
            create_date:{type: Date,default: Date.now}  
        }
    },
    transactions: {
        ref: String,        //reférence de la transaction
        sens: String,       //entrant ou sortant
        type_transaction: String,    // type_transaction = portemonnaie ou caisse
        amount: Number,
        create_date: {
            type: Date,
            default: Date.now
        }
    },*/
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export User model
var User = module.exports = mongoose.model('user', userSchema);
module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}

