/**
 * Created by christophe on 18/11/09.
 */

// roleModel.js

var mongoose = require('mongoose');


// Setup schema
var roleSchema = mongoose.Schema({
    title:{type: String,required: true},
    slug: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date:{type: Date}
});


// Export Role model
var Role = module.exports = mongoose.model('role', roleSchema);
module.exports.get = function (callback, limit) {
    Role.find(callback).limit(limit);
}

