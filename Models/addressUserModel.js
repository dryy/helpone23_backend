/**
 * Created by christophe on 19/01/05.
 */

// addressUserModel.js

var mongoose = require('mongoose');


// Setup schema
var addressUserSchema = mongoose.Schema({
    user_id: {type: String, required: true},
    address_title: {type: String},
    address: {type: String},
    zip_code_postal: {type: String},     // Zip/Postal Code
    telephone: {type: String},
    city: {type: String},
    email: {type: String},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export address user model
var AddressUser = module.exports = mongoose.model('addressuser', addressUserSchema);
module.exports.get = function (callback, limit) {
    AddressUser.find(callback).limit(limit);
}

