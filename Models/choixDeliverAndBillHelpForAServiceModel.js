/**
 * Created by christophe on 19/01/05.
 */

// choixDeliverAndBillHelpForAServiceModel.js

var mongoose = require('mongoose');


// Setup schema
var choixDeliverAndBillHelpForAServiceSchema = mongoose.Schema({
    user_id: {type: String},
    serviceAskForHelp_id: {type: String, required:true},
    addressUser_id: {type: String},
    address_title: {type: String},
    address: {type: String},
    zip_code_postal: {type: String},
    telephone: {type: String},
    city: {type: String},
    email: {type: String},
    deliver_help_to_this_address: {type: Number},       // 0 pour non et 1 pour oui
    bill_to_this_address: {type: Number},              // 0 pour non et 1 pour oui
    status_service: {type: String},                 // in_progress, completed, selected
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export choixDeliverAndBillHelpForAService model
var ChoixDeliverAndBillHelpForAService = module.exports = mongoose.model('choixDeliverAndBillHelpForAService', choixDeliverAndBillHelpForAServiceSchema);
module.exports.get = function (callback, limit) {
    ChoixDeliverAndBillHelpForAService.find(callback).limit(limit);
}
