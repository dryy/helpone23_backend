/**
 * Created by christophe on 19/01/05.
 */

// transactionWalletUserModel.js

var mongoose = require('mongoose');


// Setup schema
var transactionWalletUserSchema = mongoose.Schema({
    user_id: {type: String, required: true},
    amount: {type: Number, required: true},
    sens: {type: String,required:true},     // entrant ou sortant
    reference_transaction: String,
    device_id: String,
    device_name: String,
    methodpayment_id: String,
    methodpayment_name: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export transactionWalletUser model
var TransactionWalletUser = module.exports = mongoose.model('transactionWalletUser', transactionWalletUserSchema);
module.exports.get = function (callback, limit) {
    TransactionWalletUser.find(callback).limit(limit);
}

