/**
 * Created by christophe on 19/03/01.
 */

// ScheduleDatesModel.js

var mongoose = require('mongoose');


// Setup schema
var scheduledateSchema = mongoose.Schema({
    serviceAskForHelpId: String,
    user_id: String,
    username: String,
    firstname: String,
    date_schedule: Date,
    time_schedule: String,
    status_service: String,             //status_service = in_progress, completed, selected, review
    status_schedule: String,            //status_schedule = approved, revoked, waiting (en attente)
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export ScheduleDates model
var ScheduleDates = module.exports = mongoose.model('scheduledate', scheduledateSchema);
module.exports.get = function (callback, limit) {
    ScheduleDates.find(callback).limit(limit);
}
