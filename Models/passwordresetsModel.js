/**
 * Created by christophe on 18/10/22.
 */

// passwordresetsModel.js

var mongoose = require('mongoose');


// Setup schema
var passwordresetSchema = mongoose.Schema({
    user_id: String,
    code: String,
    confirmation_code: Number,
    statusvalidity: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Passwordreset model
var Passwordreset = module.exports = mongoose.model('passwordreset', passwordresetSchema);
module.exports.get = function (callback, limit) {
    Passwordreset.find(callback).limit(limit);
}
