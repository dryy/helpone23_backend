/**
 * Created by christophe on 19/01/06.
 */

// image_serviceModel.js

var mongoose = require('mongoose');


// Setup schema
var image_serviceSchema = mongoose.Schema({
    user_id: {type: String},
    picture: {type: String},
    serviceAskForHelp_id: {type: String, required: true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export image service model
var Image_service = module.exports = mongoose.model('image_service', image_serviceSchema);
module.exports.get = function (callback, limit) {
    Image_service.find(callback).limit(limit);
}

