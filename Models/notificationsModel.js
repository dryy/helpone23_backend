/**
 * Created by Landry on 02/10/2020.
 */

// notificationsModel.js

var mongoose = require('mongoose');


// Setup schema
var notificationsSchema = mongoose.Schema({
    user_id: String,
    username: String,
    serviceAskForHelp_id: String,
    content: String,
    read: Boolean,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {
        type: Date,
        default: Date.now
    }
});


// Export Notifications model
var Notifications = module.exports = mongoose.model('notifications', notificationsSchema);
module.exports.get = function (callback, limit) {
    Notifications.find(callback).limit(limit);
}
