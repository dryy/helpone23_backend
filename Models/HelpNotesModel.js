/**
 * Created by christophe on 19/03/01.
 */

// HelpNotesModel.js

var mongoose = require('mongoose');


// Setup schema
var helpnoteSchema = mongoose.Schema({
    serviceAskForHelpId: String,
    user_service_id: String,
    user_id_celui_qui_ecrit: String,
    user_id_celui_qui_recoit: String,
    username_celui_qui_ecrit: String,
    username_celui_qui_recoit: String,
    firstname_celui_qui_ecrit: String,
    subjest_note_help: String,
    note_help: String,
    status_service: String,             //status_service = in_progress, completed, bidder_selected, review
    status_help_note: String,           //status_help_note = selected, awarded, completed
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export HelpNotes model
var Helpnotes = module.exports = mongoose.model('helpnote', helpnoteSchema);
module.exports.get = function (callback, limit) {
    Helpnotes.find(callback).limit(limit);
}
