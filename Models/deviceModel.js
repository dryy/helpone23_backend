/**
 * Created by christophe on 19/02/11.
 */

// deviceModel.js

var mongoose = require('mongoose');


// Setup schema
var deviceSchema = mongoose.Schema({
    name: {type: String},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export device model
var device = module.exports = mongoose.model('device', deviceSchema);
module.exports.get = function (callback, limit) {
    device.find(callback).limit(limit);
}
