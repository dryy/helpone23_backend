/**
 * Created by christophe on 19/01/06.
 */

// argent_en_attente_pour_finition_serviceModel.js

var mongoose = require('mongoose');


// Setup schema
var argent_en_attente_pour_finition_serviceSchema = mongoose.Schema({
    user_id: {type: String, required: true},
    serviceAskForHelp_id: {type: String, required: true},
    amount: {type: Number},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export argent en attente pour finition service model
var Argent_en_attente_pour_finition_service = module.exports = mongoose.model('argent_en_attente_pour_finition_service', argent_en_attente_pour_finition_serviceSchema);
module.exports.get = function (callback, limit) {
    Argent_en_attente_pour_finition_service.find(callback).limit(limit);
}

