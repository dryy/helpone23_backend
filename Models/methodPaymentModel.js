/**
 * Created by christophe on 19/02/15.
 */

// methodPaymentModel.js

var mongoose = require('mongoose');


// Setup schema
var methodPaymentSchema = mongoose.Schema({
    name: {type: String},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export methodPayment model
var MethodPayment = module.exports = mongoose.model('methodpayment', methodPaymentSchema);
module.exports.get = function (callback, limit) {
    MethodPayment.find(callback).limit(limit);
}

