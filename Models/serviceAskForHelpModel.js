/**
 * Created by christophe on 19/01/06.
 */

// serviceAskForHelpModel.js

var mongoose = require('mongoose');


// Setup schema
var serviceAskForHelpSchema = mongoose.Schema({
    user_id: {type: String, required: true},
    categoryService_id: {type: String, required: true},
    sousCategory_id: {type: String, required: true},
    description: {type: String},
    min_price: {type: Number},
    max_price: {type: Number},
    status: {type: String},         //status = in_progress,completed,bidder_selected,review
    nombre_bids: {type: Number},
    Avg_bids: {type: Number},
    nom_service: {type: String},
    city_deliver_help: {type: String},
    preferred_date_service: {type: Date},
    preferred_time_service: {type: String},
    nbre_etoiles: {type: Number},
    device_id: {type: String},
    device_name: {type: String},
    user_id_celui_choisit: {type: String},
    username_celui_choisit: {type: String},
    firstname_celui_choisit: {type: String},
    location_name_celui_choisit: {type: String},
    number_helps_asked_celui_choisit: {type: Number},
    number_helps_offer_celui_choisit: {type: Number},
    nbre_etoiles_celui_choisit: {type: Number},
    picture_celui_choisit: {type: String},
    price_bid_celui_choisit: {type: Number},
    date_livraison_celui_choisit: {type: Date},
    time_livraison_celui_choisit: {type: String},
    description_proposition_celui_choisit: {type: String},
    etoiles_donnees_par_offer_service: {type: Number},
    commentaire_offer_service: {type: String},
    commentaire_celui_choisit: {type: String},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export service ask for help model
var ServiceAskForHelp = module.exports = mongoose.model('serviceAskForHelp', serviceAskForHelpSchema);
module.exports.get = function (callback, limit) {
    ServiceAskForHelp.find(callback).limit(limit);
}

