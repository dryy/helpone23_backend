
var mongoose = require('mongoose');

// Setup schema
var anshelpSchema = mongoose.Schema({
    // ansHelpId: String,
    note_help_id: String,
    ans_help: String,
    user_service_id: String,
    user_id_celui_qui_recoit: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});

// Export HelpNotes model
var Anshelp = module.exports = mongoose.model('anshelp', anshelpSchema);
module.exports.get = function (callback, limit) {
    Anshelp.find(callback).limit(limit);
}