/**
 * Created by christophe on 19/01/06.
 */

// preferred_date_and_time_serviceModel.js

var mongoose = require('mongoose');


// Setup schema
var preferred_date_and_time_serviceSchema = mongoose.Schema({
    user_id: {type: String},
    date: {type: Date},
    time: {type: String},
    serviceAskForHelp_id: {type: String, required: true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export preferred date and time service model
var Preferred_date_and_time_service = module.exports = mongoose.model('preferred_date_and_time_service', preferred_date_and_time_serviceSchema);
module.exports.get = function (callback, limit) {
    Preferred_date_and_time_service.find(callback).limit(limit);
}

