/**
 * Created by landry on 20/06/07.
 */

// LiveChatModel.js

var mongoose = require('mongoose');


// Setup schema
var livechatSchema = mongoose.Schema({
    user_name: String,
    user_email: String,
    chat_content: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export LiveChat model
var Livechat = module.exports = mongoose.model('livechat', livechatSchema);
module.exports.get = function (callback, limit) {
    Livechat.find(callback).limit(limit);
}
