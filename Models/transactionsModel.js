/**
 * Created by christophe on 19/01/05.
 */

// transactionsModel.js

var mongoose = require('mongoose');


// Setup schema
var transactionSchema = mongoose.Schema({
    type_transaction: {type: String,required: true},    //wallet, caisse, profit(intérêt sur chaque service rendu de help123), ...
    amount: {type: Number, required: true},
    sens: {type: String,required:true},     //entrant ou sortant
    user_id: {type: String},
    reference_transaction: String,
    device_id: String,
    device_name: String,
    methodpayment_id: String,
    methodpayment_name: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export transactions model
var Transactions = module.exports = mongoose.model('transactions', transactionSchema);
module.exports.get = function (callback, limit) {
    Transactions.find(callback).limit(limit);
}

