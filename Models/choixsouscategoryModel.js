/**
 * Created by christophe on 18/12/06.
 */

// choixsouscategoryModel.js

var mongoose = require('mongoose');


// Setup schema
var choixsouscategorySchema = mongoose.Schema({
    souscategory_id: {type: String,required: true},
    user_id: {type: String,required:true},
    souscategoryName: {type: String,required:true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export choixsouscategory model
var Choixsouscategory = module.exports = mongoose.model('choixsouscategory', choixsouscategorySchema);
module.exports.get = function (callback, limit) {
    Choixsouscategory.find(callback).limit(limit); 
}

