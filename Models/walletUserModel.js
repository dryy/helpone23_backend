/**
 * Created by christophe on 19/01/04.
 */

// walletUserModel.js

var mongoose = require('mongoose');


// Setup schema
var walletUserSchema = mongoose.Schema({
    user_id: {type: String,required:true},
    amount: {type: Number,required:true},
    device_id: String,
    name_device: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export walletUser model
var Walletuser = module.exports = mongoose.model('walletuser', walletUserSchema);
module.exports.get = function (callback, limit) {
    Walletuser.find(callback).limit(limit);
}
