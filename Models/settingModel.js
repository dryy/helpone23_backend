/**
 * Created by christophe on 18/09/24.
 */

// settingsModel.js

var mongoose = require('mongoose');


// Setup schema
var settingSchema = mongoose.Schema({
    percentageOneService: {
        type: Number,
        required: true
    },
    /*timeToSendReport: {
        value_time:{type: Number, required: true},
        type_time:{type: String, required: true}
    },*/
    timeToSendReport_value:{type: Number, required: true},
    timeToSendReport_type:{type: String, required: true},       //hours,minutes,seconds,days,months
    amount_registration_fees:{type: Number, required: true},
    /*costMinService: {
        value_cost:{type: Number},
        device_cost:{type: String}
    },*/
    costMinService_value: Number,
    costMinService_device: String,      //euro,dollar,live sterling, ...
    percentageAfterHourWork: Number,
    /*costOneAdvertising:{
        value_advert:{type: Number},
        device_advert:{type: String}
    },*/
    costOneAdvertising_value: Number,
    costOneAdvertising_device: String,  //euro,dollar,live sterling, ...
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Setting model
var Setting = module.exports = mongoose.model('setting', settingSchema);
module.exports.get = function (callback, limit) {
    Setting.find(callback).limit(limit);
}

