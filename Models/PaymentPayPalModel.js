/**
 * Created by christophe on 19/21/03.
 */

// PaymentPayPalModel.js

var mongoose = require('mongoose');


// Setup schema
var paymentpaypalSchema = mongoose.Schema({
    user_id: String,
    paymentId: String,
    token: String,
    PayerID: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Payment PayPal model
var PaymentPayPal = module.exports = mongoose.model('paymentpaypal', paymentpaypalSchema);
module.exports.get = function (callback, limit) {
    PaymentPayPal.find(callback).limit(limit);
}
