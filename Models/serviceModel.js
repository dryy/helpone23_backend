/**
 * Created by christophe on 18/09/24.
 */

// servicesModel.js

var mongoose = require('mongoose');


// Setup schema
var serviceSchema = mongoose.Schema({
    serviceName: {type: String,required: true},
    /*categoryService: {
        categoryName:{
            type: String,
            required: true
        }
    },*/
    description: String,
    categoryServiceId: {type: String,required:true},
    categoryName: {type: String,required:true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Service model
var Service = module.exports = mongoose.model('service', serviceSchema);
module.exports.get = function (callback, limit) {
    Service.find(callback).limit(limit);
}

