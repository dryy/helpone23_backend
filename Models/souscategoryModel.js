/**
 * Created by christophe on 18/11/23.
 */

// souscategoryModel.js

var mongoose = require('mongoose');


// Setup schema
var souscategorySchema = mongoose.Schema({
    souscategoryName: {type: String,required: true},
    logo: String,
    description: String,
    categoryServiceId: {type: String,required:true},
    categoryName: {type: String,required:true},
    description_category: String,
    logo_category: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Souscategory model
var Souscategory = module.exports = mongoose.model('souscategory', souscategorySchema);
module.exports.get = function (callback, limit) {
    Souscategory.find(callback).limit(limit);
}

