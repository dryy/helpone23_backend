/**
 * Created by christophe on 18/12/12.
 */

// languesModel.js

var mongoose = require('mongoose');


// Setup schema
var langueSchema = mongoose.Schema({
    nom_langue:{type: String,required: true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Langue model
var Langue = module.exports = mongoose.model('langue', langueSchema);
module.exports.get = function (callback, limit) {
    Langue.find(callback).limit(limit);
}

