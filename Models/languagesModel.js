/**
 * Created by christophe on 18/11/23.
 */

// languageModel.js

var mongoose = require('mongoose');


// Setup schema
var languageSchema = mongoose.Schema({
    user_id:{type: String,required: true},
    langue: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export Language model
var Language = module.exports = mongoose.model('language', languageSchema);
module.exports.get = function (callback, limit) {
    Language.find(callback).limit(limit);
}

