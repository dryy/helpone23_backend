/**
 * Created by christophe on 18/11/20.
 */

// logoModel.js

var mongoose = require('mongoose');


// Setup schema
var logoSchema = mongoose.Schema({
    nom_logo:{type: String, unique: true,required: true},
    image_logo:{type: String, required: true},
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date:{type: Date}
});


// Export Logo model
var Logo = module.exports = mongoose.model('logo', logoSchema);
module.exports.get = function (callback, limit) {
    Logo.find(callback).limit(limit);
}

