/**
 * Created by christophe on 19/01/22.
 */

// bidModel.js

var mongoose = require('mongoose');


// Setup schema
var bidSchema = mongoose.Schema({
    serviceAskForHelpId: {type: String},
    user_id_celui_qui_bid: {type: String},
    user_id_celui_qui_offre_service: {type: String},
    price_bid: {type: Number},
    date_livraison: {type: Date},
    time_livraison: {type: String},
    description_proposition: {type: String},
    status_service: {type: String},         //status = in_progress,completed,bidder_selected,review
    username_celui_qui_bid: {type: String},
    firstname_celui_qui_bid: {type: String},
    location_name_celui_qui_bid: {type: String},
    number_helps_asked_celui_qui_bid: {type: Number},
    number_helps_offer_celui_qui_bid: {type: Number},
    nbre_etoiles_celui_qui_bid: {type: Number},
    picture_celui_qui_bid: {type: String},
    username_celui_qui_offre_service: {type: String},
    firstname_celui_qui_offre_service: {type: String},
    location_name_celui_qui_offre_service: {type: String},
    number_helps_asked_celui_qui_offre_service: {type: Number},
    number_helps_offer_celui_qui_offre_service: {type: Number},
    nbre_etoiles_celui_qui_offre_service: {type: Number},
    picture_celui_qui_offre_service: {type: String},
    //city_deliver_help: {type: String},

    /* Start: champs venant de serviceAskForHelpModel.js */
    categoryService_id: {type: String, required: true},
    sousCategory_id: {type: String, required: true},
    description: {type: String},
    min_price: {type: Number},
    max_price: {type: Number},
    nombre_bids: {type: Number},
    Avg_bids: {type: Number},
    nom_service: {type: String},
    city_deliver_help: {type: String},
    preferred_date_service: {type: Date},
    preferred_time_service: {type: String},
    device_id: {type: String},
    device_name: {type: String},
    etoiles_donnees_par_offer_service: {type: Number},
    commentaire_offer_service: {type: String},

    /* End: champs venant de serviceAskForHelpModel.js*/



    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
});


// Export bid model
var Bid = module.exports = mongoose.model('bid', bidSchema);
module.exports.get = function (callback, limit) {
    Bid.find(callback).limit(limit);
}

