/**
 * Created by christophe on 18/09/24.
 */

// categoryServiceModel.js

var mongoose = require('mongoose');


// Setup schema
var categoryserviceSchema = mongoose.Schema({
    categoryName: {type: String,required: true,unique:true},
    logo: String,
    description: String,
    create_date: {
        type: Date,
        default: Date.now
    },
    update_date: {type: Date}
    
});


// Export categoryService model
var categoryService = module.exports = mongoose.model('categoryservice', categoryserviceSchema);
module.exports.get = function (callback, limit) {
    categoryService.find(callback).limit(limit);
}


