var plato = require('plato');

var files = [
  'api-routes.js',
  'Controllers/userController.js',
  'Controllers/addressUserController.js',
  'Controllers/bidController.js',
  'Controllers/categoryServiceController.js',
  'Controllers/choixDeliverAndBillHelpForAServiceController.js',
  'Controllers/choixsouscategoryController.js',
  'Controllers/contactController.js',
  'Controllers/deviceController.js',
  'Controllers/HelpNotesController.js',
  'Controllers/image_serviceController.js',
  'Controllers/languagesController.js',
  'Controllers/languesController.js',
  'Controllers/LiveChatController.js',
  'Controllers/logoController.js',
  'Controllers/methodPaymentController.js',
  'Controllers/notificationsController.js',
  'Controllers/passwordresetsController.js',
];

var outputDir = './reports';
// null options for this example
var options = {
  title: 'Your title here'
};

var callback = function (report){
// once done the analysis,
// execute this
};

plato.inspect(files, outputDir, options, callback);