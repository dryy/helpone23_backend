/**
 * Created by christophe on 18/09/24.
 */

//// Filename: api-routes.js
//// Initialize express router
//let router = require('express').Router();
//
//// Set default API response
//router.get('/', function (req, res){
//    res.json({
//        status: 'API Its Working',
//        message: 'Welcome to RESTHub crafted with love!',
//    });
//});
//
//// Export API routes
//module.exports = router;



// api-routes.js
// Initialize express router
let router = require('express').Router();


// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to Help123!'
    });
});


//Exemple pour appeler ces routes dans Postman: http://localhost:8080/api/url

/*********************************** Contact *****************************************************/
// Import contact controller
var contactController = require('./Controllers/contactController');


// Contact routes
router.route('/contacts')
    .get(contactController.index)
    .post(contactController.new);


router.route('/contacts/:contact_id')
    .get(contactController.view)
    .patch(contactController.update)
    .put(contactController.update)
    .delete(contactController.delete);

router.patch('/contact/:contact_id',contactController.updateNew);



/*********************************** User *****************************************************/
// Import user controller
var userController = require('./Controllers/userController');       //importation du controller User


// User routes
/*router.route('/users')
 .get(userController.index)          //permet de get tous les users
 .post(userController.register);     //permet de register un user
 */

/*
 router.route('/users/:user_id')
 .get(userController.view)           //permet de get un user à partir de son ObjectId
 //.patch(userController.register)
 //.put(userController.update)
 .delete(userController.delete);     //permet de supprimer un user à partir de son ObjectId
 */

router.get('/users/get',userController.index);              //permet de get tous les users
router.get('/users/sendmail',userController.eee);              //permet d'envoyer le mail de confirmation du compte àl'utilisateur
router.post('/users/activate',userController.accountConfirmation);              //permet de get tous les users

router.post('/users/signup',userController.register);     //permet de register un user

router.get('/users/motDePasse',userController.motDePasse); 

router.post('/users/switchUser',userController.switchAccount);     //permet de erndre un user demandeur en un user offreur

router.post('/users/signin',userController.login);           //permet à un user de se login

router.get('/users/getprofile/:user_id',userController.view);         //permet de get le profile d'un user à partir de son ObjectId

router.post('/users/editprofile/:user_id',userController.updateNew);   //permet d'update les infos d'un user

// router.delete('/users/delete/:user_id',userController.delete);     //permet de supprimer un user à partir de son ObjectId
router.delete('/users/delete',userController.delete);
router.post('/users/enlever',userController.enlever);

router.post('/users/mettreAdmin',userController.mettreAdmin); // cette fonction permet de définir un user comme administrateur juste pour le premier. Après c'est celui ci qui sera chargé de définir d'autre administrateurs en utilisant l'interdface graphique 

router.post('/users/suspension',userController.accountSuspension); // permet de suspendre un utilisateur de l'application

router.post('/users/unSuspension',userController.accountUnSuspension); // permet de lever la suspension d'un utilisateur de l'application

router.post('/users/checkEmail',userController.checkEmail);         //permet de vérifier si l'email existe déjà et renvoit les suggestions de username

router.post('/users/checkUsername',userController.checkUsername);   //permet de vérifier si le username existe déjà







/*********************************** Role *****************************************************/
// Import role controller
var roleController = require('./Controllers/roleController');       //importation du controller Role


router.get('/roles/get',roleController.index);              //permet de get tous les rôles

router.post('/roles/save',roleController.new);     //permet d'enregistrer un rôle

router.get('/roles/view/:role_id',roleController.view);         //permet de get un rôle à partir de son ObjectId

router.patch('/roles/editrole/:role_id',roleController.update);   //permet d'update les infos d'un rôle

router.patch('/roles/editrole/user/:user_id',roleController.update_role_user);    //permet d'update le rôle d'un user

router.delete('/roles/delete/:role_id',roleController.delete);     //permet de supprimer un rôle à partir de son ObjectId

router.post('/roles/update/role_id',roleController.update_role_id);  //permet d'update le role_id dans le modèle User





/*********************************** PasswordResets *****************************************************/
var passwordresetsController = require('./Controllers/passwordresetsController');       //importation du controller PasswordReset

// Passwordresets routes
/*router.route('/passwordresets')
 .get(passwordresetsController.index)        //permet de renvoyer tous les passwordresets
 .post(passwordresetsController.sendCodeUser);        //permet d'envoyer le code au user pour reset son password


 router.route('/passwordresets/:passwordreset_id')
 .get(passwordresetsController.view)         //permet de renvoyer un passwordreset à partir de son Id
 .delete(passwordresetsController.delete);   //permet de supprimer un passwordreset
 */

router.get('/passwordresets/get',passwordresetsController.index);       //permet de renvoyer tous les passwordresets

router.post('/passwordresets/sendcode',passwordresetsController.sendCodeUser);  //permet d'envoyer le code au user pour reset son password

router.get('/passwordresets/get/:passwordresets_id',passwordresetsController.view);     //permet de renvoyer un passwordreset à partir de son Id

router.post('/passwordresets/checkcode',passwordresetsController.updateNew);     //permet de vérifier le code entré par le user

router.delete('/passwordresets/delete/:passwordresets_id',passwordresetsController.delete);     //permet de supprimer un passwordreset

router.post('/passwordresets/new_password/:user_id',passwordresetsController.newPassword);       //permet de save le nouveau password du user



/*********************************** Notifications *****************************************************/
var notificationsController = require('./Controllers/notificationsController');       //importation du controller Notifications

// Notifications routes
/*router.route('/notifications')
 .get(notificationsController.index)        //permet de renvoyer tous les notifications
 .post(notificationsController.sendCodeUser);        //permet d'envoyer le code au user pour reset son password

 */

router.post('/notifications/save',notificationsController.new);       //permet de créer une notification

router.get('/notifications/get/:user_id',notificationsController.getNotifications);  //permet de récupérer toutes les notifications d'un utilisateur

router.get('/notifications/lire/:notif_id',notificationsController.readNotifications);  //permet de marquer une notification comme lue

// router.get('/notifications/getForService/:service_id',notificationsController.view);     //permet de renvoyer les notifications d'un service donné

router.delete('/notifications/supprimer/:notif_id',notificationsController.delete);     //permet de supprimer une notification

// router.post('/notifications/new_password/:user_id',notificationsController.newPassword);       //permet de save le nouveau password du user



/*********************************** Setting *****************************************************/
// Import setting controller
var settingController = require('./Controllers/settingController');       //importation du controller Setting


// Setting routes
/*router.route('/settings')
 .get(settingController.index)
 .post(settingController.new);
 */

/*router.route('/settings/:setting_id')
 .get(settingController.view)
 .patch(settingController.update)
 .put(settingController.update)
 .delete(settingController.delete);
 */

router.get('/settings/get',settingController.index);        //permet de get les settings

router.post('/settings/save',settingController.new);        //permet de save un setting

router.get('/settings/view/:setting_id',settingController.view);    //permet de get un setting à partir de son ObjectId

router.patch('/settings/update/:setting_id',settingController.update);  //permet d'update un setting

router.delete('/settings/delete/:setting_id',settingController.delete);     //permet de delete un setting



/*********************************** Service *****************************************************/
// Import service controller
var serviceController = require('./Controllers/serviceController');       //importation du controller Service


// Service routes
/*router.route('/services')
 .get(serviceController.index)
 .post(serviceController.new);
 */

/*router.route('/services/:service_id')
 .get(serviceController.view)
 .patch(serviceController.update)
 .put(serviceController.update)
 .delete(serviceController.delete);
 */

router.get('/services/get',serviceController.index);        //permet de get tous les services

router.post('/services/save',serviceController.new);        //permet d'enregistrer un service

router.get('/services/view/:service_id',serviceController.view);    //permet de get un service à partir de son ObjectId

router.patch('/services/update/:service_id',serviceController.update);      //permet d'update un service

router.delete('/services/delete/:service_id',serviceController.delete);     //permet de supprimer un service




/*********************************** categoryService *****************************************************/
// Import categoryservice controller
var categoryServiceController = require('./Controllers/categoryServiceController');       //importation du controller categoryService


// categoryService routes
router.get('/category/services/get',categoryServiceController.index);        //permet de get toutes les catégories de services

router.post('/category/services/save',categoryServiceController.new);        //permet d'enregistrer une catégorie de service

router.get('/category/view/:category_id',categoryServiceController.view);    //permet de get un service à partir de son ObjectId

router.patch('/category/services/update/:category_id',categoryServiceController.update);      //permet d'update une catégorie de service

router.delete('/category/services/delete/:category_id',categoryServiceController.delete);     //permet de supprimer une catégorie de service





/*********************************** sousCategory *****************************************************/
// Import souscategory controller
var souscategoryController = require('./Controllers/souscategoryController');       //importation du controller souscategory


// souscategory routes
router.get('/souscategory/get',souscategoryController.index);        //permet de get toutes les sous catégories

router.post('/souscategory/save',souscategoryController.new);        //permet d'enregistrer une sous catégorie d'une catégorie

router.post('/souscategory/get/souscategory/ofAcategory',souscategoryController.getSouscategoryCategory);   //permet de get les sous-categories d'une catégorie

router.get('/souscategory/view/:souscategory_id',souscategoryController.view);    //permet de get une souscategory à partir de son ObjectId

router.patch('/souscategory/update/:souscategory_id',souscategoryController.update);      //permet d'update une sous catégorie d'une catégorie

router.delete('/souscategory/delete/:souscategory_id',souscategoryController.delete);     //permet de supprimer une sous catégorie

router.get('/souscategory/get/category/:souscategory_id',souscategoryController.getCategorySousCategory);       //permet de get la catégorie d'une sous-catégorie

router.get('/souscategory/add/field/:souscategory_id',souscategoryController.add_field_in_souscategoryModel);   //permet d'ajouter les champs description_category et logo_category dans souscategoryModel



/*********************************** Langues *****************************************************/
// Import langues controller
var langueController = require('./Controllers/languesController');       //importation du controller Langues


// langues routes
router.get('/langues/get',langueController.index);        //permet de get toutes les langues

router.post('/langues/save',langueController.new);        //permet de save une langue

router.get('/langues/view/:langue_id',langueController.view);    //permet de get une langue à partir de son ObjectId

router.patch('/langues/update/:langue_id',langueController.update);  //permet d'update une langue

router.delete('/langues/delete/:langue_id',langueController.delete);     //permet de delete une langue







/*********************************** WalletUser *****************************************************/
// Import walletUser controller
var walletUserController = require('./Controllers/walletUserController');       //importation du controller WalletUser


// walletUser routes
router.post('/walletuser/save',walletUserController.new);        //permet de charger le portefeuille du user

router.get('/walletuser/view/:user_id',walletUserController.view);    // permet de GET le portefeuille du user à partir de son ObjectId

router.patch('/walletuser/mettre_a_zero',walletUserController.mettre_a_zero);  //pour remettre le wallet du user à un certain montant

router.delete('/walletuser/delete/:wallet_id',walletUserController.delete_device_wallet); //pour supprimer une device du wallet du user



/*********************************** TransactionWalletUser *****************************************************/
// Import transactionWalletUser controller
var transactionWalletUserController = require('./Controllers/transactionWalletUserController');       //importation du controller transactionWalletUser


// transactionWalletUser routes
router.get('/transactionwalletuser/view/:user_id',transactionWalletUserController.view);    //permet de GET les transactions du portefeuille du user





/*********************************** AddressUser *****************************************************/
// Import addresUser controller
var addressUserController = require('./Controllers/addressUserController');       //importation du controller addressUser


// addressUser routes
router.get('/addressuser/view/:user_id',addressUserController.view);    //permet de GET les différentes adresses du User

router.get('/addressuser/view/detail/:addressUser_id',addressUserController.view_address);   //permet de GET l'info d'une adresse particulière

router.post('/addressuser/save',addressUserController.new);        //permet d'enregistrer une adresse pour un user

router.post('/addressuser/update/:addressUser_id',addressUserController.update);  //permet d'update une adresse

router.delete('/addressuser/delete/:addressUser_id',addressUserController.delete);     //permet de delete une adresse




/*********************************** ServiceAskForHelp *****************************************************/
// Import serviceAskForHelp controller
var serviceAskForHelpController = require('./Controllers/serviceAskForHelpController');       //importation du controller serviceAskForHelp


// serviceAskForHelp routes
router.get('/serviceaskforhelp/view/:user_id',serviceAskForHelpController.view);    //permet de GET tous les services du User

router.post('/serviceaskforhelp/save',serviceAskForHelpController.new);        //permet d'enregistrer un service pour un user

router.post('/serviceaskforhelp/update/:serviceaskforhelp_id',serviceAskForHelpController.update);  //permet d'update un service

router.post('/serviceaskforhelp/setEtoiles/:serviceaskforhelp_id',serviceAskForHelpController.setEtoiles);  //permet d'update un service

router.post('/serviceaskforhelp/setOfferEtoiles/:serviceaskforhelp_id',serviceAskForHelpController.setOfferEtoiles);  //permet d'update le nombre d'étoiles du l'offreur

router.delete('/serviceaskforhelp/delete/:serviceaskforhelp_id',serviceAskForHelpController.delete);     //permet de delete un service

router.get('/serviceaskforhelp/view/service/:serviceaskforhelp_id',serviceAskForHelpController.view_service);   //permet de get les infos principales d'un service à savoir user_id, categoryService_id, souscategoryService_id, description,
                                                                                                                //min_price, max_price et status

router.post('/serviceaskforhelp/accept/bidder/:serviceaskforhelp_id',serviceAskForHelpController.accept_bidder);   //permet d'enregistrer celui qui a été accepté pour faire le service du user

router.patch('/serviceaskforhelp/confirm/completed/service/:serviceaskforhelp_id',serviceAskForHelpController.confirm_completed_service);       //permet de confirmer que le service a été fait avec succès puis enregistre le nombre d'étoiles
                                                                                                                                                //et le commentaire fait par l'offer de service

router.post('/serviceaskforhelp/pay/helper',serviceAskForHelpController.payHelper);       //permet de payer le travailleur qui a fini le service

router.post('/serviceaskforhelp/completeHelp',serviceAskForHelpController.completeHelp);       //permet de payer le travailleur qui a fini le service

router.get('/serviceaskforhelp/change/status/service/:serviceaskforhelp_id',serviceAskForHelpController.change_status_service);         //permet de changer le status d'un service en bidder_selected


/************ Dashboard Request ***/
router.get('/serviceaskforhelp/my_open_helps/:user_id',serviceAskForHelpController.my_open_helps);   //permet de GET tous les services du User qui sont en status = in_progress (My Open Helps)

router.get('/serviceaskforhelp/recent_helps_requests/:user_id',serviceAskForHelpController.recent_helps_requests);   //permet de get tous les services différents de celui qui est connecté (user) avec pour statut 'in_progress'

router.get('/serviceaskforhelp/other_recent_helps_requests/:user_id',serviceAskForHelpController.other_recent_helps_requests);  //permet de renvoyer les helps créés par un user sans distinction de statut

router.get('/serviceaskforhelp/recent_helps_requests',serviceAskForHelpController.recent_helps_requests_for_non_connected_users);   //permet de get tous les services avec pour status = in_progress (ceci c'est pour les users non connectés)

router.get('/serviceaskforhelp/completed_helps_requests',serviceAskForHelpController.completed_helps_requests);   //permet de get tous les services avec pour status = completed

router.get('/serviceaskforhelp/nbre_helps_dashboard/:user_id',serviceAskForHelpController.nbre_helps_dashboard);     //permet de get le nbre de My Open Helps, le nbre de My Bidding Helps, le nbre de My Completed Helps et le nbre de My Closed Helps d'un user

router.get('/serviceaskforhelp/my_bidding_helps/:user_id',serviceAskForHelpController.my_bidding_help);             //permet de renvoyer les services biddés par un user (My Bidding Helps)






/*********************************** Image_service *****************************************************/
//Import image_service controller
var image_serviceController = require('./Controllers/image_serviceController');

// image_service routes
router.get('/image/service/view/:serviceaskforhelp_id',image_serviceController.view);    //permet de GET toutes les images d'un serviceAskForHelp_id



/*********************************** Preferred_date_and_time_service *****************************************************/
//Import Preferred_date_and_time_service controller
var preferred_date_and_time_serviceController = require('./Controllers/preferred_date_and_time_serviceController');

// Preferred_date_and_time_service routes
router.get('/preferred_date_and_time/service/view/:serviceaskforhelp_id',preferred_date_and_time_serviceController.view);    //permet de GET toutes les dates et heures préférées d'un serviceAskForHelp_id



/*********************************** ChoixDeliverAndBillHelpForAService *****************************************************/
//Import ChoixDeliverAndBillHelpForAService controller
var choixDeliverAndBillHelpForAServiceController = require('./Controllers/choixDeliverAndBillHelpForAServiceController');

// ChoixDeliverAndBillHelpForAService routes
router.get('/choix_deliver_and_bill/service/view/:serviceaskforhelp_id',choixDeliverAndBillHelpForAServiceController.view);    //permet de GET toutes adresses choisies par le user pour un serviceAskForHelp_id




/*********************************** BID *****************************************************/
var bidController = require('./Controllers/bidController');

router.patch('/bid/update/location/:user_id',bidController.update_location);                    //permet d'update la location où le user se trouve pour l'aide

router.patch('/bid/update/skill/:user_id',bidController.update_skill);                  //permet d'update les skills (compétences d'un user) qui sont enregistrées dans choixsouscategoryModel

router.get('/bid/get/service/:user_id',bidController.get_service);                      //permet de get tous les services différents de celui qui est connecté (user) avec pour statut 'in_progress'

router.get('/bid/get/bidders/:serviceaskforhelp_id',bidController.get_bidders);         //permet de get tous les bidders d'un service donné

router.post('/bid/save',bidController.saveBid);                                     //permet de save le bid d'un user

router.get('/bid/mettre/a/jour/user/:user_id',bidController.mettre_a_jour_user);         //permet de mettre à jour number_helps_asked, number_helps_offer, nbre_etoiles et location_name pour un user

router.get('/bid/mettre/a/jour/nom_service/:serviceaskforhelp_id',bidController.mettre_a_jour_nom_service);     //permet de mettre à jour nom_service dans le service n'ayant pas de nom

router.get('/bid/add/field/:serviceAskForHelp_id',bidController.update_fields_bidModel);        //permet d'ajouter certains champs de serviceAskForHelp dans bidModel; ceci pour les services déjà créés





/*********************************** DEVICE *****************************************************/
var deviceController = require('./Controllers/deviceController');

router.get('/device/get',deviceController.index);       //permet de get toutes les devices

router.post('/device/save',deviceController.new);          //permet d'enregistrer une device

router.patch('/device/update/:device_id',deviceController.update);   //permet d'update une device

router.get('/device/get/adevice/:device_id',deviceController.view);         //permet de get les infos d'une device Id

router.delete('/device/delete/:device_id',deviceController.delete);         //permet de delete une device






/*********************************** MethodPayement *****************************************************/
var methodPaymentController = require('./Controllers/methodPaymentController');

router.get('/methodpayment/get',methodPaymentController.index);       //permet de get toutes les méthodes de paiement

router.post('/methodpayment/save',methodPaymentController.new);          //permet d'enregistrer une méthode de paiement

router.patch('/methodpayment/update/:methodpayment_id',methodPaymentController.update);   //permet d'update une méthode de paiement

router.get('/methodpayment/get/amethodpayment/:methodpayment_id',methodPaymentController.view);         //permet de get les infos d'une méthode de paiement Id

router.delete('/methodpayment/delete/:methodpayment_id',methodPaymentController.delete);         //permet de delete une méthode de paiement





/*********************************** HelpNotes *****************************************************/
var HelpNotesController = require('./Controllers/HelpNotesController');

router.post('/helpnotes/save',HelpNotesController.new);         //permet d'enregistrer un helpNote d'un service Ask For Help

router.post('/helpnotes/saveAnswer',HelpNotesController.newAnswer);         //permet d'enregistrer une reponse à un helpNote d'un service Ask For Help

router.post('/HelpNotes/enregistrer',HelpNotesController.enregistrer);         //permet d'enregistrer un Nouveau message

// router.post('/HelpNotes/enregistrer',HelpNotesController.enregistrer);         //permet d'enregistrer le message d'un chat

router.delete('/HelpNotes/supprimer/:helpnote_id',HelpNotesController.delete);         //permet de supprimer un HelpNote à partir de son Id

router.post('/HelpNotes/getAll',HelpNotesController.getNotes);         //permet de GET un HelpNote à partir de l'Id de l'utilisateur qui a envoyé le message

router.post('/HelpNotes/getAllReceived',HelpNotesController.getReceivedNotes);         //permet de GET les HelpNotes à partir de l'Id de l'utilisateur à qui a été envoyé le message

router.get('/helpnotes/get/ahelpnote/:helpnote_id',HelpNotesController.view);         //permet de GET un HelpNote à partir de son Id

router.get('/helpnotes/get/allhelpnotes/:serviceAskForHelpId',HelpNotesController.view_helpnotes);         //permet de GET tous les HelpNotes d'un Service Ask For Help

router.patch('/helpnotes/update/:help_notes_id',HelpNotesController.update_helpnotes);          //permet d'update le status d'un HelpNote

router.get('/helpnotes/get/allanshelp/:helpNotesId',HelpNotesController.view_anshelp);         //permet de GET tous les AnswerHelps d'un HelpNote


/*********************************** LiveChat *****************************************************/
var LiveChatController = require('./Controllers/LiveChatController');

router.post('/livechat/save',LiveChatController.save);         //permet d'enregistrer un helpNote d'un service Ask For Help

router.get('/livechat/getAll',LiveChatController.getChats);         //permet de GET un HelpNote à partir de l'Id de l'utilisateur qui a envoyé le message

// router.delete('/livechat/supprimer/:helpnote_id',HelpNotesController.delete);         //permet de supprimer un HelpNote à partir de son Id

// router.post('/livechat/getAllReceived',HelpNotesController.getReceivedNotes);         //permet de GET les HelpNotes à partir de l'Id de l'utilisateur à qui a été envoyé le message

// router.get('/livechat/get/ahelpnote/:helpnote_id',HelpNotesController.view);         //permet de GET un HelpNote à partir de son Id

// router.get('/livechat/get/allhelpnotes/:serviceAskForHelpId',HelpNotesController.view_helpnotes);         //permet de GET tous les HelpNotes d'un Service Ask For Help

// router.patch('/livechat/update/:help_notes_id',HelpNotesController.update_helpnotes);          //permet d'update le status d'un HelpNote

// router.get('/livechat/get/allanshelp/:helpNotesId',HelpNotesController.view_anshelp);         //permet de GET tous les AnswerHelps d'un HelpNote



/*********************************** ScheduleDates *****************************************************/
var ScheduleDatesController = require('./Controllers/ScheduleDatesController');

router.post('/scheduledates/save',ScheduleDatesController.new);         //permet d'enregistrer un scheduleDate d'un service Ask For Help

router.get('/scheduledates/get/ascheduledate/:scheduledate_id',ScheduleDatesController.view);         //permet de GET un scheduleDate à partir de son Id

router.get('/scheduledates/get/allscheduledates/:serviceAskForHelpId',ScheduleDatesController.view_schedules);         //permet de GET tous les scheduleDates d'un Service Ask For Help

router.patch('/scheduledates/update/schedule/status/:scheduledate_id',ScheduleDatesController.update_status_schedule);  //permet d'approuver (approved) ou de revoquer (revoked) un schedule date






var testtableau = require('./Controllers/testtableau')
choixsouscategoryController = require('./Controllers/choixsouscategoryController');

router.post('/users/test',testtableau.test);             //permet de faire le test sur les tableaux d'objet

router.get('/users/get/choose',choixsouscategoryController.index_choix);

router.get('/users/get/langue',choixsouscategoryController.index_langue);


// Export API routes
module.exports = router;







//06/12/18
//à mettre sur le serveur et sur gitlab
//1- api-routes.js
//2- userController.js
//3- userModel.js
//4- sousCategoryController.js
//5- choixsouscategoryModel.js
//6- choixsouscategoryController.js


//04/01/19
//à mettre sur le serveur et sur gitlab
//1- api-routes.js  à remplacer
//2- walletUserModel.js     à créer         (déjà)
//3- walletUserController.js    à créer     (déjà)


//05/01/19
//à mettre sur le serveur et sur gitlab
//1- transactionsModel.js       à créer         (déjà)
//2- transactionWalletUserModel.js   à créer    (déjà)
//3- transactionWalletUserController.js     à créer     (déjà)
//4- addressUserModel.js            à créer     (déjà)
//5- addressUserController.js          à créer      (déjà)
//6- choixDeliverAndBillHelpForAServiceModel.js     à créer     (déjà)


//06/01/19
//à mettre sur le serveur et sur gitlab
//1- serviceAskForHelpModel.js      à créer     (déjà)
//2- image_serviceModel.js          à créer     (déjà)
//3- preferred_date_and_time_serviceModel.js        à créer     (déjà)
//4- serviceAskForHelpController.js         à créer     (déjà)
//5- Argent_en_attente_pour_finition_serviceModel.js        à créer     (déjà)

//08/01/19
//à mettre sur le serveur et sur gitlab
//1- image_serviceController.js             à créer     (déjà)


//09/01/19
//à mettre sur le serveur et sur gitllab
//1- categoryServiceModel.js            à remplacer     (déjà)
//2- categoryServiceController.js       à remplacer     (déjà)


//22/01/19
//à mettre sur le serveur et sur gitlab
//1- remplacer userModel.js et userController.js                                                                (déjà)
//2- dire aux gens du frontend qu'ils doivent ajouter le champ location_name au moment de update un user
//3- remplacer api-routes.js                                    (déjà)
//4- créer bidModel.js                                          (déjà)
//5- créer bidController.js                                     (déjà)
//6- remplacer serviceAskForHelpModel.js                        (déjà)
//7- remplacer serviceAskForHelpController.js                   (déjà)
//8- remplacer image_serviceController.js                       (déjà)
//9- remplacer preferred_date_and_time_serviceController.js     (déjà)
//10- remplacer transactionWalletUserController.js              (déjà)
//11- remplacer walletUserController.js                         (déjà)


//à l'attention des frontender
//1- dire aux gens du frontend qu'ils doivent ajouter le champ location_name au moment de update un user
//2- dans le fichier File Data ServiceAskForHelpModel, le json de réponse du numéro 2 a été update avec les champs nombre_bids
//- (qui représente le nbre de users ayant bidés pour l'offre, Avg_bids représente la moyenne des offres, city_deliver_help représente
//- le nom de la ville qui figure sur le template, nbre_etoiles qui représente le nbre d'étoiles du user qui a créé le service, preferred_date_service qui représente la première valeur de date et
//- preferred_time_service qui représente la première valuer de time, nom_service qui représente le nom du service créé
//3- Pour avoir le number_helps_asked, le number_helps_offer et le nbre_etoiles d'un user il faudrait utiliser le numéro 6 du fichier File_Data_userModel_review
//- qui permet de get le profile du user


//11/02/19
//à mettre sur le serveur et sur gitlab
//1- remplacer api-routes.js
//2- remplacer userController.js
//3- remplacer userModel.js
//4- ajouter deviceModel.js
//5- ajouter deviceController.js
//6- remplacer languesController.js
//7- remplacer walletUserModel.js
//8- remplacer transactionsModel.js
//9- remplacer transactionWalletUserModel.js

//14/02/19
//1- remplacer serviceAskForHelpModel.js
//2- remplacer serviceAskForHelpController.js
//3- créer methodPaymentModel.js
//4- créer methodPaymentController.js

//15/02/19
//1- remplacer choixDeliverAndBillHelpForAServiceController.js



//25/02/19
//Routes à faire
//1- faire la route qui permet de confirmer que le service a été fait avec succès (status = completed)                  déjà
//2- faire une route qui va enregistrer les notes (appréciations) de l'offreur de service à l'encontre de celui qui a rendu le service
//(créer helpNoteModel.js et son controller)
//3- faire une route qui gère celui qu'on accepte pour un service (mettre ces infos dans serviceAskForHelpModel)        déjà
//4- créditer le compte d'un user (sens du offer service à celui ayant fait le service)








